process.env.NODE_ENV = 'test';

const   mongoose    = require("mongoose");
        EventModel  = require('../models/Event').EventModel;
        chai        = require('chai');
        chaiHttp    = require('chai-http');
        server      = require('../index').server;
        should      = chai.should();

chai.use(chaiHttp);

describe('Events', () => {
    beforeEach((done) => {
        EventModel.remove({}, (error) => {
            done();
        });
    });

    /**
     *  Тестируем /GET
     */
    describe('/GET event', () => {
        it('it should GET all the events', (done) => {
            chai.request(server)
                .get('/api/events')
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('array');
                    response.body.length.should.be.eql(0);
                    done();
                });
        });
    })

    /**
     *  Тестируем /POST
     */
    describe('', () => {
        it('it should not POST a book without pages field ', (done) => {
            const model = {
                id: 0,
                name: '111',
                date: '01.02.2017',
                cities: [
                    'Moscow',
                    'Rostov'
                ]
            }

            chai.request(server)
                .post('/api/events')
                .send(model)
                .end((error, response) => {
                    response.should.have.status(200);
                    response.body.should.be.a('object');
                    response.body.should.have.property('errors');
                    response.body.errors.should.have.property('pages');
                    response.body.errors.pages.should.have.property('')
                })
        })
    });
});
