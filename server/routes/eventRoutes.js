const   ObjectID    = require('mongodb').ObjectID,
        log         = require('../libs/log')(module),
        EventModel  = require('../models/Event').EventModel;

module.exports = function(app) {

    app.route('/api/events')
        .get(EventModel.getEvents)
        .post(EventModel.createEvents);

    app.route("/api/events/:id")
        .get(EventModel.getEvent)
        .delete(EventModel.deleteEvent)
        .put(EventModel.updateEvent);

};
