const   express         = require('express'),
        path            = require('path'),
        bodyParser      = require('body-parser'),
        log             = require('./libs/log')(module),
        config          = require('./libs/config'),
        app             = express();

// Parsing json
//парсинг application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

// запуск статического файлового сервера, который смотрит на
// папку app/ (в нашем случае отдает index.html)
app.use(express.static(path.join(__dirname, "app")));

app.get('/api', function (req, res) {
    res.send('API is running');
});

// Подключение остальных роутов
require('./routes')(app);

const server = app.listen(config.get('port'), function(){
    log.info('Express server listening on port', config.get('port'));
});

app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});

// экспортируем для тестирования
module.exports = {
    app: app,
    server: server,
};

/*
const events = [
    {
        id: 0,
        name: '111',
        date: '01.02.2017',
        cities: [
            'Moscow',
            'Rostov'
        ]
    },
    {
        id: 1,
        name: '222',
        date: '01.02.2017',
        cities: [
            'Moscow',
            'Rostov'
        ]
    },
    {
        id: 2,
        name: '333',
        date: '01.02.2017',
        cities: [
            'Moscow',
            'Rostov'
        ]
    },
];
*/
