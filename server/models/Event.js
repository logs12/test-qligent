const mongoose    = require('mongoose'),
      log         = require('../libs/log')(module),
      config      = require('../libs/config');

mongoose.connect(config.get('mongoose:uri'), {useMongoClient: true});
const db = mongoose.connection;

db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback () {
    log.info("Connected to DB!");
});

const Schema = mongoose.Schema;

// Schemas
const EventSchema = new Schema({
    name: { type: String, required: true },
    date: { type: Date, default: Date.now },
    cities: { type: Array, required: true },
    modified: { type: Date, default: Date.now },
});

// validation
EventSchema.path('name').validate(function (v) {
    return v.length > 1 && v.length < 70;
});

const EventModel = mongoose.model('Event', EventSchema);

/*
 * GET /book маршрут для получения списка всех событий.
 */
EventModel.getEvents = function(req, res) {
    return EventModel.find(function(error, events) {
        if (!error) {
            return res.json(events);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,error.message);
            return res.send({ error: 'Server error' });
        }
    });
}

/*
 * POST /book для создания нового события.
 */
EventModel.createEvents = (req, res) => {
    //Создать новое событие
    var newEvent = new EventModel(req.body);
    //Сохранить в базу.
    newEvent.save((err,event) => {
        if(err) {
            res.send(err);
        }
        else { //Если нет ошибок, отправить ответ клиенту
            res.json({message: "Event successfully added!", event });
        }
    });
}

/*
 * GET /event/:id маршрут для получения события по ID.
 */
EventModel.getEvent = (req, res) => {
    EventModel.findById(req.params.id, (err, event) => {
        if(err) res.send(err);
        //Если нет ошибок, отправить ответ клиенту
        res.json(event);
    });
}

/*
 * DELETE /event/:id маршрут для удаления события по ID.
 */
EventModel.deleteEvent = (req, res) => {
    EventModel.remove({_id : req.params.id}, (err, result) => {
        res.json({ message: "Event successfully deleted!", result });
    });
}

/*
 * PUT /event/:id маршрут для редактирования события по ID
 */
EventModel.updateEvent = (req, res) => {
    EventModel.findById({_id: req.params.id}, (err, event) => {
        if(err) res.send(err);
        Object.assign(event, req.body).save((err, event) => {
            if(err) res.send(err);
            res.json({ message: 'Event updated!', event });
        });
    });
}

module.exports.EventModel = EventModel;
//http://www.pvsm.ru/testirovanie/177790
//https://habrahabr.ru/post/193458/
