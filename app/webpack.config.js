'use strict';

const webpack = require('webpack'),
    path = require('path'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    staticCssPlugin = new ExtractTextPlugin({filename:'[name].css', disable:false, allChunks: true}),
    BrotliPlugin = require('brotli-webpack-plugin'),
    CompressionPlugin = require('compression-webpack-plugin'),
    NODE_ENV = process.env.NODE_ENV || 'development';

function isProduction() {
    return process.env.NODE_ENV === 'production';
}

module.exports = {
    // точки входа
    /*entry: [
        // Set up an ES6-ish environment
        'babel-polyfill',
        "./src/index.js"
    ],*/
    entry: {
        backend: "./src/backend/index.js",
        frontend: "./src/frontend/index.js",
    },
    // то, что получим на выходе
    output: {
        //path: "./build",
        path: path.resolve(__dirname, 'build'),
        //path: "/home/vasiliy/projects/starterkit-yii2-react-project.local/app/build/",
        filename: "[name].bundle.js",
        publicPath: '/build/',
        chunkFilename: "[id].bundle.js"
    },
    resolve: {
        modules: [
            path.resolve('./src'),
            "node_modules"
        ]
    },
    module: {
        rules: [
            {
                include: path.join(__dirname, 'src'),
                exclude: /node_modules/,
                test: /\.js$/,
                use: ['babel-loader'],
            },
            {
                test: /\.jsx$/,
                exclude: [/node_modules/, /bower_components/, /public/],
                use: ['react-hot', 'babel'],
            },
            {
                test:   /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                use: 'file?name=[path][name].[ext]'
            },
            {
                test   : /\.css$/,
                use: staticCssPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader'],
                }),
            },
            {
                test   : /\.scss$/,
                use: staticCssPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader'],
                }),
            },
        ]

    },
    devtool: 'cheap-module-source-map',

    plugins:(function(){
        var plugins = [];

        plugins.push(
            // Плагин для извлечения стилей из сборки
            staticCssPlugin,
            new webpack.ProvidePlugin({
                classNames: 'classNames',
                lodash:'lodash',
                $: "jquery/dist/jquery.js",
            }),
            /*new webpack.optimize.CommonsChunkPlugin(
                {
                    name: 'commons',
                    filename: 'commons.js',
                    minChunks: 2
                }
            ),*/
            // Добавляем значение переменной окружения в глобальные переменные
            new webpack.DefinePlugin({
                NODE_ENV: JSON.stringify(NODE_ENV)
            })
        );

        // Плагин для минификации
        if (NODE_ENV === 'production') {
            plugins.push(
                new webpack.optimize.UglifyJsPlugin({
                    compress: {
                        warnings: false,
                        drop_console: true,
                        unsafe: true
                    }
                }),
                new BrotliPlugin({
                    asset: '[path].br[query]',
                    test: /\.(js|css|html|svg)$/,
                    threshold: 10240,
                    minRatio: 0.8
                }),
                new CompressionPlugin({
                    asset: '[path].gz[query]',
                    algorithm: 'gzip', // zopfli
                    test: /\.(js|css|html|svg)$/,
                    threshold: 10240,
                    minRatio: 0.8
                })
            );
        }  else {
            module.exports.devtool = 'cheap-module-source-map';
        }

        return plugins;
    }())
};

