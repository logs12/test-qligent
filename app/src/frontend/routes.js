import React from 'react';
import { Route, IndexRoute } from 'react-router';

// Layouts Frontend
import FrontendLayout  from './layouts/main/index';

// Frontend components
import FrontendHomeFrontendPage from './pages/home-page/index';
import AboutCompanyFrontendPage from './pages/about-company-page/index';
import NewsArticlesFrontendPage from './pages/news-articles-page/index';
import NewsArticleViewFrontendPage from './pages/news-articles-page/news-article/index';

import ApartmentAdvertisementsFrontendPage from './pages/apartment-advertisements-page/index';
import ApartmentAdvertisementViewFrontendPage from './pages/apartment-advertisements-page/apartment-advertisement/index';

import NewApartmentAdvertisementsFrontendPage from './pages/new-apartment-advertisements-page/index';
import NewApartmentAdvertisementViewFrontendPage from './pages/new-apartment-advertisements-page/new-apartment-advertisement/index';

import CottageHousesLandAdvertisementsFrontendPage from './pages/cottage-houses-land-advertisements-page/index';
import CottageHousesLandAdvertisementViewFrontendPage from './pages/cottage-houses-land-advertisements-page/cottage-houses-land-advertisement/index';

import CommercialEstateAdvertisementsFrontendPage from './pages/commercial-estate-advertisements-page/index';
import CommercialEstateAdvertisementViewFrontendPage from './pages/commercial-estate-advertisements-page/commercial-estate-advertisement/index';

import VacanciesFrontendPage from './pages/vacancies-page/index';
import ContactsFrontendPage from './pages/contacts-page/index';

// Common components
import LoginPage from '../common/pages/login-page/index';
import SignUpPage from '../common/pages/sign-up-page/index';
import ErrorPage from '../common/pages/error-page/index';
import NotFoundPage from '../common/pages/not-found-page/index';
import ApplicationContainer from '../common/containers/ApplicationContainer';

// constants react-redux-router app
import {
    APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
    NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,

    COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE,

    // News apartment-advertisement
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE,

    NEWS_ARTICLE_FRONTEND_VIEW_ROUTE,
    NEWS_ARTICLE_FRONTEND_ROUTE,

    ABOUT_FRONTEND_ROUTE,
    VACANCIES_FRONTEND_ROUTE,
    CONTACTS_FRONTEND_ROUTE,
} from '../common/constants/index';

import ItemBreadcrumb from './components/Breadcrumbs/index';

export const routes = (
    <Route>
        {/*Frontend*/}
        <Route path="/" component={ApplicationContainer(FrontendLayout, false)}>
            <IndexRoute component={FrontendHomeFrontendPage} breadcrumb={<i className='fa fa-home' />} />
            <Route path={APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE} component={ApartmentAdvertisementsFrontendPage} breadcrumb='вторичное жилье' />
            <Route path={APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={ApartmentAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="ApartmentAdvertisement" />}/>

            <Route path={NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE} component={NewApartmentAdvertisementsFrontendPage}  breadcrumb='новостройки' />
            <Route path={NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={NewApartmentAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="NewApartmentAdvertisement" />} />

            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE} component={CottageHousesLandAdvertisementsFrontendPage} breadcrumb='дома, дачи, коттеджи, участки' />
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={CottageHousesLandAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="CottageHousesLandAdvertisement" />} />

            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE} component={CommercialEstateAdvertisementsFrontendPage} breadcrumb='коммерческая недвижимость'/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={CommercialEstateAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="CommercialEstateAdvertisement" />} />

            <Route path={ABOUT_FRONTEND_ROUTE} component={AboutCompanyFrontendPage} breadcrumb='о компании'/>
            <Route path={NEWS_ARTICLE_FRONTEND_ROUTE} component={NewsArticlesFrontendPage} breadcrumb='новости'/>
            <Route path={NEWS_ARTICLE_FRONTEND_VIEW_ROUTE(':id')} component={NewsArticleViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="NewsArticle" />} />
            <Route path={VACANCIES_FRONTEND_ROUTE} component={VacanciesFrontendPage} breadcrumb='вакансии'/>
            <Route path={CONTACTS_FRONTEND_ROUTE} component={ContactsFrontendPage} breadcrumb='контакты'/>
        </Route>

        {/*Common*/}
        <Route path="/login" component={ApplicationContainer(LoginPage, false)}/>
        <Route path="/sign-up" component={ApplicationContainer(SignUpPage, false)}/>

        <Route path="/error" component={ErrorPage}/>
        <Route path='*' component={NotFoundPage}/>
    </Route>
);