import './style.scss';

import React, { Component } from 'react';

import ListItems from '../../components/ListItems';

import {
    NEWS_ARTICLE_FRONTEND_VIEW_ROUTE,
    NEWS_ARTICLE_URL_REQUEST,
} from "../../../common/constants";


export default class NewsArticlesFrontendPage extends Component{

    render()  {
        return (
            <div className="news-articles-frontend-page">
                <div className="content-grid">
                    <h4 className="news-articles-frontend-page__caption-page">Новости компании</h4>
                    <ListItems
                        entityName='NewsArticle'
                        urlGetRequest = {NEWS_ARTICLE_URL_REQUEST}
                        urlContentView = {NEWS_ARTICLE_FRONTEND_VIEW_ROUTE}
                    />
                </div>
            </div>
        )
    }
};