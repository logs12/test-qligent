import './style.scss';

import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import Carousel from '../../../../common/widgets/flex-carousel';
import CustomerRequestDialog from '../../../components/CustomerRequestDialog';
import Form from '../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget  from '../../../../common/widgets/button-loading-widget';
import ImageBrokenLinkFilterComponent from '../../../../common/components/ImageBrokenLinkFilterComponent/index';

import { CUSTOMER_REQUEST_URL_REQUEST } from '../../../../common/constants/index';

import { BasePageView } from '../../../../common/decorators/BasePageView';

class CottageHousesLandAdvertisementViewFrontendPage extends Component {

    renderImageCottageHousesLandAdvertisement(photos) {
        return photos.map((photo, index) => {
            return <ImageBrokenLinkFilterComponent
                key={index}
                urlImage={photo.path}
                noImageUrl="/images/no-photo.png"
            />;
        });
    }

    render() {

        const { model } = this.props;

        return (
            <div className="cottage-houses-land-advertisement-view-page">
                {
                    (!isEmpty(model)) ?
                        <div className="cottage-houses-land-advertisement-view-page__container content-grid">

                            <div className="cottage-houses-land-advertisement-view-page__text-block-title">
                                <h5>{model.title}</h5>
                            </div>
                            <div className="cottage-houses-land-advertisement-view-page__address">
                                <i className="fa fa-map-marker" aria-hidden="true"></i>
                                {model.city}, {model.sub_locality_name}, {model.address}
                            </div>

                            <div className="mdl-grid">

                                <div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                    <div className="cottage-houses-land-advertisement-view-page__carousel-container" >
                                        <Carousel
                                            autoplayInteval={4500}
                                            indicator={true}
                                            switcher={true}>
                                            {this.renderImageCottageHousesLandAdvertisement(model.photos)}
                                        </Carousel>
                                    </div>
                                </div>
                                <div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                    <div className="cottage-houses-land-advertisement-view-page__prices">
                                        <p>СТОИМОСТЬ: {model.price} {model.currency}</p>
                                        <p>Цена за м²: {Math.floor(model.price / model.total_area)} {model.currency}  </p>
                                    </div>
                                  
                                    <CustomerRequestDialog
                                        buttonTitle="Записаться на просмотр"
                                        dialogTitle="Заказать звонок"
                                        entityName="CustomerRequest"
                                        messageSuccess="Спасибо за заявку."
                                    >
                                        <Form
                                            actionName="baseCreateAction"
                                            entityName="CustomerRequest"
                                            initModel={{
                                                'name': null,
                                                'phone': null,
                                            }}
                                            url={CUSTOMER_REQUEST_URL_REQUEST()}
                                        >
                                            <TextInputWidget
                                                name = 'name'
                                                label = 'Ваше имя'
                                                initFocused
                                            />
                                            <TextInputWidget
                                                name = 'phone'
                                                label = 'Контактный телефон'
                                            />

                                            <ButtonLoadingWidget
                                                label="Отправить"
                                            />
                                        </Form>
                                    </CustomerRequestDialog>
                                    <table className="cottage-houses-land-advertisement-view-page__object-params">
                                        <tbody>
                                            <tr className="cottage-houses-land-advertisement-view-page__object-param-item">
                                                <td>Этаж/Этажность: </td>
                                                <td className="cottage-houses-land-advertisement-view-page__object-param-value">
                                                    {Math.floor(model.number_floor/model.floors_total)}
                                                </td>
                                            </tr>

                                            <tr className="cottage-houses-land-advertisement-view-page__object-param-item">
                                                <td>Количество комнат: </td>
                                                <td className="cottage-houses-land-advertisement-view-page__object-param-value">
                                                    {model.count_rooms}
                                                </td>
                                            </tr>

                                            <tr className="cottage-houses-land-advertisement-view-page__object-param-item">
                                                <td>Серия дома: </td>
                                                <td className="cottage-houses-land-advertisement-view-page__object-param-value">
                                                    {model.building_series}
                                                </td>
                                            </tr>

                                            <tr className="cottage-houses-land-advertisement-view-page__object-param-item">
                                                <td>Общая площадь: </td>
                                                <td className="cottage-houses-land-advertisement-view-page__object-param-value">
                                                    {model.total_area} м²
                                                </td>
                                            </tr>

                                            <tr className="cottage-houses-land-advertisement-view-page__object-param-item">
                                                <td>Площадь кухни: </td>
                                                <td className="cottage-houses-land-advertisement-view-page__object-param-value">
                                                    {model.kitchen_space} м²
                                                </td>
                                            </tr>

                                            <tr className="cottage-houses-land-advertisement-view-page__object-param-item">
                                                <td>Жилая площадь: </td>
                                                <td className="cottage-houses-land-advertisement-view-page__object-param-value">
                                                    {model.living_space} м²
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                   
                                    
                                </div>
                                <div className="cottage-houses-land-advertisement-view-page__content"
                                     dangerouslySetInnerHTML={{__html: model.description}}>
                                </div>
                            </div>
                        </div>
                    : null
                }
            </div>
        )
    }
}

export default BasePageView(
    CottageHousesLandAdvertisementViewFrontendPage,
    'CottageHousesLandAdvertisement',
);