import './style.scss';

import React, { Component } from 'react';

import AdvertisementListItems from '../../components/AdvertisementListItems';

import Filter from '../../components/Filter';

import {
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST,
} from "../../../common/constants";


export default class CottageHousesLandAdvertisementsFrontendPage extends Component{

    render()  {

        return (
            <div className="cottage-houses-land-advertisements-frontend-page">
                <div className="content-grid">
                    {/*<h4 className="apartment-advertisements-frontend-page__caption-page">Список квартир</h4>*/}

                    <Filter
                        entityName='CottageHousesLandAdvertisement'
                        urlGetRequest={COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST}
                        gridScheme={[12,3]}
                        filterFields={{
                            'address': {
                                typeField: 'text',
                                label: 'Район, улица',
                            },

                            'price_from': {
                                typeField: 'text',
                                label: 'Цена от (руб.)',
                            },
                            'price_to': {
                                typeField: 'text',
                                label: 'Цена до (руб.)',
                            },
                            'area_from': {
                                typeField: 'text',
                                label: 'Площадь от (м²)',
                            },
                            'area_to': {
                                typeField: 'text',
                                label: 'Площадь до (м²)',
                            },
                        }}
                    />
                    
                    <AdvertisementListItems
                        entityName='CottageHousesLandAdvertisement'
                        urlGetRequest={COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST}
                        urlContentView = {COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE}
                    />
                </div>
            </div>
        )
    }
};