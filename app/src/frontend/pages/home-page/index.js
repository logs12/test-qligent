import './style.scss';

import React, { Component } from 'react';
import Carousel from '../../../common/widgets/flex-carousel';
import { BasePageView } from '../../../common/decorators/BasePageView';

import BestDeals from '../../components/BestDeals';


import { PAGE_URL_REQUEST } from "../../../common/constants";

class HomeFrontendPage extends Component{
    render()  {
        const { model } = this.props;
        return (
            <div className="home-frontend-page" >
                <div className="content-grid">
                    {
                        (model) ?
                            <Carousel
                                autoplayInteval={4500}
                                indicator={true}
                                switcher={true}>
                                {
                                     model.files.map((file, index) => {
                                        return <img key={index} src={file.path} />;
                                     })
                                }
                            </Carousel>
                        : null
                    }
                    <BestDeals />
                </div>
            </div>
        )
    }
};

export default BasePageView(HomeFrontendPage, 'Page', PAGE_URL_REQUEST, 1);