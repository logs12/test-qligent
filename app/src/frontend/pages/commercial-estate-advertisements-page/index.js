import './style.scss';

import React, { Component } from 'react';

import AdvertisementListItems from '../../components/AdvertisementListItems';

import Filter from '../../components/Filter';

import {
    COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST,
} from "../../../common/constants";


export default class CommercialEstateAdvertisementsFrontendPage extends Component{

    render()  {

        return (
            <div className="commercial-estate-advertisements-frontend-page">
                <div className="content-grid">
                    {/*<h4 className="commercial-estate-advertisements-frontend-page__caption-page">Список квартир</h4>*/}

                    <Filter
                        entityName='CommercialEstateAdvertisement'
                        urlGetRequest={COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST}
                        gridScheme={[12,3]}
                        filterFields={{
                            'address': {
                                typeField: 'text',
                                label: 'Район, улица',
                            },

                            'price_from': {
                                typeField: 'text',
                                label: 'Цена от (руб.)',
                            },
                            'price_to': {
                                typeField: 'text',
                                label: 'Цена до (руб.)',
                            },
                            'area_from': {
                                typeField: 'text',
                                label: 'Площадь от (м²)',
                            },
                            'area_to': {
                                typeField: 'text',
                                label: 'Площадь до (м²)',
                            },
                        }}
                    />
                    
                    <AdvertisementListItems
                        entityName='CommercialEstateAdvertisement'
                        urlGetRequest={COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST}
                        urlContentView = {COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE}
                    />
                </div>
            </div>
        )
    }
};