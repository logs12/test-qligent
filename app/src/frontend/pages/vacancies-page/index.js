import './style.scss';

import React, { Component } from 'react';
import { BasePageView } from '../../../common/decorators/BasePageView';
import { PAGE_URL_REQUEST } from "../../../common/constants";

class VacanciesFrontendPage extends Component{

    render()  {
        const { model } = this.props;
        return (
            <div className="vacancies-frontend-page" >
                    {
                        model ?
                            <div className="content-grid mdl-grid">
                                <h4 className="vacancies-frontend-page__caption-page">{model.title}</h4>
                                <div dangerouslySetInnerHTML={{__html: model.content}}></div>
                            </div>
                        : null
                    }
            </div>
        )
    }
};

export default BasePageView(VacanciesFrontendPage, 'Page', PAGE_URL_REQUEST, 3);