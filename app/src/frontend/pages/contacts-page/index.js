import './style.scss';

import React, { Component } from 'react';

export default class ContactsFrontendPage extends Component{

    componentDidMount() {
        this.initMap();
    }

    initMap() {

        let centerLatLng = new google.maps.LatLng(56.313508, 43.979079);

        // Обязательные опции с которыми будет проинициализированна карта
        let mapOptions = {
            center: centerLatLng, // Координаты центра мы берем из переменной centerLatLng
            zoom: 15,               // Зум по умолчанию. Возможные значения от 0 до 21
            styles: [
                {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
                {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
                {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
                {"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}
            ],
            'scrollwheel': false,
        };

        // Создаем карту внутри элемента #map
        let map = new google.maps.Map(document.getElementById("map"), mapOptions);
        new google.maps.Marker({
            position: centerLatLng,
            map: map,
            title: 'Альфа-Регион Инвест',
        });
    }

    render()  {
        return (
            <div className="contacts-frontend-page" >
                <div className="content-grid">
                    <h4 className="contacts-frontend-page__caption-page">Контакты</h4>
                    <div className="contacts-frontend-page__address-text">
                        <i className="fa fa-map-marker" aria-hidden="true"></i>
                        Альфа-Регион Инвест, 603000, г. Нижний Новгород ул. Ереванская, 8/16, оф. 202
                    </div>
                    <div id="map" className="contacts-frontend-page__map"></div>
                </div>
            </div>
        )
    }
};
