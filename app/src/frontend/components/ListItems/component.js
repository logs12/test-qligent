import React from 'react';

const ListItemsComponent = (props) => {

    const { collection, handleClickButtonReadMore } = props;

    return (
        <div className="list-items-container">
            {ListItemsComponent.renderListItem(collection, handleClickButtonReadMore)}
        </div>
    )
};

ListItemsComponent.renderImageBlock = (content) => {
    return <img src={content.photos[0].path} />;
};

ListItemsComponent.renderTextBlock = (content, handleClickButtonReadMore) => {
    return  <div className="list-items__text-block">
                <div className="list-items__text-block-title">
                    <h6>{content.title}</h6>
                    <span>{content.created}</span>
                </div>
                <span className="mdl-list__item-text-body"
                      dangerouslySetInnerHTML={{__html: content.content}}>
                </span>

                <div className="list-items__actions-block">
                    <button className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-list__item-secondary-action"
                            onClick={(event) => {handleClickButtonReadMore(event, content.id)}}
                    >
                        Подробнее
                    </button>
                </div>
            </div>
};

ListItemsComponent.renderListItem = (collection, handleClickButtonReadMore) => {
    let listItemsElement = [];
    collection.forEach((content, index) => {
        if (content.photos.length) {
            listItemsElement.push(
                <div key={index} className="mdl-grid">
                    <div className="mdl-cell mdl-cell--3-col">
                        {ListItemsComponent.renderImageBlock(content)}
                    </div>
                    <div className="mdl-cell mdl-cell--9-col">
                        {ListItemsComponent.renderTextBlock(content, handleClickButtonReadMore)}
                    </div>
                </div>
            );
        } else {
            listItemsElement.push(
                <div key={index} className="mdl-grid">
                    <div className="mdl-cell mdl-cell--12-col">
                        {ListItemsComponent.renderTextBlock(content, handleClickButtonReadMore)}
                    </div>
                </div>
            );
        }
    });

    return listItemsElement;
};

export default ListItemsComponent;