import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../../common/actions';
import { push } from 'react-router-redux';

import ListItemsComponent from './component';

/**
 * HOC component container
 * @param entityName
 * @param actionName
 * @param urlContentView
 * @param urlGetRequest
 * @returns {Container}
 * @constructor
 */
export default function ListItemContainerHoc(
    entityName,
    actionName,
    urlContentView,
    urlGetRequest,
) {

    const mapStateToProps = (state, ownProps) => ({
        urlContentView: urlContentView,
        collection: state[entityName].collection,
        condition: state.routing.locationBeforeTransitions.search,
        urlGetRequest: urlGetRequest,
        actionName: actionName,
        entityName: entityName,
    });

    const mapDispatchToProps = (dispatch) => ({
        getCollection: bindActionCreators(actions[actionName], dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class Container extends Component {

        getActionName(options) {
            if (this.props.actionName === 'baseGetAction') {
                return this.props.getCollection(this.props.urlGetRequest, options, this.props.entityName);
            }
            return this.props.getCollection(options);
        }

        componentWillMount() {
            this.getActionName(
                {condition: this.props.condition}
            );
        }

        /**
         *
         * @param {Object} nextProps
         */
        componentWillReceiveProps(nextProps) {
            if (nextProps.condition !== this.props.condition) {
                this.getActionName(
                    {condition: nextProps.condition}
                )
            }
        }

        handleClickButtonReadMore(event, id) {
            event.preventDefault();
            this.props.pushToRouter(this.props.urlContentView(id))
        }

        render() {

            const { collection } = this.props;

            return(
                <ListItemsComponent
                    collection={collection}
                    handleClickButtonReadMore={::this.handleClickButtonReadMore}
                />
            )
        }

    }
    return Container;
}

