import './style.scss';

import React, { createElement } from 'react';
import classNames from 'classnames';
import PaginationWidget from '../../../common/widgets/pagination-widget/container';
import ListItemContainerHoc from './container';

const ListItems = props => {

    const {
        entityName,
        actionName,
        urlContentView,
        className,
        urlGetRequest,
    } = props;

    const defaultClassName = 'list-items';

    const classNameMainContainer = classNames(defaultClassName, className);

    const paginationClassName = `${defaultClassName}__pagination`;

    return (

        <div className={classNameMainContainer}>

            {createElement(
                ListItemContainerHoc(
                    entityName,
                    actionName,
                    urlContentView,
                    urlGetRequest,
                )
            )}

            <div className={paginationClassName} >
                {
                    createElement(PaginationWidget({
                        entityName: entityName,
                    }))
                }
            </div>
        </div>
    )
};

ListItems.defaultProps = {
    actionName: 'baseGetAction',
};

export default ListItems;