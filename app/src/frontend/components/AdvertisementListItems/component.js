import React from 'react';
import ImageBrokenLinkFilterComponent from '../../../common/components/ImageBrokenLinkFilterComponent/index';

const AdvertisementListItemsComponent = (props) => {

    const { collection, handleClickButtonReadMore } = props;

    return (
        <div className="advertisement-list-items-container">
            {AdvertisementListItemsComponent.renderListItem(collection, handleClickButtonReadMore)}
        </div>
    )
};

AdvertisementListItemsComponent.renderListItem = (collection, handleClickButtonReadMore) => {
    let listItemsElement = [];


    collection.forEach((content, index) => {

        let urlImage = null;
        if (content.photos.length && content.photos[0].hasOwnProperty('path')) {
            urlImage = content.photos[0].path;
        }

        listItemsElement.push(
            <div key={index} className="advertisement-list-item mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
                <div className="mdl-cell mdl-cell--4-col mdl-cell--4-col-desktop mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                    <div className="advertisement-list-item__first-block">
                        <ImageBrokenLinkFilterComponent
                            urlImage={urlImage}
                            noImageUrl="images/no-photo.png"
                        />
                    </div>
                </div>
                <div className="mdl-cell mdl-cell--7-col mdl-cell--6-col-desktop mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                    <div className="advertisement-list-item__two-block">
                        <div className="advertisement-list-item__text-block-title">
                            <h6>{content.title}</h6>
                        </div>
                        <div className="advertisement-list-item__address">
                            <i className="fa fa-map-marker" aria-hidden="true"></i>
                            {content.city}, {content.sub_locality_name}, {content.address}
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <div><b>Площадь: </b>{content.total_area} м²</div>
                                <div><b>Цена за м²: </b>{Math.floor(content.price / content.total_area)} {content.currency}</div>
                            </div>

                            <div className="mdl-cell mdl-cell--6-col">
                                <div><b>Этаж/Этажность: </b> {content.number_floor}/{content.floors_total}</div>
                                <div><b>Количество комнат: </b> {content.count_rooms}</div>
                            </div>
                        </div>

                        <div className="advertisement-list-item__description"
                              dangerouslySetInnerHTML={{__html: content.description}}>
                        </div>
                    </div>
                </div>
                <div className="mdl-cell mdl-cell--1-col mdl-cell--2-col-desktop mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                    <div className="advertisement-list-item__free-block">
                        <div className="advertisement-list-item__free-block-price">
                            <b>{content.price} {content.currency}</b>
                        </div>
                        <button className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-list__item-secondary-action"
                                onClick={(event) => {handleClickButtonReadMore(event, content.id)}}
                        >
                            Подробнее
                        </button>
                    </div>
                </div>
            </div>
        );
    });

    return listItemsElement;
};

export default AdvertisementListItemsComponent;