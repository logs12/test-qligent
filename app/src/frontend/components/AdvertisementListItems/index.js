import './style.scss';

import React, { createElement } from 'react';
import classNames from 'classnames';
import PaginationWidget from '../../../common/widgets/pagination-widget/container';
import AdvertisementListItemsContainerHoc from './container';

const AdvertisementListItems = props => {

    const {
        entityName,
        actionName,
        urlContentView,
        className,
        urlGetRequest,
    } = props;

    const defaultClassName = 'advertisement-list-items';

    const classNameMainContainer = classNames(defaultClassName, className);

    const paginationClassName = `${defaultClassName}__pagination`;

    return (

        <div className={classNameMainContainer}>

            {createElement(
                AdvertisementListItemsContainerHoc(
                    entityName,
                    actionName,
                    urlContentView,
                    urlGetRequest,
                )
            )}

            <div className={paginationClassName} >
                {
                    createElement(PaginationWidget({
                        entityName: entityName,
                    }))
                }
            </div>
        </div>
    )
};

AdvertisementListItems.defaultProps = {
    actionName: 'baseGetAction',
};

export default AdvertisementListItems;