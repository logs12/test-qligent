import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import isArray from 'lodash/isArray';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as actions from '../../../common/actions';
import { push } from 'react-router-redux';

import FilterComponent from './component';

/**
 * HOC component container
 * @param entityName
 * @param actionName
 * @param filterFields
 * @param gridScheme
 * @param urlGetRequest
 * @returns {Container}
 * @constructor
 */
export default function FilterContainerHoc(
    entityName,
    actionName,
    filterFields,
    gridScheme,
    urlGetRequest,
) {

    const mapStateToProps = (state) => ({
        filterFields:filterFields,
        url: state.routing.locationBeforeTransitions.pathname,
        gridScheme:gridScheme,
        urlGetRequest:urlGetRequest,
        actionName:actionName,
        entityName:entityName,
    });

    const mapDispatchToProps = (dispatch) => ({
        getCollection: bindActionCreators(actions[actionName], dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class Container extends Component {

        inputTimer = null;

        componentWillMount() {

            let filterFields = {};

            for (let filterField in this.props.filterFields) {
                if (this.props.filterFields.hasOwnProperty(filterField)) {
                    filterFields[filterField] = null;
                }
            }

            this.setState({ 'filterFields': filterFields });
        }

        getActionName(options) {
            if (this.props.actionName === 'baseGetAction') {
                return this.props.getCollection(this.props.urlGetRequest, options, this.props.entityName);
            }
            return this.props.getCollection(options);
        }

        handleChange(targetValue, filterFieldChange) {

            let { filterFields } = this.state;
            let filterGetParametres = '?';
            for (let filterField in filterFields) {
                if (filterFields.hasOwnProperty(filterField)) {
                    if (filterFields[filterField]) {
                        if (filterField === filterFieldChange) {
                            filterFields[filterField] = targetValue;
                            filterGetParametres += this.renderGetParametres(filterField, targetValue);
                        } else {
                            filterGetParametres += this.renderGetParametres(filterField, filterFields[filterField]);
                        }
                    } else {
                        if (filterField === filterFieldChange) {
                            filterFields[filterField] = targetValue;
                            filterGetParametres += this.renderGetParametres(filterField, targetValue);
                        }
                    }
                }
            }

            clearTimeout(this.inputTimer);
            this.inputTimer = setTimeout(() => {
                this.getActionName({'condition':filterGetParametres});
                this.setState({ 'filterFields': filterFields });
            }, 400);

        }

        /**
         * Render get parametres
         * @param nameField
         * @param value
         * @returns {string}
         */
        renderGetParametres(nameField, value) {
            if (!isEmpty(value)) {
                if (isArray(value)) {
                    let filterGetParametres = '';
                    value.forEach((value) => {
                        filterGetParametres += `${encodeURIComponent(`condition[${nameField}][]`)}=${encodeURIComponent(value)}&`;
                    });
                    return filterGetParametres;
                }
                return `${encodeURIComponent(`condition[${nameField}]`)}=${encodeURIComponent(value)}&`;
            }
            return '';
        }


        render() {

            const { gridScheme, filterFields } = this.props;

            return(
                <FilterComponent
                    handleChange={::this.handleChange}
                    gridScheme={gridScheme}
                    filterFields={filterFields}
                />
            )
        }

    }
    return Container;
}

