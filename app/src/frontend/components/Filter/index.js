import './style.scss';

import React, { createElement, defaultProps } from 'react';
import classNames from 'classnames';
import FilterContainerHoc from './container';

const Filter = props => {

    const {
        entityName,
        actionName,
        className,
        filterFields,
        gridScheme,
        urlGetRequest,
    } = props;

    const defaultClassName = 'filter mdl-shadow--2dp';

    const classNameMainContainer = classNames(defaultClassName, className);

    return (

        <div className={classNameMainContainer}>
            {createElement(
                FilterContainerHoc(
                    entityName,
                    actionName,
                    filterFields,
                    gridScheme,
                    urlGetRequest,
                )
            )}
        </div>
    )
};

Filter.defaultProps = {
    actionName: 'baseGetAction',
};

export default Filter;