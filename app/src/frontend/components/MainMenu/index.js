import './styles/style.scss';

import React, { Component } from 'react';
import FlexMenu from '../../../common/widgets/flex-menu';

import {
    NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
    APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE,
    ABOUT_FRONTEND_ROUTE,
    NEWS_ARTICLE_FRONTEND_ROUTE,
    VACANCIES_FRONTEND_ROUTE,
    CONTACTS_FRONTEND_ROUTE,
} from '../../../common/constants';


export default class MainMenu extends Component {

    constructor(props, context) {
        super(props, context);
        this.brand = {
            url: '/',
            title: 'Автозапчасти'
        };
        this.links = [
            {
                url: '/',
                title: 'Главная',
                onlyActiveOnIndex: true,
            },
            {
                url: '/',
                title: 'Объекты недвижимости',
                onlyActiveOnIndex: false,
                subItems: [
                     {
                         url: NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
                         title: 'Новостройки',
                         onlyActiveOnIndex: false,
                     },
                     {
                         url: APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
                         title: 'Вторичное жилье',
                         onlyActiveOnIndex: false,
                     },
                     {
                         url: COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE,
                         title: 'Дома, дачи, коттеджи, участки',
                         onlyActiveOnIndex: false,
                     },
                     {
                         url: COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE,
                         title: 'Коммерческая недвижимость',
                         onlyActiveOnIndex: false,
                     },
                ]
            },
            {
                url: ABOUT_FRONTEND_ROUTE,
                title: 'О компании',
                onlyActiveOnIndex: false,
            },
            {
                url: NEWS_ARTICLE_FRONTEND_ROUTE,
                title: 'Новости',
                onlyActiveOnIndex: false,
            },
            {
                url: VACANCIES_FRONTEND_ROUTE,
                title: 'Работа в агентстве',
                onlyActiveOnIndex: false,
            },

            {
                url: CONTACTS_FRONTEND_ROUTE,
                title: 'Контакты',
                onlyActiveOnIndex: false,
            },

        ];

        this.state = {
            open: false
        }
        
    }

    render() {
        return (
                <div className="main-menu content-grid mdl-grid">
                    {/* Логотип */}

                    <FlexMenu links = {this.links}
                              linkTextMore="Меню"/>
                </div>
        )
    }
}