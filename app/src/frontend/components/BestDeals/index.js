import './style.scss';

import React, { Component } from 'react';
import { BaseListItems } from '../../../common/decorators/BaseListItems';

import {
    APARTMENT_ADVERTISEMENT_URL_REQUEST,
    APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
} from '../../../common/constants';

class BestDeals extends Component {

    handleClickButtonReadMore(event, id) {
        event.preventDefault();
        this.props.pushToRouter(APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(id))
    }

    /**
     * Generate content containers
     * @param {array} collection
     * @returns {Array}
     */
    generateContentContainers(collection) {

        let contentContainer = [];
        const countRow = 3;
        collection.forEach((model, index) => {
            if (index % countRow === 0) {
                contentContainer.push(
                    <div key={index} className="mdl-grid">
                        {this.generateContentItem(collection, index, countRow)}
                    </div>
                );
            }
        });
        return contentContainer;
    }

    /**
     * Generate content item
     * @param {array} collection
     * @param {number} index
     * @param {number} countRow
     * @returns {Array}
     */
    generateContentItem(collection, index, countRow) {

        let contentContainerItem = [];
        let countIteration = index+countRow;
        if (!collection[countIteration]) {
            countIteration = collection.length;
        }

        for (let i = index; i < countIteration; i++) {
            contentContainerItem.push(
                <div key={i} className="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--3-col">
                    <div className="mdl-card__title">
                        <h2 className="mdl-card__title-text">{collection[i].title}</h2>
                    </div>
                    <div className="mdl-card__supporting-text">
                        <div><b>Этаж/Этажность: </b> {collection[i].number_floor}/{collection[i].floors_total}</div>
                        <div><b>Количество комнат: </b> {collection[i].count_rooms}</div>
                    </div>
                    <div className="mdl-card__menu">
                        <button className="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                            <i className="material-icons">share</i>
                        </button>
                    </div>

                    <div className="mdl-card__footer mdl-card--border">
                        <div className="mdl-card__footer-left-area">{collection[i].price}</div>
                        <div className="mdl-card__footer-right-area">
                            <button className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-list__item-secondary-action"
                                    onClick={(event) => {this.handleClickButtonReadMore(event, collection[i].id)}}
                            >
                                Подробнее
                            </button>
                        </div>
                    </div>
                </div>
            );
        }

        return contentContainerItem;
    }

    render () {

        const { collection } = this.props;
        return (
            <div className="best-deals">
                    {this.generateContentContainers(collection)}
            </div>
        )
    }
}

BestDeals.defaultProps = {
    actionName: 'baseGetAction',
};


export default BaseListItems(
    BestDeals,
    'ApartmentAdvertisement',
    APARTMENT_ADVERTISEMENT_URL_REQUEST,

);