import React, { Component } from 'react'
import { connect } from 'react-redux';

export default function ItemBreadcrumbHoc (
    entityName,
) {
    const mapStateToProps = (state, ownProps) => ({
        entityName: entityName,
        model: state[entityName].model,
    });

    @connect(mapStateToProps)

    class ItemBreadcrumbContainer extends Component {

        render () {
            const { model } = this.props;
            if (!model['values']) {
                return <div><i className='fa fa-refresh fa-spin' /></div>
            }
            return <i>{model['values'].title}</i>
        }
    }

    return ItemBreadcrumbContainer;
}