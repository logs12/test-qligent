import React, { Component } from 'react';
import classNames from 'classnames';

import { connect } from 'react-redux';
import Button from 'react-mdl/lib/Button';

import  { Dialog, DialogTitle, DialogContent, DialogActions } from 'react-mdl/lib/Dialog';

import {
    FORM_WIDGET_SUCCESS,
    FORM_WIDGET_INIT
} from '../../../common/constants';

export default function ContainerHoc(
    children,
    buttonTitle,
    dialogTitle,
    entityName,
    messageSuccess,
    buttonOptions,
) {
    const mapStateToProps = (state) => {
        return{
            buttonTitle: buttonTitle,
            dialogTitle: dialogTitle,
            entityName: entityName,
            stateForm: state.common.formWidget.hasOwnProperty(entityName) ? state.common.formWidget[entityName].stateForm : null,
            children: children,
            messageSuccess: messageSuccess,
            buttonOptions: buttonOptions,
        }
    };

    @connect(mapStateToProps)
    class Container extends Component {

        state = {
            openDialog: false,
            messageSuccess: null,
        };

        handleOpenDialog() {
            this.setState({
                openDialog: true
            });
        }

        handleCloseDialog() {
            this.setState({
                openDialog: false,
            });
        }

         componentWillReceiveProps(nextProps) {
             if (nextProps.stateForm  === FORM_WIDGET_SUCCESS) {
                 this.setState({
                     messageSuccess: this.props.messageSuccess,
                 }, () => {
                     setTimeout(() => {
                        this.handleCloseDialog();
                     },2000)
                 });
             }
         }

        shouldComponentUpdate(nextProps) {
            if (nextProps.stateForm  === FORM_WIDGET_SUCCESS || nextProps.stateForm === FORM_WIDGET_INIT) {
                return true;
            }
            return false;
        }
        
        renderMessageSuccessContainer(messageSuccess) {
            return <div className="dialog__message-success">{messageSuccess}</div>
        }

        render() {

            const { buttonTitle, dialogTitle, children, buttonOptions } = this.props;
            const { messageSuccess } = this.state;
            return (
                <div className="customer-request-dialog">
                    <Button className="dialog__button"
                            onClick={::this.handleOpenDialog}
                            { ...buttonOptions }

                    >
                        {buttonTitle}
                    </Button>
                    <Dialog open={this.state.openDialog} ref={(dialog) => { this.dialog = dialog; }}>
                    <span className="dialog__button-close"
                          onClick={::this.handleCloseDialog}>X
                    </span>
                        <DialogTitle>{dialogTitle}</DialogTitle>
                        <DialogContent>
                            { messageSuccess ?
                                this.renderMessageSuccessContainer(messageSuccess) 
                            : children }
                        </DialogContent>
                    </Dialog>
                </div>
            );
        }
    }

    return Container;
}

