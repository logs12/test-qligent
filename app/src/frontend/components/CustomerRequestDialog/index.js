import './style.scss';

import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import ContainerHoc from './container';

export default class CustomerRequestDialog extends Component {

    static defaultProps = {
        buttonTitle: null,
        dialogTitle: null,
        entityName: null,
        messageSuccess: null,
        buttonOptions: {},
    };

    static propTypes = {
        buttonTitle: PropTypes.string.isRequired,
        dialogTitle: PropTypes.string.isRequired,
        messageSuccess: PropTypes.string,
    };


    render () {
        const {
            children,
            buttonTitle,
            dialogTitle,
            entityName,
            messageSuccess,
            buttonOptions,
            buttonClassName,
        } = this.props;

        return  createElement(ContainerHoc(
            children,
            buttonTitle,
            dialogTitle,
            entityName,
            messageSuccess,
            buttonOptions,
            buttonClassName,
        ))
    }
};