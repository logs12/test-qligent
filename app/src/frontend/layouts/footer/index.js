import './style.scss';

import React, {Component} from 'react';

import NavLink from '../../../common/widgets/nav-link/component';

import {
    APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
    NEWS_ARTICLE_FRONTEND_ROUTE,
    VACANCIES_FRONTEND_ROUTE,
    CONTACTS_FRONTEND_ROUTE,
} from '../../../common/constants';

class Footer extends Component {

    render() {

        return (
            <footer className="mdl-mega-footer">
                <div className="footer__content">
                    <div className="mdl-mega-footer__middle-section">

                        <div className="mdl-mega-footer__drop-down-section">
                            <input className="mdl-mega-footer__heading-checkbox" type="checkbox" defaultChecked />
                                <h1 className="mdl-mega-footer__heading">Категории</h1>
                                <ul className="mdl-mega-footer__link-list">
                                    <li>
                                        <NavLink
                                            onlyActiveOnIndex={true}
                                            to={APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE}>
                                            Объекты недвижимости
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink
                                            onlyActiveOnIndex={false}
                                            to={NEWS_ARTICLE_FRONTEND_ROUTE}>
                                            Новости, статьи
                                        </NavLink>
                                    </li>

                                    <li>
                                        <NavLink
                                            onlyActiveOnIndex={false}
                                            to={VACANCIES_FRONTEND_ROUTE}>
                                            Работа в агентстве
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink
                                            onlyActiveOnIndex={false}
                                            to={CONTACTS_FRONTEND_ROUTE}>
                                            Контакты
                                        </NavLink>
                                    </li>
                                </ul>
                        </div>

                        {/*<div className="mdl-mega-footer__drop-down-section">
                            <input className="mdl-mega-footer__heading-checkbox" type="checkbox" defaultChecked />
                                <h1 className="mdl-mega-footer__heading">Моя учетная запись</h1>
                                <ul className="mdl-mega-footer__link-list">
                                    <li><a href="#">Мои заказы</a></li>
                                    <li><a href="#">Мои платёжные квитанции</a></li>
                                    <li><a href="#">Мои адреса</a></li>
                                    <li><a href="#">Моя личная информация</a></li>
                                </ul>
                        </div>*/}

                        <div className="mdl-mega-footer__drop-down-section">

                            <h1 className="mdl-mega-footer__heading">Контактная информация</h1>
                            <div className="footer__contacts">
                                <p>
                                    <i className="fa fa-map-marker" aria-hidden="true"></i>
                                    Альфа-Регион Инвест, 603000, г. Нижний Новгород ул. Ереванская, 8/16, оф. 202
                                </p>
                                <p>
                                    <i className="fa fa-phone" aria-hidden="true"></i> +7 (831) 4-19-19-25
                                </p>
                                <p>
                                    <i className="fa fa-envelope-o" aria-hidden="true"></i> alfa-ri@bk.ru
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </footer>
        );
    }
}

export default Footer;
