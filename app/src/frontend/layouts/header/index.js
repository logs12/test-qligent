import './style.scss';
import React, { Component } from 'react'

import  MainMenu  from '../../components/MainMenu';
import Form from '../../../common/widgets/form-widget';
import TextInputWidget from '../../../common/widgets/text-input-widget';
import ButtonLoadingWidget  from '../../../common/widgets/button-loading-widget';

import CustomerRequestDialog from '../../components/CustomerRequestDialog';

import { CUSTOMER_REQUEST_URL_REQUEST } from '../../../common/constants/index';

export default class Header extends Component {


    render() {
        return (
            <header>
                <div className="header-top">
                    <div className="header-top__container content-grid mdl-grid">
                        <div className="mdl-cell mdl-cell--9-col">
                                <img src="/images/logo.png" />
                        </div>
                        <div className="mdl-cell mdl-cell--3-col">
                            <div className="header-top__contacts">
                                <i className="header-top__icon-phone fa fa-phone" aria-hidden="true"></i>
                                8 (831) 4-19-19-25, 8-906-578-07-22,
                            </div>
                            <CustomerRequestDialog
                                buttonTitle="Заказать звонок"
                                buttonClassName = "header-top__button-dialog"
                                dialogTitle="Заказать звонок"
                                entityName="CustomerRequest"
                                messageSuccess="Спасибо за заявку."
                            >
                                <Form
                                    actionName="baseCreateAction"
                                    entityName="CustomerRequest"
                                    initModel={{
                                        'name': null,
                                        'phone': null,
                                    }}
                                    url={CUSTOMER_REQUEST_URL_REQUEST()}
                                >
                                    <TextInputWidget
                                        name = 'name'
                                        label = 'Ваше имя'
                                        initFocused
                                    />
                                    <TextInputWidget
                                        name = 'phone'
                                        label = 'Контактный телефон'
                                    />

                                    <ButtonLoadingWidget
                                        label="Отправить"
                                    />
                                </Form>
                            </CustomerRequestDialog>
                        </div>
                    </div>
                </div>
                <div className="header__menu">
                    <MainMenu />
                </div>
            </header>
        );
    }
}