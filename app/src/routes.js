import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { render } from 'react-dom';

// Layouts Frontend
import FrontendLayout  from './frontend/layouts/main';

// Layouts Backend
import MainPageLayout from './backend/layouts/MainPageLayout';

// Backend components
import DashboardPage from './backend/pages/dashboard-page';
import AdministrationPage from './backend/menu/administration';
import UsersPage from './backend/menu/administration/users';
import UserViewPage from './backend/menu/administration/users/user';
import UserFormPage from './backend/menu/administration/users/user-form';

import ApartmentAdvertisementsPage from './backend/menu/advertisements/apartment-advertisements';
import ApartmentAdvertisementViewPage from './backend/menu/advertisements/apartment-advertisements/apartment-advertisement';
import ApartmentAdvertisementFormPage from './backend/menu/advertisements/apartment-advertisements/apartment-advertisement-form';

import CommercialEstateAdvertisementsPage from './backend/menu/advertisements/commercial-estate-advertisements';
import CommercialEstateAdvertisementViewPage from './backend/menu/advertisements/commercial-estate-advertisements/commercial-estate-advertisement';
import CommercialEstateAdvertisementFormPage from './backend/menu/advertisements/commercial-estate-advertisements/commercial-estate-advertisement-form';

import CottageHousesLandAdvertisementsPage from './backend/menu/advertisements/cottage-houses-land-advertisements';
import CottageHousesLandAdvertisementViewPage from './backend/menu/advertisements/cottage-houses-land-advertisements/cottage-houses-land-advertisement';
import CottageHousesLandAdvertisementFormPage from './backend/menu/advertisements/cottage-houses-land-advertisements/cottage-houses-land-advertisement-form';

import CustomerRequestsPage from './backend/menu/customer-requests';
import CustomerRequestViewPage from './backend/menu/customer-requests/customer-request';

import NewsArticlesPage from './backend/menu/news-articles';
import NewsArticleViewPage from './backend/menu/news-articles/news-article';
import NewsArticleFormPage from './backend/menu/news-articles/news-article-form'

import ImportReportsPage from './backend/menu/import-reports';
import ImportReportFormPage from './backend/menu/import-reports/import-report-form';

import PagesPage from './backend/menu/pages';
import PageFormPage from './backend/menu/pages/page-form';

// Frontend components
import FrontendHomeFrontendPage from './frontend/pages/home-page';
import AboutCompanyFrontendPage from './frontend/pages/about-company-page';
import NewsArticlesFrontendPage from './frontend/pages/news-articles-page';
import NewsArticleViewFrontendPage from './frontend/pages/news-articles-page/news-article';

import ApartmentAdvertisementsFrontendPage from './frontend/pages/apartment-advertisements-page';
import ApartmentAdvertisementViewFrontendPage from './frontend/pages/apartment-advertisements-page/apartment-advertisement';

import NewApartmentAdvertisementsFrontendPage from './frontend/pages/new-apartment-advertisements-page';
import NewApartmentAdvertisementViewFrontendPage from './frontend/pages/new-apartment-advertisements-page/new-apartment-advertisement';

import CottageHousesLandAdvertisementsFrontendPage from './frontend/pages/cottage-houses-land-advertisements-page';
import CottageHousesLandAdvertisementViewFrontendPage from './frontend/pages/cottage-houses-land-advertisements-page/cottage-houses-land-advertisement';

import CommercialEstateAdvertisementsFrontendPage from './frontend/pages/commercial-estate-advertisements-page';
import CommercialEstateAdvertisementViewFrontendPage from './frontend/pages/commercial-estate-advertisements-page/commercial-estate-advertisement';

import VacanciesFrontendPage from './frontend/pages/vacancies-page';
import ContactsFrontendPage from './frontend/pages/contacts-page';

// Common components
import LoginPage from './common/pages/login-page';
import SignUpPage from './common/pages/sign-up-page';
import ErrorPage from './common/pages/error-page';
import NotFoundPage from './common/pages/not-found-page';
import ApplicationContainer from './common/containers/ApplicationContainer';

// constants react-redux-router app
import {
    ADMIN_ROUTE,
    ADMINISTRATION_ROUTE,
    USERS_ROUTE,
    USER_VIEW_ROUTE,
    USER_CREATE_ROUTE,
    USER_UPDATE_ROUTE,
    USER_DELETE_ROUTE,

    APARTMENT_ADVERTISEMENTS_ROUTE,
    APARTMENT_ADVERTISEMENT_CREATE_ROUTE,
    APARTMENT_ADVERTISEMENT_VIEW_ROUTE,
    APARTMENT_ADVERTISEMENT_UPDATE_ROUTE,
    APARTMENT_ADVERTISEMENT_DELETE_ROUTE,
    APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,
    NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE,

    COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_DELETE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE,

    // News apartment-advertisement
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_DELETE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE,

    CUSTOMER_REQUESTS_ROUTE,
    CUSTOMER_REQUEST_VIEW_ROUTE,
    CUSTOMER_REQUEST_DELETE_ROUTE,

    NEWS_ARTICLE_ROUTE,
    NEWS_ARTICLE_CREATE_ROUTE,
    NEWS_ARTICLE_VIEW_ROUTE,
    NEWS_ARTICLE_UPDATE_ROUTE,
    NEWS_ARTICLE_DELETE_ROUTE,
    NEWS_ARTICLE_FRONTEND_VIEW_ROUTE,
    NEWS_ARTICLE_FRONTEND_ROUTE,

    IMPORT_REPORTS_ROUTE,
    IMPORT_REPORT_CREATE_ROUTE,
    IMPORT_REPORT_DELETE_ROUTE,

    ABOUT_FRONTEND_ROUTE,
    VACANCIES_FRONTEND_ROUTE,
    CONTACTS_FRONTEND_ROUTE,

    PAGES_ROUTE,
    PAGE_CREATE_ROUTE,
    PAGE_UPDATE_ROUTE,
    PAGE_DELETE_ROUTE,

} from './common/constants';

import ItemBreadcrumb from 'frontend/components/Breadcrumbs';

export const routes = (
    <Route>
        {/*Frontend*/}
        <Route path="/" component={ApplicationContainer(FrontendLayout, false)}>
            <IndexRoute component={FrontendHomeFrontendPage} breadcrumb={<i className='fa fa-home' />} />
            <Route path={APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE} component={ApartmentAdvertisementsFrontendPage} breadcrumb='вторичное жилье' />
            <Route path={APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={ApartmentAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="ApartmentAdvertisement" />}/>

            <Route path={NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE} component={NewApartmentAdvertisementsFrontendPage}  breadcrumb='новостройки' />
            <Route path={NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={NewApartmentAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="NewApartmentAdvertisement" />} />

            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE} component={CottageHousesLandAdvertisementsFrontendPage} breadcrumb='дома, дачи, коттеджи, участки' />
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={CottageHousesLandAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="CottageHousesLandAdvertisement" />} />

            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE} component={CommercialEstateAdvertisementsFrontendPage} breadcrumb='коммерческая недвижимость'/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE(':id')}
                   component={CommercialEstateAdvertisementViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="CommercialEstateAdvertisement" />} />

            <Route path={ABOUT_FRONTEND_ROUTE} component={AboutCompanyFrontendPage} breadcrumb='о компании'/>
            <Route path={NEWS_ARTICLE_FRONTEND_ROUTE} component={NewsArticlesFrontendPage} breadcrumb='новости'/>
            <Route path={NEWS_ARTICLE_FRONTEND_VIEW_ROUTE(':id')} component={NewsArticleViewFrontendPage}
                   breadcrumb={<ItemBreadcrumb entityName="NewsArticle" />} />
            <Route path={VACANCIES_FRONTEND_ROUTE} component={VacanciesFrontendPage} breadcrumb='вакансии'/>
            <Route path={CONTACTS_FRONTEND_ROUTE} component={ContactsFrontendPage} breadcrumb='контакты'/>
        </Route>

        {/*Backend*/}
        <Route path={ADMIN_ROUTE} component={ApplicationContainer(MainPageLayout)}>
            <IndexRoute component={DashboardPage}/>
            <Route path={ADMINISTRATION_ROUTE} component={AdministrationPage}/>
            <Route path={USERS_ROUTE} component={UsersPage}/>
            <Route path={USER_CREATE_ROUTE} component={UserFormPage}/>
            <Route path={USER_VIEW_ROUTE(':id')} component={UserViewPage}/>
            <Route path={USER_UPDATE_ROUTE(':id')} component={UserFormPage}/>
            <Route path={USER_DELETE_ROUTE(':id')} component={UsersPage}/>

            {/* Object Real Estate Advertisements */}
            <Route path={APARTMENT_ADVERTISEMENTS_ROUTE} component={ApartmentAdvertisementsPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_CREATE_ROUTE} component={ApartmentAdvertisementFormPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_VIEW_ROUTE(':id')} component={ApartmentAdvertisementViewPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_UPDATE_ROUTE(':id')} component={ApartmentAdvertisementFormPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_DELETE_ROUTE(':id')} component={ApartmentAdvertisementsPage}/>

            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE} component={CommercialEstateAdvertisementsPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE} component={CommercialEstateAdvertisementFormPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE(':id')} component={CommercialEstateAdvertisementViewPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE(':id')} component={CommercialEstateAdvertisementFormPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_DELETE_ROUTE(':id')} component={CommercialEstateAdvertisementsPage}/>

            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE} component={CottageHousesLandAdvertisementsPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE} component={CottageHousesLandAdvertisementFormPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE(':id')} component={CottageHousesLandAdvertisementViewPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE(':id')} component={CottageHousesLandAdvertisementFormPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_DELETE_ROUTE(':id')} component={CottageHousesLandAdvertisementsPage}/>

            <Route path={CUSTOMER_REQUESTS_ROUTE} component={CustomerRequestsPage}/>
            <Route path={CUSTOMER_REQUEST_VIEW_ROUTE(':id')} component={CustomerRequestViewPage }/>
            <Route path={CUSTOMER_REQUEST_DELETE_ROUTE(':id')} component={CustomerRequestsPage}/>

            <Route path={NEWS_ARTICLE_ROUTE} component={NewsArticlesPage}/>
            <Route path={NEWS_ARTICLE_CREATE_ROUTE} component={NewsArticleFormPage}/>
            <Route path={NEWS_ARTICLE_VIEW_ROUTE(':id')} component={NewsArticleViewPage}/>
            <Route path={NEWS_ARTICLE_UPDATE_ROUTE(':id')} component={NewsArticleFormPage}/>
            <Route path={NEWS_ARTICLE_DELETE_ROUTE(':id')} component={NewsArticlesPage}/>

            <Route path={IMPORT_REPORTS_ROUTE} component={ImportReportsPage}/>
            <Route path={IMPORT_REPORT_CREATE_ROUTE} component={ImportReportFormPage}/>
            <Route path={IMPORT_REPORT_DELETE_ROUTE(':id')} component={ImportReportsPage}/>

            <Route path={PAGES_ROUTE} component={PagesPage}/>
            <Route path={PAGE_CREATE_ROUTE} component={PageFormPage}/>
            <Route path={PAGE_UPDATE_ROUTE(':id')} component={PageFormPage}/>
            <Route path={PAGE_DELETE_ROUTE(':id')} component={PagesPage}/>

        </Route>

        {/*Common*/}
        <Route path="/login" component={ApplicationContainer(LoginPage, false)}/>
        <Route path="/sign-up" component={ApplicationContainer(SignUpPage, false)}/>

        <Route path="/error" component={ErrorPage}/>
        <Route path='*' component={NotFoundPage}/>
    </Route>
);