import './style.scss';

import React, { Component } from 'react';
import Icon from 'react-mdl/lib/Icon';
import classNames from 'classnames';
import NavLink from '../../../common/widgets/nav-link/component';
import DrawerMenuSubItemComponent from './DrawerMenuSubItemComponent';

export default class DrawerMenu extends Component {


    renderLinks(link, index, className, onClick) {

        let classNameLi = classNames(`${className}__li`);
        let classNameNavLink = classNames(`${className}__nav-link`);

        if (link.hasOwnProperty('subItems')) {

            return  <DrawerMenuSubItemComponent key={index}
                            className={`${className}`}
                            onClick={onClick}
                            link={link}
                            index={index}
                />
        }

        return <li key={index} className={classNameLi}>
            <NavLink
                onClick={onClick}
                className={classNameNavLink}
                onlyActiveOnIndex={true}
                to={link.url}>
                    <Icon name={link.iconName}/>
                    {link.title}
            </NavLink>
        </li>;
    }

    render () {

        const { className, links, onClick } = this.props;
        let { classNameUl } = this.props;

        let classNameNav =  classNames(className, 'mdl-color--blue-grey-800');

        if (!classNameUl) {
            classNameUl = `${this.props.className}__ul`;
        }

        return (
            <nav className={classNameNav}>
                <ul className={classNameUl}>
                    {links.map((link, index) => {
                        return this.renderLinks(link, index, className, onClick)
                    })}
                </ul>
            </nav>
        );
    }
}
