import './style.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-mdl/lib/Icon';
import classNames from 'classnames';
import NavLink from '../../../common/widgets/nav-link/component';

export default class DrawerMenuSubItemComponent extends Component {

    state = {
        isVisibleSubItemContainer: false,
    };

    static propTypes = {
        className: PropTypes.string.isRequired,
    };


    handleClick(event) {
        event.preventDefault();

        this.setState({isVisibleSubItemContainer: false});
        if (!this.state.isVisibleSubItemContainer) {
            this.setState({isVisibleSubItemContainer: true});
        } else {
            this.setState({isVisibleSubItemContainer: false});
        }
    }

    renderLinks(link, index, className, onClick) {

        let classNameLi = classNames(`${className}__li`);
        let classNameNavLink = classNames(`${className}__nav-link`);

        return <li key={index} className={classNameLi}>
            <NavLink
                onClick={onClick}
                className={classNameNavLink}
                onlyActiveOnIndex={true}
                to={link.url}>
                <Icon name={link.iconName}/>
                {link.title}
            </NavLink>
        </li>;
    }

    render () {

        const { className, link, onClick } = this.props;

        const { isVisibleSubItemContainer } = this.state,
            classNameSubItemContainer = classNames(`${className}__sub-item-container`,{
                [`${className}__sub-item-container--visible`]: isVisibleSubItemContainer,
            }),
            classNameLi = classNames(`${className}__li`, `${className}__sub-item-li`),
            classNameNavLink = classNames(`${className}__nav-link`);

        return (
            <li className={classNameLi}>
                <NavLink
                    className={classNameNavLink}
                    onlyActiveOnIndex={true}
                    onClick={::this.handleClick}
                    to={link.url}
                >
                    <Icon name={link.iconName}/>
                    {link.title}
                    { isVisibleSubItemContainer
                         ? <i className="fa fa-caret-up" aria-hidden="true"></i>
                         : <i className="fa fa-caret-down" aria-hidden="true"></i>
                    }
                </NavLink>
                <ul className={classNameSubItemContainer}>
                    {link.subItems.map((link, index) => {
                        return this.renderLinks(link, index, className, onClick)
                    })}
                </ul>
            </li>
        );
    }
}
