import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import find from 'lodash/find';
import FormWidget from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../../components/BackToListButton';
import TextDraftWysiwygWidget from '../../../../../common/widgets/text-draft-wysiwyg-widget';

import { baseGetAction } from '../../../../../common/actions/BaseAction';
import {
    APARTMENT_ADVERTISEMENT_URL_REQUEST,
    APARTMENT_ADVERTISEMENTS_ROUTE,
    APARTMENT_ADVERTISEMENT_CREATE_ROUTE,
} from "../../../../../common/constants";

const initModel = {
    'photos': [],
    'is_manually_added': 1,
    'is_best_offer': 0,
    'currency_id': 1,
    'country': null,
    'title': null,
    'region': null,
    'city': null,
    'sub_locality_name': null,
    'address': null,
    'price': null,
    'count_rooms': null,
    'number_floor': null,
    'floors_total': null,
    'is_new_building': null,
    'is_phone': null,
    'is_internet': null,
    'is_parking': null,
    'is_rubbish_chute': null,
    'is_lift': null,
    'is_gaz': null,
    'building_type': null,
    'building_series': null,
    'total_area': null,
    'kitchen_space': null,
    'living_space': null,
    'description': null,
    'discriminator': 'apartment',
};

const mapStateToProps = (state, ownProps) => ({
    apartmentAdvertisementId: Number(ownProps.params.id),
    apartmentAdvertisements: state.ApartmentAdvertisement.collection,
    apartmentAdvertisement: find(state.ApartmentAdvertisement.collection, {id: Number(ownProps.params.id)})
        ? find(state.ApartmentAdvertisement.collection, {id: Number(ownProps.params.id)})
        : initModel,
});

const mapDispatchToProps = (dispatch) => ({
    apartmentAdvertisementsGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class ApartmentAdvertisementsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    componentWillMount() {

        // If entity update
        if (this.props.apartmentAdvertisementId) {
            if (!this.props.apartmentAdvertisements.length) {
                this.props.apartmentAdvertisementsGetAction(
                    APARTMENT_ADVERTISEMENT_URL_REQUEST(),
                    this.props.apartmentAdvertisementId,
                    'ApartmentAdvertisement',
                );
            }
            this.formAction = 'baseUpdateAction';
        }
    }

    render () {

        const { apartmentAdvertisement, apartmentAdvertisementId, pushToRouter } = this.props;

        return (
            <div className="news-article-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={APARTMENT_ADVERTISEMENTS_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="ApartmentAdvertisement"
                        initModel={apartmentAdvertisement}
                        url={APARTMENT_ADVERTISEMENT_URL_REQUEST(apartmentAdvertisementId)}>
                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name = 'title'
                                    label = 'Название объявления'
                                    initFocused
                                />
                            </div>
                        </div>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'country'
                                    label = 'Страна'
                                    initFocused
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'region'
                                    label = 'Область'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'city'
                                    label = 'Город'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'sub_locality_name'
                                    label = 'Район'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'address'
                                    label = 'Адрес'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'price'
                                    label = 'Цена'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'count_rooms'
                                    label = 'Количество комнат'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'number_floor'
                                    label = 'Номер этажа'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'floors_total'
                                    label = 'Общее количество этажей в доме'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_new_building'
                                    label='Новостройка'
                                    typeField='checkbox'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_best_offer'
                                    label='Лучшее предложение (показывать ли объявление в виджете "лучшее предложение")'
                                    typeField='checkbox'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_phone'
                                    label='Телефон'
                                    typeField='checkbox'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_internet'
                                    label='Интернет'
                                    typeField='checkbox'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_parking'
                                    label='Парковка'
                                    typeField='checkbox'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_rubbish_chute'
                                    label='Мусоропровод'
                                    typeField='checkbox'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_lift'
                                    label='Лифт'
                                    typeField='checkbox'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_gaz'
                                    label='Газ'
                                    typeField='checkbox'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--6-col">
                                <TextInputWidget
                                    name='building_type'
                                    label='Тип дома'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--6-col">
                                <TextInputWidget
                                    name='building_series'
                                    label='Серия дома'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='total_area'
                                    label='Общая площадь'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='kitchen_space'
                                    label='Площадь кухни'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='living_space'
                                    label='Жилая площадь'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextDraftWysiwygWidget
                                    name = 'description'
                                    label = 'Описание'
                                />
                            </div>
                        </div>


                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <FileInputMultipleWidget
                                    type = 'file'
                                    name = 'photos'
                                    title = 'Выберите изображение для загрузки'
                                />
                            </div>
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Сохранить"
                                type="saveAndBackToList"
                                backToListUrl={APARTMENT_ADVERTISEMENTS_ROUTE}
                            />
                            { !apartmentAdvertisementId ?
                                <ButtonLoadingWidget
                                    label="Сохранить & Создать новую"
                                    type="saveAndMore"
                                    backToListUrl={APARTMENT_ADVERTISEMENT_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}