import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    APARTMENT_ADVERTISEMENTS_ROUTE,
    APARTMENT_ADVERTISEMENT_CREATE_ROUTE,
    APARTMENT_ADVERTISEMENT_VIEW_ROUTE,
    APARTMENT_ADVERTISEMENT_UPDATE_ROUTE,
    APARTMENT_ADVERTISEMENT_URL_REQUEST,
} from '../../../../common/constants';

const ApartmentAdvertisementsPage = () => {
    return (
        <div className="apartment-advertisements-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Создать',
                        iconName: 'add',
                        link: APARTMENT_ADVERTISEMENT_CREATE_ROUTE,
                    },
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: APARTMENT_ADVERTISEMENT_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: APARTMENT_ADVERTISEMENT_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    city: {
                        title: 'Город',
                        propsTableHeader: {
                            tooltip: 'Город',
                        },
                    },

                    sub_locality_name: {
                        title: 'Адрес',
                        propsTableHeader: {
                            tooltip: 'Адрес',
                        },
                    },

                    price: {
                        title: 'Цена',
                        propsTableHeader: {
                            tooltip: 'Цена',
                        },
                    },
                    created: {
                        title: 'Дата создания объявления',
                        propsTableHeader: {
                            tooltip: 'Дата создания объявления',
                        },
                    },

                    updated: {
                        title: 'Дата редактирования объявления',
                        propsTableHeader: {
                            tooltip: 'Дата редактирования объявления',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "ApartmentAdvertisement",
                        url: APARTMENT_ADVERTISEMENTS_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "ApartmentAdvertisement",
                        url: APARTMENT_ADVERTISEMENTS_ROUTE,
                    }
                }}

                entityName='ApartmentAdvertisement'
                urlGetRequest={APARTMENT_ADVERTISEMENT_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default ApartmentAdvertisementsPage;