import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST,
} from '../../../../common/constants';

const CottageHousesLandAdvertisementsPage = () => {
    return (
        <div className="cottage-houses-land-advertisements-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Создать',
                        iconName: 'add',
                        link: COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE,
                    },
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    city: {
                        title: 'Город',
                        propsTableHeader: {
                            tooltip: 'Город',
                        },
                    },

                    sub_locality_name: {
                        title: 'Адрес',
                        propsTableHeader: {
                            tooltip: 'Адрес',
                        },
                    },

                    price: {
                        title: 'Цена',
                        propsTableHeader: {
                            tooltip: 'Цена',
                        },
                    },
                    created: {
                        title: 'Дата создания объявления',
                        propsTableHeader: {
                            tooltip: 'Дата создания объявления',
                        },
                    },

                    updated: {
                        title: 'Дата редактирования объявления',
                        propsTableHeader: {
                            tooltip: 'Дата редактирования объявления',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "CottageHousesLandAdvertisement",
                        url: COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "CottageHousesLandAdvertisement",
                        url: COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
                    }
                }}

                entityName="CottageHousesLandAdvertisement"
                urlGetRequest={COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default CottageHousesLandAdvertisementsPage;