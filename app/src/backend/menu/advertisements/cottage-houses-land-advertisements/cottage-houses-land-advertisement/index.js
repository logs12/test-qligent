import './style.scss';

import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';
import GalleryLightboxWidget from '../../../../../common/widgets/gallery-lightbox-widget';
import { BasePageView } from '../../../../../common/decorators/BasePageView';

import {
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST,
} from "../../../../../common/constants";

class CottageHousesLandAdvertisementView extends Component {

    render() {

        const { model, pushToRouter } = this.props;

        return (
            <div className="apartment-advertisement-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE} />

                    {(!isEmpty(model)) ?
                        <div>
                            <p><b>Добавлено вручную:</b> {model.is_manually_added}</p>
                            <p><b>Страна:</b> {model.country}</p>
                            <p><b>Область:</b> {model.region}</p>
                            <p><b>Город:</b> {model.city}</p>
                            <p><b>Район:</b> {model.sub_locality_name}</p>
                            <p><b>Аддрес квартиры:</b> {model.address} {model.currency}</p>
                            <p><b>Цена:</b> {model.price}</p>
                            <p><b>Количество комнат:</b> {model.count_rooms}</p>
                            <p><b>Номер этажа:</b> {model.number_floor}</p>
                            <p><b>Общее количество этажей в доме:</b> {model.floors_total}</p>
                            <p><b>Новостройка:</b> {model.is_new_building}</p>
                            <p><b>Телефон:</b> {model.is_phone}</p>
                            <p><b>Интернет:</b> {model.is_internet}</p>
                            <p><b>Парковка:</b> {model.is_parking}</p>
                            <p><b>Мусоропровод:</b> {model.is_rubbish_chute}</p>
                            <p><b>Лифт:</b> {model.is_lift}</p>
                            <p><b>Газ:</b> {model.is_gaz}</p>
                            <p><b>Тип дома:</b> {model.building_type}</p>
                            <p><b>Серия дома:</b> {model.building_series}</p>
                            <p><b>Общая площадь:</b> {model.total_area}</p>
                            <p><b>Площадь кухни:</b> {model.kitchen_space}</p>
                            <p><b>Жилая площадь:</b> {model.living_space}</p>
                            <p><b>Дата создания:</b> {model.created}</p>
                            <p><b>Дата редактирования:</b> {model.updated}</p>
                            <p><b>Описание:</b> {model.building_type}</p>

                            <GalleryLightboxWidget images={model.photos} />
                        </div>
                    : null}
                </Article>
            </div>
        )
    }
}

export default BasePageView(
    CottageHousesLandAdvertisementView,
    'CottageHousesLandAdvertisement',
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST,
);