import React from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST,
} from '../../../../common/constants';

const CommercialEstateAdvertisementsPage = () => {
    return (
        <div className="commercial-estate-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Создать',
                        iconName: 'add',
                        link: COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE,
                    },
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    city: {
                        title: 'Город',
                        propsTableHeader: {
                            tooltip: 'Город',
                        },
                    },

                    sub_locality_name: {
                        title: 'Адрес',
                        propsTableHeader: {
                            tooltip: 'Адрес',
                        },
                    },

                    price: {
                        title: 'Цена',
                        propsTableHeader: {
                            tooltip: 'Цена',
                        },
                    },
                    created: {
                        title: 'Дата создания объявления',
                        propsTableHeader: {
                            tooltip: 'Дата создания объявления',
                        },
                    },

                    updated: {
                        title: 'Дата редактирования объявления',
                        propsTableHeader: {
                            tooltip: 'Дата редактирования объявления',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "CommercialEstateAdvertisement",
                        url: COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "CommercialEstateAdvertisement",
                        url: COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
                    }
                }}

                entityName="CommercialEstateAdvertisement"
                urlGetRequest = {COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default CommercialEstateAdvertisementsPage;