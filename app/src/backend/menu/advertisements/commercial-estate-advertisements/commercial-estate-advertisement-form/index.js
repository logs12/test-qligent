import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import find from 'lodash/find';
import FormWidget from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../../components/BackToListButton';
import TextDraftWysiwygWidget from '../../../../../common/widgets/text-draft-wysiwyg-widget';

import { baseGetAction } from '../../../../../common/actions/BaseAction';
import {
    COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST,
    COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE,
} from "../../../../../common/constants";

const initModel = {
    'photos': [],
    'is_manually_added': 1,
    'is_best_offer': 0,
    'currency_id': 1,
    'country': null,
    'title': null,
    'region': null,
    'city': null,
    'sub_locality_name': null,
    'address': null,
    'price': null,
    'total_area': null,
    'description': null,
    'discriminator': 'commercial_estate',
};

const mapStateToProps = (state, ownProps) => ({
    commercialEstateAdvertisementId: Number(ownProps.params.id),
    commercialEstateAdvertisements: state.CommercialEstateAdvertisement.collection,
    commercialEstateAdvertisement: find(state.CommercialEstateAdvertisement.collection, {id: Number(ownProps.params.id)})
        ? find(state.CommercialEstateAdvertisement.collection, {id: Number(ownProps.params.id)})
        : initModel,
});

const mapDispatchToProps = (dispatch) => ({
    commercialEstateAdvertisementsGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class CommercialEstateAdvertisementsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    componentWillMount() {

        // If entity update
        if (this.props.commercialEstateAdvertisementId) {
            if (!this.props.commercialEstateAdvertisements.length) {
                this.props.commercialEstateAdvertisementsGetAction(
                    COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST(),
                    this.props.commercialEstateAdvertisementId,
                    'CommercialEstateAdvertisement',
                );
            }
            this.formAction = 'baseUpdateAction';
        }
    }

    render () {

        const { commercialEstateAdvertisement, commercialEstateAdvertisementId, pushToRouter } = this.props;

        return (
            <div className="commercial-estate-advertisement-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="CommercialEstateAdvertisement"
                        initModel={commercialEstateAdvertisement}
                        url={COMMERCIAL_ESTATE_ADVERTISEMENT_URL_REQUEST(commercialEstateAdvertisementId)}>
                        <div className="mdl-grid">
                            <div className="mdl-grid">
                                <div className="mdl-cell mdl-cell--12-col">
                                    <TextInputWidget
                                        name = 'title'
                                        label = 'Название объявления'
                                        initFocused
                                    />
                                </div>
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'country'
                                    label = 'Страна'
                                    initFocused
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'region'
                                    label = 'Область'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'city'
                                    label = 'Город'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'sub_locality_name'
                                    label = 'Район'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'address'
                                    label = 'Адрес'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'price'
                                    label = 'Цена'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_best_offer'
                                    label='Лучшее предложение (показывать ли объявление в виджете "лучшее предложение")'
                                    typeField='checkbox'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='total_area'
                                    label='Общая площадь'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextDraftWysiwygWidget
                                    name = 'description'
                                    label = 'Описание'
                                />
                            </div>
                        </div>


                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <FileInputMultipleWidget
                                    type = 'file'
                                    name = 'photos'
                                    title = 'Выберите изображение для загрузки'
                                />
                            </div>
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Сохранить"
                                type="saveAndBackToList"
                                backToListUrl={COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE}
                            />
                            { !commercialEstateAdvertisementId ?
                                <ButtonLoadingWidget
                                    label="Сохранить & Создать новую"
                                    type="saveAndMore"
                                    backToListUrl={COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}