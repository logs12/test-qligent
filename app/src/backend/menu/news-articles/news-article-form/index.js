import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import find from 'lodash/find';
import FormWidget from '../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../components/BackToListButton';
import TextDraftWysiwygWidget from '../../../../common/widgets/text-draft-wysiwyg-widget';

import { baseGetAction } from '../../../../common/actions/BaseAction';
import {
    NEWS_ARTICLE_URL_REQUEST,
    NEWS_ARTICLE_ROUTE,
    NEWS_ARTICLE_CREATE_ROUTE,
} from "../../../../common/constants";

const initModel = {
    'title' :null,
    'content': null,
    'photos': [],
};

const mapStateToProps = (state, ownProps) => ({
    newsArticleId: Number(ownProps.params.id),
    newsArticles: state.NewsArticle.collection,
    newsArticle: find(state.NewsArticle.collection, {id: Number(ownProps.params.id)})
        ? find(state.NewsArticle.collection, {id: Number(ownProps.params.id)})
        : initModel,
});

const mapDispatchToProps = (dispatch) => ({
    newsArticlesGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class NewsArticlesForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    componentWillMount() {

        // If entity update
        if (this.props.newsArticleId) {
            if (!this.props.newsArticles.length) {
                this.props.newsArticlesGetAction(
                    NEWS_ARTICLE_URL_REQUEST(),
                    this.props.newsArticleId,
                    'NewsArticle',
                );
            }
            this.formAction = 'baseUpdateAction';
        }
    }

    render () {

        const { newsArticle, newsArticleId, pushToRouter } = this.props;

        return (
            <div className="news-article-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={NEWS_ARTICLE_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="NewsArticle"
                        initModel={newsArticle}
                        url={NEWS_ARTICLE_URL_REQUEST(newsArticleId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name = 'title'
                                    label = 'Название новости'
                                    initFocused
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <FileInputMultipleWidget
                                    type = 'file'
                                    name = 'photos'
                                    title = 'Выберите изображение для загрузки превью новости'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <TextDraftWysiwygWidget
                                    name = 'content'
                                    label = 'Содержимое новости'
                                    initFocused
                                />
                            </div>
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Сохранить"
                                type="saveAndBackToList"
                                backToListUrl={NEWS_ARTICLE_ROUTE}
                            />
                            { !newsArticleId ?
                                <ButtonLoadingWidget
                                    label="Сохранить & Создать новую"
                                    type="saveAndMore"
                                    backToListUrl={NEWS_ARTICLE_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}