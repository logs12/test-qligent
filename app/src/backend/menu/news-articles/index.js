import React from 'react';
import TableWidget from '../../../common/widgets/table-widget';

import {
    NEWS_ARTICLE_ROUTE,
    NEWS_ARTICLE_CREATE_ROUTE,
    NEWS_ARTICLE_VIEW_ROUTE,
    NEWS_ARTICLE_UPDATE_ROUTE,
    NEWS_ARTICLE_URL_REQUEST,
} from '../../../common/constants';

const NewsArticlesPage = () => {
    return (
        <div className="news-articles-page">
            <TableWidget
                actionsTableHeader={[
                    {
                        title: 'Add',
                        iconName: 'add',
                        link: NEWS_ARTICLE_CREATE_ROUTE,
                    }
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: NEWS_ARTICLE_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionUpdate: {
                        title: 'Edit',
                        url: NEWS_ARTICLE_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    title: {
                        title: 'Название новости',
                        propsTableHeader: {
                            tooltip: 'Название новости',
                        },
                    },
                    created: {
                        title: 'Дата создания новости',
                        propsTableHeader: {
                            tooltip: 'Дата создания новости',
                        },
                    },

                    updated: {
                        title: 'Дата редактирования новости',
                        propsTableHeader: {
                            tooltip: 'Дата редактирования новости',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "NewsArticle",
                        url: NEWS_ARTICLE_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "NewsArticle",
                        url: NEWS_ARTICLE_ROUTE,
                    }
                }}
                entityName="NewsArticle"
                urlGetRequest={NEWS_ARTICLE_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default NewsArticlesPage;