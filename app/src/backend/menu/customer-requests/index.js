import React from 'react';
import TableWidget from '../../../common/widgets/table-widget';

import {
    CUSTOMER_REQUESTS_ROUTE,
    CUSTOMER_REQUEST_CREATE_ROUTE,
    CUSTOMER_REQUEST_VIEW_ROUTE,
    CUSTOMER_REQUEST_URL_REQUEST,
} from '../../../common/constants';

const CustomerRequestsPage = () => {
    return (
        <div className="customer-request-page">
            <TableWidget
                actionsTableHeader={[
                    /*{
                        title: 'Add',
                        iconName: 'add',
                        link: CUSTOMER_REQUEST_CREATE_ROUTE,
                    }*/
                ]}

                rowMenuActions = {{
                    actionView: {
                        title: 'View',
                        url: CUSTOMER_REQUEST_VIEW_ROUTE,
                        settings: {
                            clickRow: true,
                        },
                    },
                    actionDelete: {
                        title: 'Delete',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    name: {
                        title: 'Имя клиента',
                        propsTableHeader: {
                            tooltip: 'Имя клиента',
                        },
                    },
                    phone: {
                        title: 'Номер телефона',
                        propsTableHeader: {
                            tooltip: 'Номер телефона',
                        },
                    },
                    created: {
                        title: 'Дата создания заявки',
                        propsTableHeader: {
                            tooltip: 'Дата создания заявки',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "CustomerRequest",
                        url: CUSTOMER_REQUESTS_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "CustomerRequest",
                        url: CUSTOMER_REQUESTS_ROUTE,
                    }
                }}
                entityName="CustomerRequest"
                urlGetRequest = {CUSTOMER_REQUEST_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default CustomerRequestsPage;