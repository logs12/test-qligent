import './style.scss';

import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import Article from '../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../components/BackToListButton';


import { BasePageView } from '../../../../common/decorators/BasePageView';

import {
    CUSTOMER_REQUESTS_ROUTE,
    CUSTOMER_REQUEST_URL_REQUEST,
} from "../../../../common/constants";

class CustomerRequestView extends Component {

    render() {

        const { model, pushToRouter } = this.props;

        return (
            <div className="customer-request-view-page">
                <Article>
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={CUSTOMER_REQUESTS_ROUTE} />

                    {(!isEmpty(model)) ?
                        <div>
                            <p><b>Title:</b> {model.name}</p>
                            <p><b>Content:</b> {model.phone}</p>
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}

export default BasePageView(
    CustomerRequestView,
    'CustomerRequest',
    CUSTOMER_REQUEST_URL_REQUEST,
);