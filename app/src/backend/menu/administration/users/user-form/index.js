import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import { baseGetAction } from '../../../../../common/actions/BaseAction';
import find from 'lodash/find';

import Form from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import BackToListButton from '../../../../components/BackToListButton';

import {
    USERS_ROUTE,
    USER_CREATE_ROUTE,
    USER_URL_REQUEST,
} from "../../../../../common/constants";

const initModel = {
    'first_name': null,
    'second_name': null,
    'third_name': null,
    'phone': null,
    'email': null,
    'password': null,
};

const mapToStateProps = (state, ownProps) => ({
    userId: Number(ownProps.params.id),
    users: state.User.collection,
    user: find(state.User.collection, {id: Number(ownProps.params.id)})
            ? find(state.User.collection, {id: Number(ownProps.params.id)})
            :initModel,
});

const mapDispatchToProps = (dispatch) => ({
    usersGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapToStateProps, mapDispatchToProps)

export default class UserForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    componentWillMount() {
        // If entity update
        if (this.props.userId) {
            if (!this.props.users.length) {
                this.props.usersGetAction(
                    USER_URL_REQUEST(),
                    this.props.userId,
                    'User',
                );
            }
            this.formAction = 'baseUpdateAction';
        }
    }

    render () {

        const { pushToRouter, user, userId } = this.props;

        return (
            <div className="user-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={USERS_ROUTE} />
                    <Form
                        actionName={this.formAction}
                        entityName="User"
                        initModel={user}
                        url={USER_URL_REQUEST(userId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'first_name'
                                    label = 'First name'
                                    initFocused
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'second_name'
                                    label = 'Second name'
                                />
                            </div>


                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'third_name'
                                    label = 'Third name'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'phone'
                                    label = 'Phone'
                                    className="mdl-cell mdl-cell--4-col"
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'email'
                                    label = 'Email'
                                    className="mdl-cell mdl-cell--4-col"
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'password'
                                    label = 'Password'
                                    className="mdl-cell mdl-cell--4-col"
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <ButtonLoadingWidget
                                label="Save"
                                type="saveAndBackToList"
                                backToListUrl={USERS_ROUTE}
                            />
                            { !userId ?
                                <ButtonLoadingWidget
                                    label="Save & New"
                                    type="saveAndMore"
                                    backToListUrl={USER_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </Form>
                </div>
            </div>
        );
    }
}