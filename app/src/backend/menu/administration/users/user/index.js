import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import Article from '../../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../../components/BackToListButton';

import { BasePageView } from '../../../../../common/decorators/BasePageView';

import {
    USERS_ROUTE
} from "../../../../../common/constants";

class UserView extends Component {

    render() {

        const { model, pushToRouter } = this.props;

        return (
            <div className="user-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={USERS_ROUTE} />

                    {(!isEmpty(model)) ?
                        <div>
                            <p>{model.first_name}</p>
                            <p>{model.second_name}</p>
                            <p>{model.third_name}</p>
                            <p>{model.email}</p>
                            <p>{model.phone}</p>
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}

export default BasePageView(UserView, 'User');