import React, { Component } from 'react';
import TableWidget from '../../../../common/widgets/table-widget';

import {
    USERS_ROUTE,
    USER_CREATE_ROUTE,
    USER_VIEW_ROUTE,
    USER_UPDATE_ROUTE,
    USER_URL_REQUEST,
} from '../../../../common/constants';

export default class UsersPage extends Component{

    render() {
        return (
            <div className="users-page">
                    <TableWidget
                        actionsTableHeader={[
                            {
                                title: 'Add',
                                iconName: 'add',
                                link: USER_CREATE_ROUTE,
                            }
                        ]}

                        rowMenuActions = {{
                                actionView: {
                                    title: 'View',
                                    url: USER_VIEW_ROUTE,
                                    settings: {
                                        clickRow: true,
                                    },
                                },
                                actionUpdate: {
                                    title: 'Edit',
                                    url: USER_UPDATE_ROUTE,
                                    settings: {
                                        nameIconButton: 'edit',
                                    },
                                },
                                actionDelete: {
                                    title: 'Delete',
                                    settings: {
                                        nameIconButton: 'delete',
                                    },
                                },
                        }}

                        attributes={
                            {
                                first_name: {
                                    title: 'First name',
                                    propsTableHeader: {
                                        tooltip: 'First name tooltip',
                                    },
                                },
                                second_name: {
                                    title: 'Second name',
                                    propsTableHeader: {
                                        tooltip: 'Second name tooltip',
                                    },
                                },

                                third_name: {
                                    title: 'Third name',
                                    propsTableHeader: {
                                        tooltip: 'Third name tooltip',
                                    },
                                },
                                phone: {
                                    title: 'Phone',
                                    propsTableHeader: {
                                        tooltip: 'phone name tooltip',
                                    },
                                },
                                email: {
                                    title: 'Email',
                                    propsTableHeader: {
                                        tooltip: 'email name tooltip',
                                    },
                                },
                            }
                        }

                        widgetOptions ={{
                            search: {
                                entityName: "User",
                                url: USERS_ROUTE,
                                searchFields: [
                                    'first_name',
                                    'second_name',
                                    'third_name',
                                    'email',
                                    'phone',
                                ]
                            },
                            pagination: {
                                entityName: "User",
                                url: USERS_ROUTE,
                            }
                        }}

                        entityName="User"
                        urlGetRequest = {USER_URL_REQUEST}
                        shadow={0}
                        className="wide"
                    >
                    </TableWidget>
            </div>
        );
    }

};