import React from 'react';
import TableWidget from '../../../common/widgets/table-widget';

import {
    PAGES_ROUTE,
    PAGE_UPDATE_ROUTE,
    PAGE_URL_REQUEST,
} from '../../../common/constants';

const PagesPage = () => {
    return (
        <div className="pages-page">
            <TableWidget
                actionsTableHeader={[
                    /*{
                        title: 'Add',
                        iconName: 'add',
                        link: PAGE_CREATE_ROUTE,
                    }*/
                ]}

                rowMenuActions = {{
                    actionUpdate: {
                        title: 'Редактировать',
                        url: PAGE_UPDATE_ROUTE,
                        settings: {
                            nameIconButton: 'edit',
                        },
                    },
                    actionDelete: {
                        title: 'Удалить',
                        settings: {
                            nameIconButton: 'delete',
                        },
                    },
                }}

                attributes={{
                    title: {
                        title: 'Название страницы',
                        propsTableHeader: {
                            tooltip: 'Название страницы',
                        },
                    },
                    created: {
                        title: 'Дата создания страницы',
                        propsTableHeader: {
                            tooltip: 'Дата создания страницы',
                        },
                    },

                    updated: {
                        title: 'Дата редактирования страницы',
                        propsTableHeader: {
                            tooltip: 'Дата редактирования страницы',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "Page",
                        url: PAGES_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "Page",
                        url: PAGES_ROUTE,
                    }
                }}

                entityName="Page"
                urlGetRequest = {PAGE_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default PagesPage;