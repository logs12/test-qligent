import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import find from 'lodash/find';
import FormWidget from '../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../components/BackToListButton';
import TextDraftWysiwygWidget from '../../../../common/widgets/text-draft-wysiwyg-widget';

import { baseGetAction} from '../../../../common/actions/BaseAction';
import {
    PAGES_ROUTE,
    PAGE_CREATE_ROUTE,
    PAGE_URL_REQUEST,
} from "../../../../common/constants";

const initModel = {
    'title' :null,
    'content': null,
    'files': [],
};

const mapStateToProps = (state, ownProps) => ({
    modelId: Number(ownProps.params.id),
    models: state.Page.collection,
    model: find(state.Page.collection, {id: Number(ownProps.params.id)})
        ? find(state.Page.collection, {id: Number(ownProps.params.id)})
        : initModel,
});

const mapDispatchToProps = (dispatch) => ({
    pagesGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class PageFormPage extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    componentWillMount() {

        // If entity update
        if (this.props.modelId) {
            if (!this.props.models.length) {
                this.props.pagesGetAction(PAGE_URL_REQUEST(), this.props.modelId, 'Page');
            }
            this.formAction = 'baseUpdateAction';
        }
    }

    render () {

        const { model, modelId, pushToRouter } = this.props;

        return (
            <div className="page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={PAGES_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="Page"
                        initModel={model}
                        url={PAGE_URL_REQUEST(modelId)}>
                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name = 'title'
                                    label = 'Название страницы'
                                    initFocused
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <FileInputMultipleWidget
                                    type = 'file'
                                    name = 'files'
                                    title = 'Выберите изображение для загрузки'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <TextDraftWysiwygWidget
                                    name = 'content'
                                    label = 'Содержимое страницы'
                                    initFocused
                                />
                            </div>
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Сохранить"
                                type="saveAndBackToList"
                                backToListUrl={PAGES_ROUTE}
                            />
                            { !modelId ?
                                <ButtonLoadingWidget
                                    label="Сохранить & Создать новую"
                                    type="saveAndMore"
                                    backToListUrl={PAGE_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}