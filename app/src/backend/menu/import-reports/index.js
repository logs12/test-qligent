import React from 'react';
import TableWidget from '../../../common/widgets/table-widget';

import {
    IMPORT_REPORTS_ROUTE,
    IMPORT_REPORT_VIEW_ROUTE,
    IMPORT_REPORT_URL_REQUEST,
} from '../../../common/constants';

const ImportReportsPage = () => {
    return (
        <div className="import-reports-page">
            <TableWidget

                attributes={{
                    id: {
                        title: '№',
                        propsTableHeader: {
                            tooltip: 'Номер отчета',
                        },
                    },
                    created: {
                        title: 'Дата создания импорта',
                        propsTableHeader: {
                            tooltip: 'Дата создания импорта',
                        },
                    },
                }}

                widgetOptions ={{
                    search: {
                        entityName: "ImportReport",
                        url: IMPORT_REPORTS_ROUTE,
                        searchFields: [
                            'title',
                        ]
                    },
                    pagination: {
                        entityName: "ImportReport",
                        url: IMPORT_REPORTS_ROUTE,
                    }
                }}
                entityName="ImportReport"
                urlGetRequest = {IMPORT_REPORT_URL_REQUEST}
                shadow={0}
                className="wide"
            >
            </TableWidget>
        </div>
    );
};

export default ImportReportsPage;