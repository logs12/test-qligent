import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import FormWidget from '../../../../common/widgets/form-widget';
import ButtonLoadingWidget from '../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../components/BackToListButton';

import { baseGetAction } from '../../../../common/actions/BaseAction';
import {
    IMPORT_REPORT_IMPORT_URL_REQUEST,
    ADMIN_ROUTE,
} from "../../../../common/constants";

const mapStateToProps = (state, ownProps) => ({
    discriminator: ownProps.params.discriminator,
});

const mapDispatchToProps = (dispatch) => ({
    apartmentAdvertisementsGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class ImportReportForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    render () {

        const { pushToRouter } = this.props;

        return (
            <div className="import-report-page-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={ADMIN_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="ImportReport"
                        initModel={{
                            'file': [],
                        }}
                        url={IMPORT_REPORT_IMPORT_URL_REQUEST}>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <FileInputMultipleWidget
                                    type="file"
                                    name="file"
                                    title="Выберите файл для импорта"
                                />
                            </div>
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Импортировать"
                            />

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}