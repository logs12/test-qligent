import React, {Component} from "react";
import shallowEqual from "react-redux/lib/utils/shallowEqual";
import Button from "react-mdl/lib/Button";
import { push } from 'react-router-redux';

/**
 *
 @backToListButton(
 ({auth: email}, {folder}) => ({email, folder}),
 ({email, folder}, actions) => email && actions.fetchStatus(folder)
 )
 * Декоратор для загрузки данных
 * @param getter
 * @param executor
 * @returns {function(): BackToListButton}
 */
export default function backToListButton(routeBackToList, executor) {
    return (DecoratedComponent) => class BackToListButton extends Component {

        /**
         *  Перед началом рендера компонента
         */
        componentWillMount() {
            executor;
            debugger;
            this.props;
            //executor(getter(this.props, this.props.params), this.props.actions);
        }

        handleBackToListButton() {
            this.props.dispatch(push(routeBackToList));
        }

        /**
         * Вызывается сразу после render.
         * @param prevProps
         */
        componentDidUpdate(prevProps) {

            /*const params = getter(this.props, this.props.params);
            const prevParams = getter(prevProps, prevProps.params);
            // Сравниваем новые свойства со старыми
            !shallowEqual(params, prevParams)
            && executor(params, this.props.actions);*/
        }

        render() {
            return (
                <div className="building-page-form">
                    <div className="mdl-card mdl-shadow--2dp wide">
                        <div className = "back-to-list">
                            <Button className = "back-to-list__button" ripple onClick={::this.handleBackToListButton}>Back to list</Button>
                        </div>
                        <DecoratedComponent {...this.props} />;
                    </div>
                </div>
            )
        }
    };
};