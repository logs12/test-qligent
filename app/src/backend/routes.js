import React from 'react';
import { Route, IndexRoute } from 'react-router';

// Layouts Backend
import MainPageLayout from './layouts/MainPageLayout/index';

// Backend components
import DashboardPage from './pages/dashboard-page/index';
import AdministrationPage from './menu/administration/index';
import UsersPage from './menu/administration/users/index';
import UserViewPage from './menu/administration/users/user/index';
import UserFormPage from './menu/administration/users/user-form/index';

import ApartmentAdvertisementsPage from './menu/advertisements/apartment-advertisements/index';
import ApartmentAdvertisementViewPage from './menu/advertisements/apartment-advertisements/apartment-advertisement/index';
import ApartmentAdvertisementFormPage from './menu/advertisements/apartment-advertisements/apartment-advertisement-form/index';

import CommercialEstateAdvertisementsPage from './menu/advertisements/commercial-estate-advertisements/index';
import CommercialEstateAdvertisementViewPage from './menu/advertisements/commercial-estate-advertisements/commercial-estate-advertisement/index';
import CommercialEstateAdvertisementFormPage from './menu/advertisements/commercial-estate-advertisements/commercial-estate-advertisement-form/index';

import CottageHousesLandAdvertisementsPage from './menu/advertisements/cottage-houses-land-advertisements/index';
import CottageHousesLandAdvertisementViewPage from './menu/advertisements/cottage-houses-land-advertisements/cottage-houses-land-advertisement/index';
import CottageHousesLandAdvertisementFormPage from './menu/advertisements/cottage-houses-land-advertisements/cottage-houses-land-advertisement-form/index';

import CustomerRequestsPage from './menu/customer-requests/index';
import CustomerRequestViewPage from './menu/customer-requests/customer-request/index';

import NewsArticlesPage from './menu/news-articles/index';
import NewsArticleViewPage from './menu/news-articles/news-article/index';
import NewsArticleFormPage from './menu/news-articles/news-article-form/index'

import ImportReportsPage from './menu/import-reports/index';
import ImportReportFormPage from './menu/import-reports/import-report-form/index';

import PagesPage from './menu/pages/index';
import PageFormPage from './menu/pages/page-form/index';

// Common components
import LoginPage from '../common/pages/login-page/index';
import SignUpPage from '../common/pages/sign-up-page/index';
import ErrorPage from '../common/pages/error-page/index';
import NotFoundPage from '../common/pages/not-found-page/index';
import ApplicationContainer from '../common/containers/ApplicationContainer';

// constants react-redux-router app
import {
    ADMIN_ROUTE,
    ADMINISTRATION_ROUTE,
    USERS_ROUTE,
    USER_VIEW_ROUTE,
    USER_CREATE_ROUTE,
    USER_UPDATE_ROUTE,
    USER_DELETE_ROUTE,

    APARTMENT_ADVERTISEMENTS_ROUTE,
    APARTMENT_ADVERTISEMENT_CREATE_ROUTE,
    APARTMENT_ADVERTISEMENT_VIEW_ROUTE,
    APARTMENT_ADVERTISEMENT_UPDATE_ROUTE,
    APARTMENT_ADVERTISEMENT_DELETE_ROUTE,

    COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENT_DELETE_ROUTE,

    // News apartment-advertisement
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_DELETE_ROUTE,

    CUSTOMER_REQUESTS_ROUTE,
    CUSTOMER_REQUEST_VIEW_ROUTE,
    CUSTOMER_REQUEST_DELETE_ROUTE,

    NEWS_ARTICLE_ROUTE,
    NEWS_ARTICLE_CREATE_ROUTE,
    NEWS_ARTICLE_VIEW_ROUTE,
    NEWS_ARTICLE_UPDATE_ROUTE,
    NEWS_ARTICLE_DELETE_ROUTE,

    IMPORT_REPORTS_ROUTE,
    IMPORT_REPORT_CREATE_ROUTE,
    IMPORT_REPORT_DELETE_ROUTE,

    PAGES_ROUTE,
    PAGE_CREATE_ROUTE,
    PAGE_UPDATE_ROUTE,
    PAGE_DELETE_ROUTE,

} from '../common/constants/index';

export const routes = (
    <Route>
        {/*Backend*/}
        <Route path={ADMIN_ROUTE} component={ApplicationContainer(MainPageLayout)}>
            <IndexRoute component={DashboardPage}/>
            <Route path={ADMINISTRATION_ROUTE} component={AdministrationPage}/>
            <Route path={USERS_ROUTE} component={UsersPage}/>
            <Route path={USER_CREATE_ROUTE} component={UserFormPage}/>
            <Route path={USER_VIEW_ROUTE(':id')} component={UserViewPage}/>
            <Route path={USER_UPDATE_ROUTE(':id')} component={UserFormPage}/>
            <Route path={USER_DELETE_ROUTE(':id')} component={UsersPage}/>

            {/* Object Real Estate Advertisements */}
            <Route path={APARTMENT_ADVERTISEMENTS_ROUTE} component={ApartmentAdvertisementsPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_CREATE_ROUTE} component={ApartmentAdvertisementFormPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_VIEW_ROUTE(':id')} component={ApartmentAdvertisementViewPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_UPDATE_ROUTE(':id')} component={ApartmentAdvertisementFormPage}/>
            <Route path={APARTMENT_ADVERTISEMENT_DELETE_ROUTE(':id')} component={ApartmentAdvertisementsPage}/>

            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE} component={CommercialEstateAdvertisementsPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE} component={CommercialEstateAdvertisementFormPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE(':id')} component={CommercialEstateAdvertisementViewPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE(':id')} component={CommercialEstateAdvertisementFormPage}/>
            <Route path={COMMERCIAL_ESTATE_ADVERTISEMENT_DELETE_ROUTE(':id')} component={CommercialEstateAdvertisementsPage}/>

            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE} component={CottageHousesLandAdvertisementsPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE} component={CottageHousesLandAdvertisementFormPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE(':id')} component={CottageHousesLandAdvertisementViewPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE(':id')} component={CottageHousesLandAdvertisementFormPage}/>
            <Route path={COTTAGE_HOUSES_LAND_ADVERTISEMENT_DELETE_ROUTE(':id')} component={CottageHousesLandAdvertisementsPage}/>

            <Route path={CUSTOMER_REQUESTS_ROUTE} component={CustomerRequestsPage}/>
            <Route path={CUSTOMER_REQUEST_VIEW_ROUTE(':id')} component={CustomerRequestViewPage }/>
            <Route path={CUSTOMER_REQUEST_DELETE_ROUTE(':id')} component={CustomerRequestsPage}/>

            <Route path={NEWS_ARTICLE_ROUTE} component={NewsArticlesPage}/>
            <Route path={NEWS_ARTICLE_CREATE_ROUTE} component={NewsArticleFormPage}/>
            <Route path={NEWS_ARTICLE_VIEW_ROUTE(':id')} component={NewsArticleViewPage}/>
            <Route path={NEWS_ARTICLE_UPDATE_ROUTE(':id')} component={NewsArticleFormPage}/>
            <Route path={NEWS_ARTICLE_DELETE_ROUTE(':id')} component={NewsArticlesPage}/>

            <Route path={IMPORT_REPORTS_ROUTE} component={ImportReportsPage}/>
            <Route path={IMPORT_REPORT_CREATE_ROUTE} component={ImportReportFormPage}/>
            <Route path={IMPORT_REPORT_DELETE_ROUTE(':id')} component={ImportReportsPage}/>

            <Route path={PAGES_ROUTE} component={PagesPage}/>
            <Route path={PAGE_CREATE_ROUTE} component={PageFormPage}/>
            <Route path={PAGE_UPDATE_ROUTE(':id')} component={PageFormPage}/>
            <Route path={PAGE_DELETE_ROUTE(':id')} component={PagesPage}/>

        </Route>

        {/*Common*/}
        <Route path="/login" component={ApplicationContainer(LoginPage, false)}/>
        <Route path="/sign-up" component={ApplicationContainer(SignUpPage, false)}/>

        <Route path="/error" component={ErrorPage}/>
        <Route path='*' component={NotFoundPage}/>
    </Route>
);