import './style.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Header, Content, Drawer, Navigation } from '../../../common/widgets/layout-widget';

import DrawerHeader from '../../containers/DrawerHeader';
import DrawerMenu from '../../components/DrawerMenu';
import HeaderMenu from './HeaderMenu';

import NavLink from '../../../common/widgets/nav-link/component';
import ProgressBarWidget from '../../../common/widgets/progress-bar-widget';
import SnackbarWidget from '../../../common/widgets/snackbar-widget';

import {
    ADMIN_ROUTE,
    USERS_ROUTE,
    ADMINISTRATION_ROUTE,
    APARTMENT_ADVERTISEMENTS_ROUTE,
    COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
    NEWS_ARTICLE_ROUTE,
    CUSTOMER_REQUESTS_ROUTE,
    IMPORT_REPORTS_ROUTE,
    IMPORT_REPORT_CREATE_ROUTE,
    PAGES_ROUTE,
} from '../../../common/constants';

const mapStateToProps = state => ({
    pathname: state.routing.locationBeforeTransitions.pathname,
});

@connect(mapStateToProps)

export default class MainPageLayout extends Component {

    /**
     *
     * @type {{navLinks: Array, title: null, fixedDrawer: boolean, closeDrawer: boolean}}
     */
    state = {
        navLinks: [],
        title: null,
        fixedDrawer: false,
        closeDrawer:false,
    };

    /**
     * Handle click navigation link drawer menu
     */
    onClickNavLinkDrawerMenu() {
        this.setState({
            closeDrawer:true,
        });
    }

    /**
     * Определяем показывать или нет drawer b верхние подменю
     * @param pathname
     */
    renderSubComponentFromPathname(pathname) {
        if (pathname.indexOf('administration') !== -1) {
            this.setState({
                navLinks: [
                    <NavLink
                        key="users"
                        to={USERS_ROUTE}
                        className="drawer-menu__nav-link"
                    >
                        Пользователи
                    </NavLink>
                ],
                title: 'Администрирование',
                fixedDrawer: false,
            });
        }
        else if (pathname.indexOf('apartment-advertisement') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer:false,
                title: 'Квартиры',
            });
        } else if (pathname.indexOf('cottage-houses-land-advertisement') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer:false,
                title: 'Дома, дачи, коттеджи, участки',
            });
        } else if (pathname.indexOf('commercial-estate-advertisement') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer:false,
                title: 'Коммерческая недвижимость',
            });
        } else if (pathname.indexOf('news-article') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer:false,
                title: 'Новости',
            });
        } else if (pathname.indexOf('customer-request') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer:false,
                title: 'Заявки от клиентов',
            });
        }
        else if (pathname.indexOf('import-report') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer: false,
                title: 'Отчеты импорта',
            });
        }
        else if (pathname.indexOf('page') !== -1) {
            this.setState({
                navLinks: [],
                fixedDrawer:false,
                title: 'Страницы',
            });
        } else {
            this.setState({
                navLinks: [],
                fixedDrawer:true,
                title: 'Главная',
            });
        }
    }

    componentWillMount() {
        this.renderSubComponentFromPathname(this.props.pathname);
    }

    componentWillReceiveProps(nextProps) {
        this.renderSubComponentFromPathname(nextProps.pathname);
    }

    links = [
        {
            url: ADMIN_ROUTE,
            title: 'Главная',
            iconName:'home',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: true,
        },
        {
            url: '#',
            title: 'Объявления',
            iconName:'art_track',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: false,
            subItems: [
                {
                    url: APARTMENT_ADVERTISEMENTS_ROUTE,
                    title: 'Квартиры',
                    iconName:'location_city',
                    onClick:this.onClickNavLinkDrawerMenu,
                    onlyActiveOnIndex: false,
                },
                {
                    url: COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
                    title: 'Дома, дачи, коттеджи, участки',
                    iconName:'nature_people',
                    onClick:this.onClickNavLinkDrawerMenu,
                    onlyActiveOnIndex: false,
                },
                {
                    url: COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE,
                    title: 'Коммерческая недвижимость',
                    iconName:'domain',
                    onClick:this.onClickNavLinkDrawerMenu,
                    onlyActiveOnIndex: false,
                },
            ]
        },
        {
            url: '#',
            title: 'Импорт',
            iconName:'file_upload',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: true,
            subItems: [
                {
                    url: IMPORT_REPORT_CREATE_ROUTE,
                    title: 'Импорт объявлений',
                    iconName:'file_upload',
                    onClick:this.onClickNavLinkDrawerMenu,
                    onlyActiveOnIndex: false,
                },
                {
                    url: IMPORT_REPORTS_ROUTE,
                    title: 'Отчеты импорта',
                    iconName:'playlist_add_check',
                    onClick:this.onClickNavLinkDrawerMenu,
                    onlyActiveOnIndex: false,
                },
            ]
        },
        {
            url: CUSTOMER_REQUESTS_ROUTE,
            title: 'Заявки от клиентов',
            iconName:'contact_phone',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: true,
        },
        {
            url: NEWS_ARTICLE_ROUTE,
            title: 'Новости',
            iconName:'book',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: true,
        },
        {
            url: PAGES_ROUTE,
            title: 'Страницы',
            iconName:'pages',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: true,
        },
        {
            url: ADMINISTRATION_ROUTE,
            title: 'Администрирование',
            iconName:'people',
            onClick:this.onClickNavLinkDrawerMenu,
            onlyActiveOnIndex: true,
        },
    ];



    render() {

        const { title, navLinks, fixedDrawer, closeDrawer } = this.state;

        return (
            <Layout fixedHeader fixedDrawer={fixedDrawer} className="menu-page-layout"  closeDrawer = {closeDrawer}>
                <Header title={
                        <span style={{ color: '#ddd' }}>
                        {title}
                        </span>
                    }>
                    <HeaderMenu navLinks={navLinks}/>
                </Header>
                <ProgressBarWidget />
                <section className="breadcrumbs"></section>
                <Drawer className="mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
                    <DrawerHeader />
                    <DrawerMenu className="drawer-menu"
                                onClick={::this.onClickNavLinkDrawerMenu}
                                links={this.links} />
                </Drawer>
                <Content component="main">{this.props.children}</Content>
                <SnackbarWidget />
            </Layout>
        );
    }
};

