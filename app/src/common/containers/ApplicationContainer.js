import React from 'react';
import { connect } from 'react-redux';
import { replace, push } from 'react-router-redux';

import { LOGIN_ROUTE } from '../constants';

import ErrorPage from "../pages/error-page";

/**
 * Контейнер обертка для проверки компонентов на права
 * @param Component
 * @param isRequireAuthentication - флаг делать ли проверку на аутентификацию компонента
 * @returns {ApplicationComponent}
 * @constructor
 */
export default function ApplicationContainer(Component, isRequireAuthentication = true) {

    @connect(
        (state, ownProps) => ({ // mapStateToProps
            configData: state.common.configData,
            systemError: state.common.systemError,
            pathname: ownProps.location.pathname,
        })
    )

    class ApplicationComponent extends React.Component {

        /**
         * false - показывается обычный компонент, true - компонент ошибки
         * @type {boolean}
         */
        isError = false;

        /**
         * React component
         */
        component = null;


        /*shouldComponentUpdate(nextProps) {
            // Если человек аутентифицирован, и роуты одинаковые, обновление компонента не делаем
            if (nextProps.configData.isAuthenticated && (this.props.pathname == nextProps.location.pathname)) {
                return false;
            }
            return true;
        }*/

        componentWillMount() {
            this.component = <Component { ...this.props } />;
            this.checkAuth(this.props);
        }

        componentWillReceiveProps(nextProps) {

            // Если пользователь не аутентифицирован или роуты не одинаковые
            if (!nextProps.configData.isAuthenticated || this.props.pathname !== nextProps.pathname) {

                this.component = <Component { ...nextProps } />;
                this.isError = nextProps.systemError.isError;
                this.checkAuth(nextProps);
            }
        }

        /**
         * Проверка аутентификации пользователя, если нет перенаправление на страницу авторизации
         * @param props
         */
        checkAuth(props) {
            if (!props.configData.isAuthenticated && isRequireAuthentication) {
                this.props.dispatch(push(LOGIN_ROUTE));
                this.component = null;
            }
        }

        render() {
            return (
                (!this.isError) ? this.component : <ErrorPage/>
            )
        }
    }

    return ApplicationComponent;
}