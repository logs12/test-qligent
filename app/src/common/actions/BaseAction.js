import { BaseFetch } from '../../common/services/BaseFetch';
import _has from 'lodash/has';

import {
    BASE_FORM_WIDGET_REQUEST,
    BASE_FORM_WIDGET_SUCCESS,
    BASE_FORM_MODEL_ERROR,
    BASE_URL_REQUEST,
    BASE_MODEL_DELETE,
    BASE_MODEL_GET,
} from '../constants/BaseConstants';

export function baseGetAction(url, options, entityName) {

    return (dispatch) => {

        const userId = _has(options, 'userId') && options.userId !== undefined ? options.userId : null;
        const condition = _has(options, 'condition') && options.condition !== undefined ? options.condition : '';
        const onSuccess = _has(options, 'onSuccess') && options.onSuccess !== undefined ? options.onSuccess : null;

        return BaseFetch.get({
            url: `${url(userId)}${condition}`,
            dispatch: dispatch,
            success: object => {
                dispatch({
                    type: BASE_MODEL_GET(entityName),
                    payload: object,
                });
                if (typeof onSuccess === 'function') {
                    onSuccess();
                }
            },
        });
    };
}

export function baseCreateAction(data, options, entityName) {
    return {
        types: [
            BASE_FORM_WIDGET_REQUEST(entityName),
            BASE_FORM_WIDGET_SUCCESS(entityName),
            BASE_FORM_MODEL_ERROR(entityName),
        ],
        promise: (dispatch) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'POST' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}


export function baseUpdateAction(data, options, entityName) {
    return {
        types: [
            BASE_FORM_WIDGET_REQUEST(entityName),
            BASE_FORM_WIDGET_SUCCESS(entityName),
            BASE_FORM_MODEL_ERROR(entityName),
        ],
        promise: (dispatch) => {
            return new Promise((resolve, reject) => {
                BaseFetch.saveOrError({
                    url: data.url,
                    headers: { method: 'PUT' },
                    body: data.values,
                    dispatch: dispatch,
                    success: object => resolve(object),
                    error: object => reject(object),
                });
            });
        },
        options: options
    }
}

export function baseDeleteAction(url, modelId, entityName) {

    return (dispatch, getState) => {
        return BaseFetch.delete(
            {
                url: `${url(modelId)}`,
                dispatch: dispatch,
                getState: getState,
                success: () => {
                    dispatch({
                        type: BASE_MODEL_DELETE(entityName),
                        payload: {
                            modelId: modelId,
                        }
                    });
                },
            },
            {
                messageSnackbar: 'Удалить?',
                actionOneTitle: 'Да',
                actionTwoTitle: 'Нет',
            }
        );
    }
}