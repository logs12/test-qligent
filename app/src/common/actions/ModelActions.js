/**
 * Первоначальная инициализация модели в глобальном state
 * @param modelInitType - constant type action
 * @param values - model form
 * @param errors - model form error
 * @returns {function(*): *}
 */
export function initModelAction(modelInitType, values, errors = {}) {
    return {
        type: modelInitType,
        values,
        errors,
    };
}

/**
 * Обновление значений input в глобальном state
 * @param modelUpdateType - constant type action
 * @param data
 * @returns {{type, payload: *, modelName: *}}
 */
export function updateModelAction(modelUpdateType, data) {
    return {
        type: modelUpdateType,
        payload: data,
    }
}