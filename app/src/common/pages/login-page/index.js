import './style.scss';

import React from 'react';

import Article from '../../widgets/article-widget/component';
import Form from '../../widgets/form-widget';
import TextInputWidget from '../../widgets/text-input-widget';

import ButtonLoadingWidget  from '../../widgets/button-loading-widget';

import { LOGIN_URL_REQUEST } from "../../constants";

const LoginPage = () => {
    return (
        <div className="page-login">
            <Article>
                <h3>Log In</h3>
                <Form
                    actionName="loginAction"
                    entityName="Login"
                    initModel={{
                        'email':null,
                        'password':null,
                    }}
                    url={LOGIN_URL_REQUEST}
                >
                    <TextInputWidget
                        name = 'email'
                        label = 'Login'
                        initFocused
                    />
                    <TextInputWidget
                        name = 'password'
                        label = 'Password'
                    />
                    <ButtonLoadingWidget
                        label="Log In"
                    />
                </Form>
            </Article>
        </div>
    );
};

export default LoginPage;