//========== /Constant Import report ==========//
export const IMPORT_REPORT_IMPORT_URL_REQUEST = `/api-internal/import-report/import/`;
export const IMPORT_REPORT_URL_REQUEST = (id = null) => (id) ? `/api-internal/import-report/${id}` : `/api-internal/import-report/`;