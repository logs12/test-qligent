//========== /Constant New article ==========//
export const NEWS_ARTICLE_URL_REQUEST = (id = null) => (id) ? `/api-internal/news-article/${id}` : `/api-internal/news-article`;