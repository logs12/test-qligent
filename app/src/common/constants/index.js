//========== /Constants react-redux-router app  ==========//
export * from './RoutesConstants';

//========== /Constants WIDGETS ==========//
export * from './WidgetConstants';

//========== /Constants Аuthorization ==========//
export * from './LoginConstants';

export * from './BaseConstants';

export * from './UserConstants';
export * from './ApartmentAdvertisementConstants';
export * from './CommercialEstateAdvertisementConstants';
export * from './CottageHousesLandAdvertisementConstants';
export * from './CustomerRequestConstants';
export * from './NewsArticleConstants';
export * from './ImportReportConstants';
export * from './PageConstants';


export const CONFIG_DATA_GET = 'CONFIG_DATA_GET';

// Constants url request
export const CONFIG_DATA_URL_REQUEST = '/api-internal/config';

