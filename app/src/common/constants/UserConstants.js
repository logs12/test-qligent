//========== /Constants Users ==========//

export const USERS_GET = 'USERS_GET';
export const USER_DELETE = 'USER_DELETE';
export const USER_FORM_WIDGET_REQUEST = 'USER_FORM_WIDGET_REQUEST';
export const USER_FORM_WIDGET_SUCCESS = 'USER_FORM_WIDGET_SUCCESS';
export const USER_FORM_WIDGET_ERROR = 'USER_FORM_WIDGET_ERROR';
export const USER_MODEL_INIT = 'USER_MODEL_INIT';
export const USER_MODEL_UPDATE = 'USER_MODEL_UPDATE';
export const USER_FORM_MODEL_ERROR = 'USER_FORM_MODEL_ERROR';

// Constants url request
export const USER_GET_URL_REQUEST = `/api-internal/user`;
export const USER_URL_REQUEST = (id = null) => (id) ? `/api-internal/user/${id}` : `/api-internal/user`;