//========== /Constants WIDGETS ==========//

// Constants widget-form
export * from '../widgets/form-widget/constants';

// Constants file-input-multiple-widget
export * from '../widgets/file-input-multiple-widget/constants';

// Constants widget-error
export const ERROR_WIDGET_SERVER = 'ERROR_WIDGET_SERVER';
export const ERROR_WIDGET_CLIENT = 'ERROR_WIDGET_CLIENT';
export const ERROR_WIDGET_RESET = 'ERROR_WIDGET_RESET';

// Constants progress-bar-widget
export const PROGRESS_BAR_WIDGET_START = 'PROGRESS_BAR_WIDGET_START';
export const PROGRESS_BAR_WIDGET_STOP = 'PROGRESS_BAR_WIDGET_STOP';

// Constants progress-bar-widget
export const SNACKBAR_WIDGET_ACTIVE = 'SNACKBAR_WIDGET_ACTIVE';
export const SNACKBAR_WIDGET_INACTIVE = 'SNACKBAR_WIDGET_INACTIVE';

// Constants Pagination-widget
export const PAGINATION_WIDGET_GET = 'PAGINATION_WIDGET_GET';
export const PAGINATION_WIDGET_DELETE = 'PAGINATION_WIDGET_DELETE';

// Constants search-widget
export const SEARCH_WIDGET_UPDATE = 'SEARCH_WIDGET_UPDATE';