import { addTextToConstant } from '../utils/helperFunctions';

export const BASE_FORM_WIDGET_REQUEST = entityName => addTextToConstant(entityName, 'FORM_WIDGET_REQUEST');
export const BASE_FORM_WIDGET_SUCCESS =  entityName => addTextToConstant(entityName, 'FORM_WIDGET_SUCCESS');
export const BASE_FORM_MODEL_ERROR =  entityName => addTextToConstant(entityName, 'FORM_MODEL_ERROR');
export const BASE_URL_REQUEST =  entityName => addTextToConstant(entityName, 'URL_REQUEST');
export const BASE_MODEL_VIEW =  entityName => addTextToConstant(entityName, 'VIEW');
export const BASE_MODEL_GET =  entityName => addTextToConstant(entityName, 'GET');
export const BASE_MODEL_INIT =  entityName => addTextToConstant(entityName, 'MODEL_INIT');
export const BASE_MODEL_UPDATE =  entityName => addTextToConstant(entityName, 'MODEL_UPDATE');
export const BASE_MODEL_DELETE =  entityName => addTextToConstant(entityName, 'DELETE');
export const BASE_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64 = entityName => addTextToConstant(entityName, 'FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64');
export const BASE_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE = entityName => addTextToConstant(entityName, 'FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE');
export const BASE_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE = entityName => addTextToConstant(entityName, 'FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE');