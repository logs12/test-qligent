//========== /Constants react-redux-router app ==========//
export const ADMIN_ROUTE = '/admin';
export const DASHBOARD_ROUTE = `${ADMIN_ROUTE}/dashboard`;
export const ADMINISTRATION_ROUTE = `/administration`;
export const LOGIN_ROUTE = `/login`;
export const ERROR_ROUTE = `/error`;

// user
export const USERS_ROUTE = `${ADMIN_ROUTE}${ADMINISTRATION_ROUTE}/users`;
export const USER_CREATE_ROUTE = `${ADMIN_ROUTE}${ADMINISTRATION_ROUTE}/user/create`;
export const USER_VIEW_ROUTE = id => `${ADMIN_ROUTE}${ADMINISTRATION_ROUTE}/user/${id}`;
export const USER_UPDATE_ROUTE = id => `${ADMIN_ROUTE}${ADMINISTRATION_ROUTE}/user/update/${id}`;
export const USER_DELETE_ROUTE = id => `${ADMIN_ROUTE}${ADMINISTRATION_ROUTE}/user/delete/${id}`;

// News articles
export const NEWS_ARTICLE_ROUTE = `${ADMIN_ROUTE}/news-articles`;
export const NEWS_ARTICLE_CREATE_ROUTE = `${ADMIN_ROUTE}/news-article/create`;
export const NEWS_ARTICLE_VIEW_ROUTE = id => `${ADMIN_ROUTE}/news-article/${id}`;
export const NEWS_ARTICLE_UPDATE_ROUTE = id => `${ADMIN_ROUTE}/news-article/update/${id}`;
export const NEWS_ARTICLE_DELETE_ROUTE = id => `${ADMIN_ROUTE}/news-article/delete/${id}`;
export const NEWS_ARTICLE_FRONTEND_VIEW_ROUTE = id => `/news/${id}`;
export const NEWS_ARTICLE_FRONTEND_ROUTE = `/news-articles`;

export const ADVERTISEMENTS_ROUTE = `${ADMIN_ROUTE}/advertisements`;

// apartment-advertisement
export const APARTMENT_ADVERTISEMENTS_ROUTE = `${ADVERTISEMENTS_ROUTE}/apartment-advertisements`;
export const APARTMENT_ADVERTISEMENT_CREATE_ROUTE = `${ADVERTISEMENTS_ROUTE}/apartment-advertisement/create`;
export const APARTMENT_ADVERTISEMENT_VIEW_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/apartment-advertisement/${id}`;
export const APARTMENT_ADVERTISEMENT_UPDATE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/apartment-advertisement/update/${id}`;
export const APARTMENT_ADVERTISEMENT_DELETE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/apartment-advertisement/delete/${id}`;
export const APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE = id => `/apartment-advertisements/${id}`;
export const APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE = `/apartment-advertisements`;

// New apartment advertisement
export const NEW_APARTMENT_ADVERTISEMENTS_ROUTE = `${ADVERTISEMENTS_ROUTE}/new-apartment-advertisements`;
export const NEW_APARTMENT_ADVERTISEMENT_CREATE_ROUTE = `${ADVERTISEMENTS_ROUTE}/new-apartment-advertisement/create`;
export const NEW_APARTMENT_ADVERTISEMENT_VIEW_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/new-apartment-advertisement/${id}`;
export const NEW_APARTMENT_ADVERTISEMENT_UPDATE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/new-apartment-advertisement/update/${id}`;
export const NEW_APARTMENT_ADVERTISEMENT_DELETE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/new-apartment-advertisement/delete/${id}`;
export const NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE = id => `/new-apartment-advertisements/${id}`;
export const NEW_APARTMENT_ADVERTISEMENTS_FRONTEND_ROUTE = `/new-apartment-advertisements`;

// commercial estate advertisements
export const COMMERCIAL_ESTATE_ADVERTISEMENTS_ROUTE = `${ADVERTISEMENTS_ROUTE}/commercial-estate-advertisements`;
export const COMMERCIAL_ESTATE_ADVERTISEMENT_CREATE_ROUTE = `${ADVERTISEMENTS_ROUTE}/commercial-estate-advertisement/create`;
export const COMMERCIAL_ESTATE_ADVERTISEMENT_VIEW_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/commercial-estate-advertisement/${id}`;
export const COMMERCIAL_ESTATE_ADVERTISEMENT_UPDATE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/commercial-estate-advertisement/update/${id}`;
export const COMMERCIAL_ESTATE_ADVERTISEMENT_DELETE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/commercial-estate-advertisement/delete/${id}`;
export const COMMERCIAL_ESTATE_ADVERTISEMENT_FRONTEND_VIEW_ROUTE = id => `/commercial-estate-advertisements/${id}`;
export const COMMERCIAL_ESTATE_ADVERTISEMENTS_FRONTEND_ROUTE = `/commercial-estate-advertisements`;

// cottage houses land advertisements
export const COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE = `${ADVERTISEMENTS_ROUTE}/cottage-houses-land-advertisements-advertisements`;
export const COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE = `${ADVERTISEMENTS_ROUTE}/cottage-houses-land-advertisements-advertisement/create`;
export const COTTAGE_HOUSES_LAND_ADVERTISEMENT_VIEW_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/cottage-houses-land-advertisements-advertisement/${id}`;
export const COTTAGE_HOUSES_LAND_ADVERTISEMENT_UPDATE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/cottage-houses-land-advertisements-advertisement/update/${id}`;
export const COTTAGE_HOUSES_LAND_ADVERTISEMENT_DELETE_ROUTE = id => `${ADVERTISEMENTS_ROUTE}/cottage-houses-land-advertisements-advertisement/delete/${id}`;
export const COTTAGE_HOUSES_LAND_ADVERTISEMENT_FRONTEND_VIEW_ROUTE = id => `/cottage-houses-land-advertisements-advertisements/${id}`;
export const COTTAGE_HOUSES_LAND_ADVERTISEMENTS_FRONTEND_ROUTE = `/cottage-houses-land-advertisements-advertisements`;

// customer-requests
export const CUSTOMER_REQUESTS_ROUTE = `${ADMIN_ROUTE}/customer-requests`;
export const CUSTOMER_REQUEST_CREATE_ROUTE = `${ADMIN_ROUTE}/customer-request/create`;
export const CUSTOMER_REQUEST_VIEW_ROUTE = id => `${ADMIN_ROUTE}/customer-request/${id}`;
export const CUSTOMER_REQUEST_UPDATE_ROUTE = id => `${ADMIN_ROUTE}/customer-request/update/${id}`;
export const CUSTOMER_REQUEST_DELETE_ROUTE = id => `${ADMIN_ROUTE}/customer-request/delete/${id}`;
export const CUSTOMER_REQUEST_FRONTEND_VIEW_ROUTE = id => `/customer-requests/${id}`;

// News customer-requests
export const IMPORT_REPORTS_ROUTE = `${ADMIN_ROUTE}/import-reports`;
export const IMPORT_REPORT_CREATE_ROUTE = `${ADMIN_ROUTE}/import-report/create`;
export const IMPORT_REPORT_VIEW_ROUTE = id => `${ADMIN_ROUTE}/import-report/${id}`;
export const IMPORT_REPORT_DELETE_ROUTE = id => `${ADMIN_ROUTE}/import-report/delete/${id}`;

// About company
export const ABOUT_FRONTEND_ROUTE = `/about`;

// Vacancies
export const VACANCIES_FRONTEND_ROUTE = `/vacancies`;

// Contacts
export const CONTACTS_FRONTEND_ROUTE = `/contacts`;

// Page
export const PAGES_ROUTE = `${ADMIN_ROUTE}/pages`;
export const PAGE_CREATE_ROUTE =`${ADMIN_ROUTE}/page/create`;
export const PAGE_VIEW_ROUTE = id => `${ADMIN_ROUTE}/page/${id}`;
export const PAGE_UPDATE_ROUTE = id => `${ADMIN_ROUTE}/page/update/${id}`;
export const PAGE_DELETE_ROUTE = id => `${ADMIN_ROUTE}/page/delete/${id}`;
export const PAGE_FRONTEND_VIEW_ROUTE = id => `/page/${id}`;
export const PAGE_FRONTEND_ROUTE = `/pages`;