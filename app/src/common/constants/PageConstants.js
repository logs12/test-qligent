//========== /Constant page ==========//
export const PAGE_URL_REQUEST = (id = null) => (id) ? `/api-internal/page/${id}` : `/api-internal/page`;