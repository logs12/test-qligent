//========== /Constant customer request ==========//
export const CUSTOMER_REQUEST_URL_REQUEST = (id = null) => (id) ? `/api-internal/customer-request/${id}` : `/api-internal/customer-request`;