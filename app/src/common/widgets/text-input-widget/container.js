import React, { Component } from 'react';

import { addTextToConstant } from '../../utils/helperFunctions';
import InputTextComponent from "./component";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import { updateModelAction } from '../../actions/ModelActions';

export default function TextInputWidgetHoc(
    name,
    label,
    initFocused,
    entityName,
    className,
    typeField,
) {

    const mapStateToProps = state => ({
        name: name,
        label: label,
        initFocused: initFocused,
        entityName: entityName,
        model: state[entityName].model,
        className: className,
        typeField: typeField,
    });

    const mapDispatchToProps = dispatch => ({
        updateModelAction: bindActionCreators(updateModelAction, dispatch)
    });

    @connect(mapStateToProps, mapDispatchToProps)

    class TextInputWidgetContainer extends Component {

        /**
         *
         * @type {{type: string, reducerName: string, className: string, updateModelConstant: (())}}
         */
        static defaultProps = {
            updateModelConstant: addTextToConstant(entityName, 'MODEL_UPDATE'),
        };

        /**
         * Инициализируем состояния
         * @type {{error: null, inputTextValue: null}}
         */
        state = {
            error: null,
            inputTextValue: null,
        };

        componentWillMount() {
            this.setState({
                inputTextValue: this.props.model.values[this.props.name],
            });
        }

        /**
         * Хук на получение новых свойств
         * @param nextProps
         */
        componentWillReceiveProps(nextProps) {
            this.setState({
                inputTextValue: nextProps.model.values[this.props.name],
                error: nextProps.model.errors[this.props.name],
            });
        }

        /**
         * Обработчик события изменения input, отправка введеного значения в store
         */
        onChange(event) {
            event.preventDefault();

            let value = null;

            switch (this.props.typeField) {
                case 'checkbox': {
                    value = event.target.value === "off" ? 1 : 0;
                    break;
                }
                default: {
                    value = event.target.value !=='' ? event.target.value: null;
                    break;
                }
            }

            this.setState({
                inputTextValue: value,
            });

            // Передаем в редьюсер значение поля
            this.props.updateModelAction(
                this.props.updateModelConstant,
                {
                    [this.props.name]: value,
                }
            );
        }

        render () {

            const { name, label, className, initFocused, typeField } = this.props;

            const { inputTextValue, error } = this.state;

            return (
                <InputTextComponent
                    name = {name}
                    label = {label}
                    onChange = {::this.onChange}
                    error={error}
                    className={className}
                    initFocused={initFocused}
                    inputTextValue={inputTextValue}
                    typeField={typeField}
                />
            )
        }
    }

    return TextInputWidgetContainer;
}
