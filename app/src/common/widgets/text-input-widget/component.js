import React, { Component } from "react";
import TextField from 'react-mdl/lib/Textfield';
import Checkbox from 'react-mdl/lib/Checkbox';

export default class InputTextComponent extends Component {

    componentDidMount() {
        if (this.props.initFocused && this.props.inputTextValue === '') {
            this.inputContainer.childNodes[0].classList.add('is-focused');
            this.inputContainer.childNodes[0].childNodes[0].focus();
        }
    };

    /**
     *
     * @param typeField
     * @param onChange
     * @param error
     * @param label
     * @param inputTextValue
     * @returns {XML}
     */
    renderField(typeField, onChange, error, label, inputTextValue) {
        switch (typeField) {
            case 'checkbox': {
                let checked = false,
                    value = "off";
                if (inputTextValue) {
                    checked = true;
                    value ="on";
                } else {
                    checked = false;
                    value ="off";
                }

                return <Checkbox label={label}
                                 onChange={onChange}
                                 checked={checked}
                                 value={value}
                                 ripple
                />;
            }
            default: {
                return <TextField
                    onChange={onChange}
                    floatingLabel
                    error={error}
                    label={label}
                    value={(inputTextValue !== null) ? inputTextValue.toString() : ''}
                />
            }
        }
    }

    render() {
        const {
            className,
            onChange,
            error,
            label,
            inputTextValue,
            typeField,
        } = this.props;

        return (
            <div className={className}
                 ref={(inputContainer) => {this.inputContainer = inputContainer}}>
                {this.renderField(
                    typeField,
                    onChange,
                    error,
                    label,
                    inputTextValue,
                )}
            </div>
        );
    }
}
