import './style/style.scss'

import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import TextInputWidgetHoc from './container';
import classNames from 'classnames';

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название
 * entityName - название сущности со значением которой будет работатьь виджет
 * в глобальном state в свойстве reducerName, которому принадлежат данные
     <InputText
         name = {'password'}
         label = {'Пароль'}
         typeField = {'textField'},
         initFocused
     />
 */
export default class TextInputWidget extends Component {

    static defaultProps = {
        typeField: 'text',
        entityName: null,
        initFocused: false,
    };

    /**
     * Инициализируем контроль типов свойств
     * @type {{name: *, label: *, label: *}}
     */
    static propTypes = {
        type: PropTypes.string,
        name: PropTypes.string.isRequired,
        label: PropTypes.string,
        className: PropTypes.string,
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{url: *}}
     */
    static contextTypes = {
        entityNameForm: PropTypes.string.isRequired,
    };

    render () {
        const {
            name,
            label,
            initFocused,
            className,
            typeField,
        } = this.props;

        let entityName = null;

        if (this.context.entityNameForm) {
            entityName = this.context.entityNameForm;
        } else {
            entityName = this.props.entityName;
        }

        const classNameWidget = classNames('input-text-widget', className);

        return  createElement(TextInputWidgetHoc(
            name,
            label,
            initFocused,
            entityName,
            classNameWidget,
            typeField,
        ))
    }
};