import {
    SEARCH_WIDGET_UPDATE
} from '../../constants';

export function searchWidgetUpdateAction(data) {
    return {
        type: SEARCH_WIDGET_UPDATE,
        payload: {
            ...data
        },
    }
}
