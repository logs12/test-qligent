import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Textfield from 'react-mdl/lib/Textfield';

const SearchWidgetComponent = props => {
    return (
        <div className={props.className}>
            <Textfield
                floatingLabel
                onChange={props.onChange}
                label="Search"
                expandable
                expandableIcon="search"
            />
        </div>
    );
};

SearchWidgetComponent.propTypes = {
    onChange: PropTypes.func
};

export default SearchWidgetComponent;