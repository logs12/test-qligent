import basicClassCreator from 'react-mdl/lib/utils/basicClassCreator';

export default basicClassCreator('Spacer', 'mdl-layout-spacer');
