import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import {bindActionCreators} from "redux";

import ButtonLoadingComponent from './component';
import Spinner from 'react-mdl/lib/Spinner';
import { reset } from '../form-widget/actions';

import {
    FORM_WIDGET_INIT,
    FORM_WIDGET_REQUEST,
    FORM_WIDGET_SUCCESS,
    FORM_WIDGET_ERROR,
} from '../../constants';

export default function ButtonLoadingWidgetHoc (
    label,
    type,
    backToListUrl,
    entityNameForm,
    formName,
) {

    const mapStateToProps = (state) => ({
        label: label,
        type: type,
        backToListUrl: backToListUrl,
        entityNameForm,
        formName,
        formWidget: state.common.formWidget,
        model: state[entityNameForm].model
    });

    const mapDispatchToProps = dispatch => ({
        resetForm: bindActionCreators(reset, dispatch),
        dispatch: dispatch
    });

    @connect(mapStateToProps, mapDispatchToProps)

    class ButtonLoadingWidgetContainer extends Component{

        static propTypes = {
            className: PropTypes.string,
            label: PropTypes.string,
            backToListUrl: PropTypes.string,
            type: PropTypes.string,
        };

        /**
         * Дефолтные свойства
         * @type {{}}
         */
        static defaultProps = {
            reducerName: 'formWidget',
            label: 'Submit',
            className: '',
            backToListUrl: '/',
            type: 'default',
        };

        /**
         * Свойство выключения кнопки
         * @type {{disabled: boolean, isPending: boolean, buttonState, typeButton: null}}
         */
        state = {
            disabled: true,
            isPending: false,
            buttonState: FORM_WIDGET_INIT,
            typeButton: null,
            stateElement: null,
        };

        /**
         * Инициализируем контроль типов свойств контекста
         * @type {{entityNameForm: *, isPending: RegExp}}
         */
        static contextTypes = {
            entityNameForm: PropTypes.string.isRequired,
        };

        successElement = <div className="widget-button-loading__success-text widget-button-loading__success-text--icon mdl-color-text--green-600">
            <i className="material-icons">done</i>
        </div>;

        errorElement = <div className="widget-button-loading__error-text widget-button-loading__error-text--icon mdl-color-text--red-600">
            <i className="material-icons">error</i>
        </div>;


        /**
         * @param nextProps
         */
        componentWillReceiveProps(nextProps) {
            this.setState({
                disabled: !nextProps.model.isChanged, // Если isChanged == true, то выключаем кнопку disabled == false
            });
            this.stateElement = null;
            this.stateButton(nextProps.formWidget[nextProps.formName].stateForm);
        }

        /**
         * Визуализация состояния кнопки
         * @returns {*}
         */
        stateButton(buttonState) {
            switch (buttonState) {
                case FORM_WIDGET_REQUEST: {

                    switch (this.state.typeButton) {
                        case 'saveAndBackToList': {

                            this.setState({stateElement: <Spinner />});
                            break;
                        }
                        case 'saveAndMore': {

                            this.setState({stateElement: <Spinner />});
                            break;
                        }
                        case 'default': {

                            this.setState({stateElement: <Spinner />});
                            break;
                        }
                    }
                    break;
                }
                case FORM_WIDGET_SUCCESS: {
                    switch (this.state.typeButton) {
                        case 'saveAndBackToList': {
                            this.props.dispatch(push(this.props.backToListUrl));

                            this.setState({
                                typeButton: null,
                                stateElement: null
                            });
                            break;
                        }
                        case 'saveAndMore': {
                            this.props.resetForm(
                                this.props.entityNameForm,
                                this.props.model
                            );
                            this.props.dispatch(push(this.props.backToListUrl));

                            this.setState({
                                typeButton: null,
                                stateElement: this.successElement
                            });
                            break;
                        }
                        case 'default': {
                            this.setState({
                                typeButton: null,
                                stateElement: this.successElement
                            });
                            break;
                        }
                    }
                    break;
                }
                case FORM_WIDGET_ERROR: {
                    switch (this.state.typeButton) {
                        case 'saveAndBackToList': {
                            this.setState({
                                typeButton: null,
                                stateElement: this.errorElement,
                            });
                            break;
                        }
                        case 'saveAndMore': {
                            this.setState({
                                typeButton: null,
                                stateElement: this.errorElement,
                            });
                            break;
                        }
                        case 'default': {
                            this.setState({
                                typeButton: null,
                                stateElement: this.errorElement,
                            });
                            break;
                        }
                    }
                    break;
                }
            }

        }

        handleClickButton() {
            this.setState({
                typeButton: this.props.type,
            });
        }

        render () {

            const { className, label, type } = this.props;

            const { disabled, stateElement } = this.state;

            return (
                <ButtonLoadingComponent
                    className={className}
                    label={label}
                    disabled={disabled}
                    stateElement={stateElement}
                    type={type}
                    handleClickButton={::this.handleClickButton}
                >
                </ButtonLoadingComponent>
            )
        }
    }

    return ButtonLoadingWidgetContainer;
}
