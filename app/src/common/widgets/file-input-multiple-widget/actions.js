/**
 *
 * @param modelUpdateType
 * @param data
 * @returns {{type: *, payload: *}}
 */
export function actionFileInputMultiple(modelUpdateType, data) {
    return({
        type: modelUpdateType,
        payload: data,
    })
}