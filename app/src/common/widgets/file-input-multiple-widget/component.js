import "./style/style.scss";

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import mdlUpgrade from 'react-mdl/lib/utils/mdlUpgrade';
import IconButton from 'react-mdl/lib/IconButton';
import Icon from 'react-mdl/lib/Icon';

const propTypes = {
    className: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.node,
    id: PropTypes.string,
    inputClassName: PropTypes.string,
    title: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};

class FileInputMultipleComponent extends Component {


    fileContainer = [];

    componentWillMount() {
        this.renderFileContainer(this.props.files);
    }

    componentWillReceiveProps(nextProps) {
        this.renderFileContainer(nextProps.files);
    }

    /**
     * Rendering files container preview files
     * @param files
     */
    renderFileContainer(files) {

        // Clear array with preview
        this.fileContainer = [];
        if (files) {
            files.forEach((file, index) => {
                if (file) {
                    if (file.hasOwnProperty('base64')) {
                        this.fileContainer.push(
                            <div className="widget-file-input__container-file mdl-card" key={index}>
                                <div className="widget-file-input__container-file-header">
                                    <IconButton name="clear" onClick={(e) => this.props.handleDeleteFile(e,index)} />
                                </div>
                                <div className="widget-file-input__container-file-body">
                                    {this.renderPreviewUploadFile(file)}
                                </div>
                                <div className="widget-file-input__container-file-footer">
                                    <span>{file.name}</span>
                                </div>
                            </div>
                        );
                    }
                }
            });
        }
    }

    /**
     *
     * @param file
     * @returns {XML}
     */
    renderPreviewUploadFile(file) {
        if (file.type == 'image/jpeg') {
            return <img src={`data:${file.type};base64,${file.base64}`} />
        }
        return <Icon className="insert-drive-file" name="insert_drive_file" />
    }

    render() {
        const { className, inputClassName, buttonClassName, isActiveDropZone, error, title, handleChange, onDrop, onDragOver,
            onDragEnter, onDragLeave } = this.props;

        const errorContainer = !!error && <span className="mdl-textfield__error">{error}</span>;

        const mainContainerClassNames = classNames('widget-file-input', className);

        const dropZoneClassNames = classNames('widget-file-input_drop-zone', {
            'widget-file-input_drop-zone--active': isActiveDropZone,
        });

        const inputClassNames = classNames("widget-file-input__input",inputClassName);

        const buttonClasses = classNames(
            'mdl-button ' +
            'mdl-js-button ' +
            'mdl-button--raised ' +
            'mdl-js-ripple-effect ' +
            'mdl-button--primary ' +
            'mdl-button--file',
            buttonClassName
        );

        return  (
            <div className={mainContainerClassNames}>
                <div className={dropZoneClassNames}
                     onDrop={onDrop}
                     onDragOver={onDragOver}
                     onDragEnter={onDragEnter}
                     onDragLeave={onDragLeave}
                >
                    <button className={buttonClasses} type="button">
                        {title}
                        <input className={inputClassNames} type="file" onChange={handleChange} multiple="true"/>
                    </button>

                    <div className="widget-file-input__container">{this.fileContainer}</div>
                </div>
                <div className="mdl-textfield is-invalid">
                    {errorContainer}
                </div>
            </div>
        );
    }
}

FileInputMultipleComponent.propTypes = propTypes;

export default mdlUpgrade(FileInputMultipleComponent);

