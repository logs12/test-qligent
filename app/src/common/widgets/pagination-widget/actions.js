import {
    PAGINATION_WIDGET_GET,
    PAGINATION_WIDGET_DELETE,
} from '../../constants';

export function paginationWidgetGetAction(data) {
    return {
        type: PAGINATION_WIDGET_GET,
        payload: data,
    }
}

export function paginationWidgetDeleteAction() {
    return {
        type: PAGINATION_WIDGET_DELETE,
    }
}