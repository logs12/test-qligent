import {
    PAGINATION_WIDGET_GET,
    PAGINATION_WIDGET_DELETE,
} from '../../constants';

export default function PaginationWidgetReducer(state = {}, action) {
    switch(action.type) {
        case PAGINATION_WIDGET_GET: {
            return {
                ...state,
                ...action.payload
            };
        }
        // The removal to reduce the number of records
        case PAGINATION_WIDGET_DELETE: {
            let pagination = state;
            pagination.total = pagination.total - 1;
            return {
                ...state,
                ...pagination
            }
        }
    }

    return state;
}