import './style.scss';

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from "react-router-redux";

import PaginationComponent from './component';

/**
 *
 * @param options
 * @constructor
 */
export default function PaginationWidget(options) {

    let entityName = null;

    if (options.hasOwnProperty('entityName')) {
        entityName = options.entityName;
    } else {
        new Error('Not found parameters "entityName" in PaginationWidget');
    }

    const mapStateToProps = (state) => {
        return ({
            entityName: entityName,
            collection: state[entityName].collection,
            pagination: state[entityName].pagination,
            url: state.routing.locationBeforeTransitions.pathname,
        });
    };

    const mapDispatchToProps = dispatch => {
        return ({
            push: bindActionCreators(push, dispatch),
        });
    };
    return connect(mapStateToProps, mapDispatchToProps)(PaginationComponent);
}