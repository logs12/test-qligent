import './style.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import TableHeader  from './TableHeader';
import Table  from './Table';

export default class TableWidget extends Component {

    static propTypes = {
        actionsTableHeader: PropTypes.arrayOf(
            PropTypes.object
        ),
        rowMenuActions: PropTypes.object,
        attributes:PropTypes.object.isRequired
    };

    /**
     * Rendering table header
     * @param attributes {object}
     * @returns {Array}
     */
    renderTableHeader(attributes) {
        let componentTableHeader = [];
        if (!isEmpty(attributes)) {
            let key = 0;
            for (let attribute in attributes) {
                if (!attributes.hasOwnProperty(attribute)) continue;
                const { title, ...otherProps } = attributes[attribute];
                componentTableHeader.push(
                    <TableHeader key = {key} name = {attribute} { ...otherProps.propsTableHeader }>{title}</TableHeader>
                );
                key++;
            }
        }
        return componentTableHeader;
    }

    render() {

        const {
            actionsTableHeader,
            rowMenuActions,
            attributes,
            collectionName,
            actionName,
            ...otherProperties
        } = this.props;

        return(
            <Table
                actionsTableHeader={actionsTableHeader}
                rowMenuActions={rowMenuActions}
                collectionName = {collectionName}
                actionName = {actionName}
                attributes={attributes}
                { ...otherProperties }
            >
                {this.renderTableHeader(attributes)}
            </Table>
        )
    }
}


