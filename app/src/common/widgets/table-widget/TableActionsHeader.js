import React, { Component, createElement } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import Tooltip from 'react-mdl/lib/Tooltip';
import Icon from 'react-mdl/lib/Icon';
import FABButton from 'react-mdl/lib/FABButton';
import SearchWidget from '../search-widget/container';

const mapDispatchTpProps = dispatch => ({
    push: bindActionCreators(push, dispatch)
});

@connect(null, mapDispatchTpProps)
export default class TableActionsHeader extends Component {

    static defaultProps = {
        actions: [],
    };

    /**
     * Render actions button
     * @returns {Array}
     */
    renderActionButton() {
        let actionComponents = [];
        if (this.props.actions.length) {
            this.props.actions.forEach((action, index) => {
                actionComponents.push(
                    <Tooltip key={index} label={action.title}>
                        <FABButton mini className="table-widget__action-button">
                            <Icon name={action.iconName} onClick = {() => {this.props.push(action.link)}} />
                        </FABButton>
                    </Tooltip>
                );
            });
        }
        return actionComponents;
    }

    render() {

        const { searchOptions } = this.props;

        return(
            <div className="table-widget__actions">
                <div className="table-widget__links">
                    {this.renderActionButton()}
                </div>

                <div className="mdl-layout-spacer"></div>

                <div className="table-widget__tools">
                    {createElement(SearchWidget(searchOptions))}
                </div>

            </div>
        )
    }
}