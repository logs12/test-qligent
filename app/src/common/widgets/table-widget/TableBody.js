import React, { Component } from 'react';
import { connect } from 'react-redux';
import TableRow from './TableRow';
import { bindActionCreators } from "redux";
import * as actions from '../../actions';

/**
 * HOC component for render table body
 * @param entityName
 * @param actionName
 * @param columnChildren
 * @param rowKeyColumn
 * @param rowMenuActions
 * @param attributes
 * @param urlGetRequest
 * @returns {TableBodyContainer}
 * @constructor
 */
export default function TableBody(
    entityName,
    actionName,
    columnChildren,
    rowKeyColumn,
    rowMenuActions,
    attributes,
    urlGetRequest,
) {
    const mapStateToProps = state => {
        return {
            collection: state[entityName].collection,
            searchValue: state[entityName].search.searchValue,
            columnChildren: columnChildren,
            rowKeyColumn: rowKeyColumn,
            rowMenuActions: rowMenuActions,
            condition: state.routing.locationBeforeTransitions.search,
            actionName: actionName,
            attributes:attributes,
            entityName: entityName,
            urlGetRequest: urlGetRequest,
        }
    };

    const mapDispatchToProps = dispatch => ({
        getCollection: bindActionCreators(actions[actionName], dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class TableBodyContainer extends Component {


        componentWillMount() {
            this.getActionName({condition: this.props.condition});
        }

        componentWillReceiveProps(nextProps) {
            if (this.props.condition !== nextProps.condition) {
                this.getActionName({condition: nextProps.condition});
            }
        }

        getActionName(options) {
            if (this.props.actionName === 'baseGetAction') {
                return this.props.getCollection(this.props.urlGetRequest, options, this.props.entityName);
            }
            return this.props.getCollection(options);
        }

        render() {

            const {
                collection,
                columnChildren,
                rowKeyColumn,
                rowMenuActions,
                searchValue,
                attributes,
                entityName,
                urlGetRequest,
            } = this.props;

            return (
                <tbody>
                {
                    collection.map((model, idx) => {
                        return <TableRow
                            columnChildren={columnChildren}
                            rowKeyColumn={rowKeyColumn}
                            rowMenuActions={rowMenuActions}
                            model={model}
                            idx={idx}
                            key={idx}
                            searchValue={searchValue}
                            attributes={attributes}
                            entityName={entityName}
                            urlGetRequest={urlGetRequest}
                        />
                    })
                }
                </tbody>
            )
        }
    }

    return TableBodyContainer;
}

