import React, { createElement } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { push } from 'react-router-redux';
import IconButton from 'react-mdl/lib/IconButton';
import Tooltip from 'react-mdl/lib/Tooltip';
import Menu, { MenuItem } from 'react-mdl/lib/Menu';
import * as actions from '../../actions';

/**
 *
 * @param rowMenuActions
 * @param menuItemKey
 * @param onclickHandler
 * @returns {{menuItemComponent: Array, actionButtonIconComponent: Array, onClick: *}}
 */
const renderActionComponent = (rowMenuActions, menuItemKey, onclickHandler) => {

    let menuItemComponent = [];
    let actionButtonIconComponent = [];
    let onClick = null;

    let clickRow = rowMenuActions.settings && rowMenuActions.settings.hasOwnProperty('clickRow') ?
            rowMenuActions.settings.clickRow : false,

        showInMenu = rowMenuActions.settings && rowMenuActions.settings.hasOwnProperty('showInMenu') ?
            rowMenuActions.settings.showInMenu : false,

        nameIconButton = rowMenuActions.settings && rowMenuActions.settings.hasOwnProperty('nameIconButton') ?
            rowMenuActions.settings.nameIconButton : 'arrow_drop_down_circle';

    if (clickRow) {
        onClick = onclickHandler;
    } else {
        if (showInMenu) {
            menuItemComponent.push(
                <MenuItem key={menuItemKey}
                          onClick={onclickHandler}>
                    {rowMenuActions.title}
                </MenuItem>
            );
        } else {
            actionButtonIconComponent.push(
                <Tooltip label={rowMenuActions.title}>
                    <IconButton key={menuItemKey}
                                name={nameIconButton}
                                onClick={onclickHandler} />
                </Tooltip>
            );
        }
    }

    return {menuItemComponent, actionButtonIconComponent, onClick}
};

/**
 * Render row actions menu
 * @param rowMenuActions
 * @param model
 * @param idx
 * @param columnChildren
 * @param dispatch
 * @param searchValue
 * @param attributes
 * @param entityName
 * @param urlGetRequest
 * @returns {*|Array}
 */
const renderRow = (
        rowMenuActions,
        model,
        idx,
        columnChildren,
        dispatch,
        searchValue,
        attributes,
        entityName,
        urlGetRequest,
    ) => {

    let menuItemComponents = [];
    let actionButtonIconComponents = [];

    // Handler on td table
    let onClickCell = null;

    let menuItemKey = 0;

    for(let rowMenuAction in rowMenuActions) {

        if (!rowMenuActions.hasOwnProperty(rowMenuAction)) continue;

        switch (rowMenuAction) {
            case 'actionView': {

                let { menuItemComponent, actionButtonIconComponent , onClick} = {...renderActionComponent(
                    rowMenuActions[rowMenuAction],
                    menuItemKey,
                    () => dispatch(push(rowMenuActions[rowMenuAction].url(model['id']))),
                )};

                if (menuItemComponent.length) { menuItemComponents.push(menuItemComponent) }
                if (actionButtonIconComponent.length) { actionButtonIconComponents.push(actionButtonIconComponent) }
                onClickCell = onClickCell ? onClickCell : onClick;
                break;
            }
            case 'actionUpdate': {
                let { menuItemComponent, actionButtonIconComponent , onClick} = {...renderActionComponent(
                    rowMenuActions[rowMenuAction],
                    menuItemKey,
                    () => dispatch(push(rowMenuActions[rowMenuAction].url(model['id']))),
                )};

                if (menuItemComponent.length) { menuItemComponents.push(menuItemComponent) }
                if (actionButtonIconComponent.length) { actionButtonIconComponents.push(actionButtonIconComponent) }
                onClickCell = onClickCell ? onClickCell : onClick;
                break;
            }
            case 'actionDelete': {
                let action = null;
                if (rowMenuActions[rowMenuAction].hasOwnProperty('dispatchAction')) {
                    action = actions[rowMenuActions[rowMenuAction].dispatchAction](model['id']);
                } else {
                    action = actions['baseDeleteAction'](
                        urlGetRequest,
                        model['id'],
                        entityName,
                    );
                }

                let { menuItemComponent, actionButtonIconComponent , onClick} = {...renderActionComponent(
                    rowMenuActions[rowMenuAction],
                    menuItemKey,
                    () => {dispatch(action)},
                )};

                if (menuItemComponent.length) { menuItemComponents.push(menuItemComponent) }
                if (actionButtonIconComponent.length) { actionButtonIconComponents.push(actionButtonIconComponent) }
                onClickCell = onClickCell ? onClickCell : onClick;
                break;
            }
        }
        menuItemKey++;
    }

    let menuId = `menu-lower-right${model['id']}`;

    // Push usually td
    let cellComponent = columnChildren.map((child, index) => {

        const className = classNames(
            !child.props.numeric ? 'mdl-data-table__cell--non-numeric' : '',
            onClickCell ? 'mdl-data-table__cell--cursor-pointer' : ''
        );

        let cellValue = null;
        let attribute = child.props.name;

        if (attributes[attribute].hasOwnProperty('value')) {
            cellValue = attributes[child.props.name].value(model[attribute]);
        } else {
            cellValue = model[attribute];
        }

        if (searchValue) {
            let regex = new RegExp(searchValue, 'gi');
            cellValue = String(cellValue);
            cellValue = cellValue.replace(regex, '<span class="mdl-color--green-200">$&</span>');
            return <td key={index} className={className} onClick={onClickCell} dangerouslySetInnerHTML={{__html: cellValue}}></td>
        }

        return <td key={index} className={className} onClick={onClickCell}>
            {child.props.cellFormatter ? child.props.cellFormatter(cellValue, model, idx) : cellValue}
        </td>
    });

    // Push action td
    cellComponent.push(
        <td key={cellComponent.length} >
            <div className="table-widget__action-cell">
                <div className="table-widget__action-button-icon">
                    {actionButtonIconComponents}
                </div>
                {menuItemComponents.length ?
                    <div className="table-widget__action-menu">
                        <IconButton name="more_vert" id={menuId}/>
                        <Menu target={menuId} align="right">
                            {menuItemComponents}
                        </Menu>
                    </div>
                : null}
            </div>
        </td>
    );
    return cellComponent;

};

const TableRow = (props) => {

    const {
        columnChildren,
        rowKeyColumn,
        rowMenuActions,
        model,
        idx,
        dispatch,
        searchValue,
        attributes,
        entityName,
        urlGetRequest,
    } = { ...props};

    const { className: mdlRowPropsClassName, ...remainingMdlRowProps } = model.mdlRowProps || {};

    return (
        <tr
            key={model[rowKeyColumn] || model.key || idx}
            className={classNames(model.className, mdlRowPropsClassName)}
            {...remainingMdlRowProps}
        >
            {renderRow(
                rowMenuActions,
                model,
                idx,
                columnChildren,
                dispatch,
                searchValue,
                attributes,
                entityName,
                urlGetRequest,
            )}
        </tr>
    );
};

export default connect()(TableRow)