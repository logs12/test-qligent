import {
    INIT_SELECT2_WIDGET,
    UPDATE_VALUE_SELECT2_WIDGET,
} from './constants';

/**
 *
 * @param state - глобальный объект с состоянием проекта
 * @param action - действие
 * @returns {{}}
 * @constructor
 */
export default function Select2WidgetReducer(state = {}, action) {

    switch (action.type) {
        // Обновляем state Select2Widget при вводе данных в input
        case INIT_SELECT2_WIDGET: {
            return {
                ...state,
                [action.nameSelect2Widget]: {
                    value: action.value,
                }
            };
        }

        // Обновляем state Select2Widget при вводе данных в input
        case UPDATE_VALUE_SELECT2_WIDGET: {
            return {
                ...state,
                [action.nameSelect2Widget]: {
                    ...state[action.nameSelect2Widget],
                    value: action.value,
                }
            };
        }
    }

    return state;
}

