import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import Select2WidgetContainerHoc from './container';

/**
 * Widget select2, for relation one-to-many
 *
     <Select2Widget
         entityName="Floor"
         name="building_id"
         nameHidden="building_address"
         label="Building"
         relationGetActionName="buildingsGetAction"
         relationCollectionName="Buildings"
         relationFieldId="id"
         relationFieldName="address_full"
         relationFieldsToSearch={['address_one', 'address_two']}
     />
 name - field id relation current model
 nameHidden - field title relation current model
 label - label input widget
 entityName - reducer for handler widget
 relationGetActionName - action fo get relation collection
 relationCollectionName - name relation collection name,
 relationFieldId - name relation field id,
 relationFieldName - name relation field text,
 className,
 initFocused,
 relationFieldsToSearch,
 */
export default class Select2Widget extends Component {

    /**
     * Инициализируем контроль типов свойств
     * @type {{name: *, placeholder: *, label: *}}
     */
    static propTypes = {
        entityName: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        actionName: PropTypes.string,
        urlSubmit: PropTypes.string,
        relationGetActionName: PropTypes.string.isRequired,
        relationCollectionName: PropTypes.string.isRequired,
        relationFieldId: PropTypes.string.isRequired,
        relationFieldName: PropTypes.string.isRequired,
        relationFieldsToSearch: PropTypes.array,
    };


    /**
     * @param entityName - название reducer
     * @type {{actionName: string}}
     */
    static defaultProps = {
        type: 'text',
        urlSubmit: '/',
        pageSize: 10,
        initFocused: false,
        relationFieldsToSearch: [],
    };

    render () {

        const {
            name,
            nameHidden,
            entityName,
            label,
            relationGetActionName,
            relationCollectionName,
            relationFieldId,
            relationFieldName,
            initFocused,
            relationFieldsToSearch,
        } = this.props;

        const updateModelConstant = `${entityName.toUpperCase()}_MODEL_UPDATE`;

        return createElement(Select2WidgetContainerHoc(
            name,
            nameHidden,
            entityName,
            label,
            relationGetActionName,
            relationCollectionName,
            relationFieldId,
            relationFieldName,
            updateModelConstant,
            initFocused,
            relationFieldsToSearch,
        ));
    }
}