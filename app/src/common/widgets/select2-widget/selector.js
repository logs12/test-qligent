import { createSelector } from 'reselect';
import find from 'lodash/find';

/**
 * Selected Collection Building
 * @param state
 * @returns {*}
 */
const collectionSelector = (collection) => {
    debugger;
    return state.Building.collection;
};

/**
 * id for filtering collection
 * @param state
 * @param selectedBuildingId
 */
const selectedBuildingSelector = (state, selectedBuildingId) => (Number(selectedBuildingId));

/**
 * Filtering collection
 * @param buildings
 * @param selectedBuildingId
 * @returns {*|{}|T}
 */
const getBuildingSelector = (buildings, selectedBuildingId) => {
    return find(buildings, {id: selectedBuildingId});
};


export const getModel = createSelector(
    collectionSelector,
    selectedBuildingSelector,
    getBuildingSelector,
);
