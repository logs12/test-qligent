import React, { Component } from 'react';
import Select2WidgetComponent from "./component";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import * as actions from '../../actions';
import * as select2WidgetActions  from './actions';
import { updateModelAction } from '../../actions/ModelActions';

import DropDownWidget from '../dropdown-widget';


import { ACTION_NAME_SELECT2_WIDGET } from './constants';


export default function Select2WidgetContainerHoc(
    name,
    nameHidden,
    entityName,
    label,
    relationGetActionName,
    relationCollectionName,
    relationFieldId,
    relationFieldName,
    updateModelConstant,
    initFocused,
    relationFieldsToSearch,
) {
    const mapStateToProps = state => {
        return {
            dataWidget: state.common['select2Widget'][name],
            model: state[entityName].model,
            name: name,
            nameHidden: nameHidden,
            entityName: entityName,
            label: label,
            relationGetActionName: relationGetActionName,
            relationCollection: state[relationCollectionName].collection,
            relationFieldId: relationFieldId,
            relationFieldName: relationFieldName,
            updateModelConstant:updateModelConstant,
            initFocused: initFocused,
            relationFieldsToSearch: relationFieldsToSearch,
        }
    };

    const mapDispatchToProps = dispatch => ({
        relationGetAction: bindActionCreators(actions[relationGetActionName], dispatch),
        initSelect2Widget: bindActionCreators(select2WidgetActions.initSelect2Widget, dispatch),
        changeInputSelect2Widget: bindActionCreators(select2WidgetActions.changeInputSelect2Widget, dispatch),
        updateModelAction: bindActionCreators(updateModelAction, dispatch),
    });

    /**
     * Подключение к reduxStore
     */
    @connect(mapStateToProps, mapDispatchToProps)
    class Select2WidgetContainer extends Component {

        /**
         * Инициализируем состояния
         * @type {{error: Array}}
         */
        state = {
            error: null,
            inputValue: null,
            showDropDown: false,
        };

        inputTimer = null;

        static defaultProps = {
            actionName: ACTION_NAME_SELECT2_WIDGET,
        };

        /**
         *  Init select2-widget
         */
        componentWillMount() {

            let inputInitValue = this.props.model.values[this.props.nameHidden];
            this.setState({
                inputValue: inputInitValue,
            });
        }

        /**
         * Хук на получение новых свойств
         * @param nextProps
         */
        componentWillReceiveProps(nextProps) {
            this.setState({
                error: nextProps.model.errors[this.props.name],
            });
        }


        /**
         * Handler click dropdown widget item
         * @param {function} event
         * @param {string} selectId - value id field of the model
         * @param {string} selectValue - value text field of the model
         */
        handlerClickDropDownItem(event, selectId, selectValue) {
            event.preventDefault();

            this.props.updateModelAction(
                this.props.updateModelConstant,
                {
                    [this.props.name]: selectId,
                    [this.props.nameHidden]: selectValue,
                }
            );

            this.setState({
                showDropDown: false,
                inputValue: selectValue,
            });
        }

        /**
         * Clear input select2Widget
         */
        handlerClickClearInput() {
            this.setState({
                inputValue: '',
            });
        }

        /**
         * Handler change event on input select2-widget
         * @param event
         */
        onChangeInput(event) {
            event.preventDefault();
            let value = event.target.value;

            this.setState({
                inputValue: value,
            });

            // Send search request to the server
            this.sendSearchRequest(value);
        }

        /**
         * Send search request, show dropdown and send input value to the state
         * @param event
         */
        onFocusInput(event) {
            event.preventDefault();

            const currentValueInput = this.props.model.values[this.props.nameHidden];

            // Send search request
            this.sendSearchRequest(currentValueInput);
        }

        /**
         * Hide dropdown and set default value input
         * @param event
         */
        onBlurInput(event) {
            event.preventDefault();

            const currentValueInput = this.props.model.values[this.props.nameHidden];

            this.setState({
                showDropDown: false,
                inputValue: currentValueInput,
            });
        }

        /**
         * Send search request to the server
         * @param {string} searchValue - search value
         */
        sendSearchRequest(searchValue) {
                let conditionGetParametres = '?',
                currentId =  this.props.model.values[this.props.name];

            conditionGetParametres += 'condition'+
                encodeURIComponent('['+this.props.relationFieldId+'][]')+
                '='+encodeURIComponent('not in')+
                '&condition'+
                encodeURIComponent('['+this.props.relationFieldId+'][1][]')+
                '='+currentId;

            if (this.props.relationFieldsToSearch.length) {
                this.props.relationFieldsToSearch.forEach((fieldToSearch) => {
                    conditionGetParametres += '&condition' +
                        encodeURIComponent('['+fieldToSearch+']')+
                        '='+
                        encodeURIComponent(searchValue);
                });
            } else {
                conditionGetParametres += '&condition' +
                    encodeURIComponent('[' + relationFieldName + ']') +
                    '=' +
                    encodeURIComponent(searchValue);
            }

            // Delay sending search request
            clearTimeout(this.inputTimer);

            this.inputTimer = setTimeout(() => {
                this.props.relationGetAction({
                    condition: conditionGetParametres,
                    onSuccess: () => {
                        this.setState({
                            showDropDown: true,
                        });
                    }
                });
            }, 400);
        }

        /**
         * Generation content for dropdown
         * @param {collection} relationCollection - relation collection
         * @param {string} relationFieldId - name relation field id
         * @param {string} relationFieldName - name relation field
         * @param {function} handlerClickDropDownItem
         * @param {string} searchValue - search value
         * @returns {Array}
         */
        generationInnerContentForDropDown(
            relationCollection,
            relationFieldId,
            relationFieldName,
            handlerClickDropDownItem,
            searchValue
        ) {

            let innerContentForUlMenu = [];

            if (relationCollection.length) {
                relationCollection.forEach((model, index) => {

                    let liValue = null;
                    if (searchValue != '') {
                        let regex = new RegExp(searchValue, 'gi');
                        liValue = String(model[relationFieldName]);
                        liValue = liValue.replace(regex, '<span class="mdl-color--green-200">$&</span>');

                        innerContentForUlMenu.push(
                            <li key={index}
                                className="dropdown-widget__item"
                                onMouseDown={(event) => { handlerClickDropDownItem(
                                    event,
                                    model[relationFieldId],
                                    model[relationFieldName],
                                )}}
                                dangerouslySetInnerHTML={{__html: liValue}}
                            >
                            </li>
                        );
                    }
                });
            } else {
                innerContentForUlMenu.push(
                    <li
                        key={0}
                        className="dropdown-widget__item">
                        Not found
                    </li>
                );
            }

            return innerContentForUlMenu;
        }


        render () {

            const {
                name,
                label,
                initFocused,
                relationCollection,
                relationFieldId,
            } = this.props;

            const { inputValue, showDropDown, error } = this.state;

            return (
                <div className="widget-select2">
                    <Select2WidgetComponent
                        name={name}
                        label={label}
                        inputValue={(inputValue !== null) ? inputValue.toString() : ''}
                        error={error}
                        initFocused={initFocused}
                        onChangeInput={::this.onChangeInput}
                        onFocusInput={::this.onFocusInput}
                        onBlurInput={::this.onBlurInput}
                        relationCollection={relationCollection}
                        relationFieldName={relationFieldName}
                        floatingLabel
                        handlerClickClearInput={::this.handlerClickClearInput}
                    />

                    <DropDownWidget showDropDown={showDropDown}>
                        {this.generationInnerContentForDropDown(
                            relationCollection,
                            relationFieldId,
                            relationFieldName,
                            ::this.handlerClickDropDownItem,
                            inputValue
                        )}
                    </DropDownWidget>
                </div>
            )
        }
    }

    return Select2WidgetContainer;
}