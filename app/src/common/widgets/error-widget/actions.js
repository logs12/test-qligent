import {
    ERROR_WIDGET_CLIENT,
    ERROR_WIDGET_SERVER,
    ERROR_WIDGET_RESET,
} from '../../constants';

export function resetError() {
    return dispatch => dispatch({
        type: ERROR_WIDGET_RESET,
    });
}