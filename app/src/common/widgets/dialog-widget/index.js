import './style.scss';

import React, { Component } from 'react';
import Button from 'react-mdl/lib/Button';
import DialogWidgetComponent from './component';


export default class DialogWidget extends Component {

    state = {
        openDialog: false,
    };

    handleOpenDialog() {
        this.setState({
            openDialog: true
        });
    }

    handleCloseDialog() {
        this.setState({
            openDialog: false
        });
    }



    render () {
        const {
            children,
            buttonTitle,
            classNameButton,
        } = this.props;

        return (
            <div className="dialog-widget">
                <Button className="dialog-widget__button" onClick={this.handleOpenDialog} >Войти</Button>
                <DialogWidgetComponent onOpen={::this.state.openDialog} onCancel={::this.handleCloseDialog}>
                    <span>X</span>
                    {children}
                </DialogWidgetComponent>
            </div>
        );
    }
}