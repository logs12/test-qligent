import './style.scss';

import React, { Component } from 'react';


import Button from 'react-mdl/lib/Button';
import  { Dialog, DialogTitle, DialogContent, DialogActions } from 'react-mdl/lib/Dialog';

export default class DialogWidgetComponent extends Component {

    state = {
        openDialog: false,
    };

    handleOpenDialog() {
        this.setState({
            openDialog: true
        });
    }

    handleCloseDialog() {
        this.setState({
            openDialog: false
        });
    }



    render () {
        const {
            titleDialog,
            contentDialog
        } = this.props;

        return (
            <Dialog open={this.state.openDialog} onCancel={::this.handleCloseDialog}>
                <DialogTitle>{titleDialog}</DialogTitle>
                <DialogContent>{contentDialog}</DialogContent>
                <DialogActions>
                    <Button type='button'>Agree</Button>
                    <Button type='button' onClick={::this.handleCloseDialog}>Disagree</Button>
                </DialogActions>
            </Dialog>
        );
    }
}