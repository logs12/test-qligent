import { addTextToConstant } from '../../utils/helperFunctions';

import React, { Component } from 'react';
import classNames from 'classnames';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import { updateModelAction } from '../../actions/ModelActions';
import TextDraftWysiwygWidgetComponent from './component';

export default function TextDraftWysiwygWidgetHoc(
    name,
    label,
    initFocused,
    entityName,
    actionName,
    className,
    placeholder,
) {

    const mapStateToProps = state => ({
        name: name,
        label: label,
        initFocused: initFocused,
        entityName: entityName,
        model: state[entityName].model,
        actionName: actionName,
        className: className,
        placeholder: placeholder,
    });

    const mapDispatchToProps = dispatch => ({
        updateModelAction: bindActionCreators(updateModelAction, dispatch)
    });

    @connect(mapStateToProps, mapDispatchToProps)

    class TextDraftWysiwygWidgetContainer extends Component {

        /**
         * @param
         * @type {{actionName: string}}
         */
        static defaultProps = {
            updateModelConstant: addTextToConstant(entityName, 'MODEL_UPDATE'),
        };

        /**
         * Инициализируем состояния
         * @type {{errors: Array}}
         */
        state = {
            error: null,
            value: null,
        };

        componentWillMount() {
            this.setState({
                value: this.props.model.values[this.props.name],
            });
        }

        /**
         * Хук на получение новых свойств
         * @param nextProps
         */
        componentWillReceiveProps(nextProps) {
            this.setState({
                value: nextProps.model.values[this.props.name],
                error: nextProps.model.errors[this.props.name]
            });
        }


        /**
         * Обработчик события изменения input, отправка введеного значения в store
         */
        handleChangeTextDraftWysiwygWidgetComponent(value) {
            // Передаем в редьюсер значение поля
            this.props.updateModelAction(
                this.props.updateModelConstant,
                {
                    [this.props.name]: value,
                }
            );

            this.setState({
                value: value,
            });
        }

        render () {

            const { label, className, placeholder, initFocused } = this.props;

            const { value, error } = this.state;

            const classNameWidget = classNames('draft-wysiwyg-widget', className);

            return (

                <div className={classNameWidget}>
                    <span className="draft-wysiwyg-widget__label">{label}</span>
                    <TextDraftWysiwygWidgetComponent
                        theme={'snow'}
                        handleChangeTextDraftWysiwygWidgetComponent={::this.handleChangeTextDraftWysiwygWidgetComponent}
                        initValue={(value !== null) ? value.toString() : ''}
                        placeholder={placeholder}
                    />
                    <span className="draft-wysiwyg-widget__error-field">{error}</span>
                </div>
            )
        }
    }

    return TextDraftWysiwygWidgetContainer;
}
