import './style/style.scss';

import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import TextDraftWysiwygWidgetHoc from './container';

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название
 * entityName - название сущности со значением которой будет работатьь виджет
 * в глобальном state в свойстве reducerName, которому принадлежат данные
     <TextDraftWysiwygWidget
         name = 'password'
         label = 'Пароль'
         placeholder = 'Текст'
         theme = 'snow'
         className = 'class'
         initFocused
     />
 */
export default class TextDraftWysiwygWidget extends Component {

    static defaultProps = {
        theme: 'snow',
        entityName: null,
        initFocused: false,
        className: '',
        placeholder: 'Текст'
    };

    /**
     * Инициализируем контроль типов свойств контекста
     * @type {{url: *}}
     */
    static contextTypes = {
        entityNameForm: PropTypes.string.isRequired,
    };

    render () {
        const {
            name,
            label,
            initFocused,
            actionName,
            className,
            placeholder,
        } = this.props;

        let entityName = null;

        if (this.context.entityNameForm) {
            entityName = this.context.entityNameForm;
        } else {
            entityName = this.props.entityName;
        }

        return  createElement(TextDraftWysiwygWidgetHoc(
            name,
            label,
            initFocused,
            entityName,
            actionName,
            className,
            placeholder,
        ))
    }
};