import 'react-quill/dist/quill.snow.css';
import 'react-quill/dist/quill.core.css';
import 'react-quill/dist/quill.bubble.css';

import React, { Component } from "react";

import ReactQuill from 'react-quill';


export default class TextDraftWysiwygWidgetComponent extends Component {

    modules = {
        toolbar: [
            [
                { 'header': [1, 2, 3, 4,  false] },
                { 'font': [] }],
            [
                'bold', 'italic', 'underline', 'strike', 'blockquote'
            ],
            [
                {'list': 'ordered'}, {'list': 'bullet'},
                {'indent': '-1'}, {'indent': '+1'}
            ],
            ['link', 'image', 'video'],
            ['clean']
        ]
    };

    formats = [
        'header', 'font', 'size',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image', 'video'
    ];

    render() {
        const { theme, placeholder, initValue, handleChangeTextDraftWysiwygWidgetComponent } = this.props;

        return (
            <ReactQuill
                theme={theme}
                onChange={handleChangeTextDraftWysiwygWidgetComponent}
                value={initValue}
                modules={this.modules}
                formats={this.formats}
                placeholder={placeholder}
            />
        );
    }
}
