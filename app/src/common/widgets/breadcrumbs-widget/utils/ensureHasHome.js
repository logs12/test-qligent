import mapBreadcrumb from './mapBreadcrumb'

export default function ensureHasHome (breadcrumbs = [], routes) {
  //debugger;
  if (breadcrumbs && breadcrumbs.length) return breadcrumbs;
  const first = routes && routes[1];/*routes.filter((route) => {
          return route.hasOwnProperty('indexRoute');
      });*/
  return first && [...breadcrumbs, mapBreadcrumb(first)] || breadcrumbs;
}
