import {
    FORM_WIDGET_INIT,
    FORM_WIDGET_REQUEST,
    FORM_WIDGET_SUCCESS,
    FORM_WIDGET_ERROR,
    FORM_WIDGET_RESET,
} from '../../constants';

/**
 * Структура state виджета Form
 *
 * Все actions работающие с виджетом Form должны иметь обязательные типы, заканчивающиеся на
 * _REQUEST,
 * _ERROR,
 * _SUCCESS
 *
 * @param state - глобальный объект с состоянием проекта
 * @param action - действие
 * @returns {{}}
 * @constructor
 */

export default function FormWidgetReducer(state = {}, action) {

    // Разбиваем тип action по разделителю
    let typeActionAfterSplit = action.type.split('_');
    
    let typeAction = typeActionAfterSplit[typeActionAfterSplit.length-3] + '_'
        + typeActionAfterSplit[typeActionAfterSplit.length-2] + '_'
        + typeActionAfterSplit[typeActionAfterSplit.length-1];
    
    // Обработка actions при submit формы
    switch (typeAction) {
    
        // обработка REQUEST, меняем isPending на true
        case FORM_WIDGET_INIT: {
            return {
                ...state,
                [action.payload.formName]: {
                    stateForm: FORM_WIDGET_INIT,
                }
            }
        }


        // обработка REQUEST, меняем isPending на true
        case FORM_WIDGET_REQUEST: {
            return {
                ...state,
                [action.options.formName]: {
                    ...state[action.options.formName],
                    stateForm: FORM_WIDGET_REQUEST,
                }
            }
        }

        case FORM_WIDGET_SUCCESS: {
            return {
                ...state,
                [action.options.formName]: {
                    ...state[action.options.formName],
                    stateForm: FORM_WIDGET_SUCCESS,
                }
            }
        }
        case FORM_WIDGET_ERROR: {
            return {
                ...state,
                [action.options.formName]: {
                    ...state[action.options.formName],
                    stateForm: FORM_WIDGET_ERROR,
                }
            }
        }

        case FORM_WIDGET_RESET: {

            let values = {},
                errors = {};

            for (let value in action.modelForm.values) {
                values[value] = null;
                errors[value] = null;
            }
            return {
                ...state,
                [action.formName]: {
                    values: values,
                    isChanged: false,
                    stateForm: FORM_WIDGET_INIT,
                    errors: errors
                }
            }
        }
    }
    return state;
}