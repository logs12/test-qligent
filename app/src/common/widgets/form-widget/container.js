import './style.scss';
import { addTextToConstant } from '../../utils/helperFunctions';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import shallowEqual from "react-redux/lib/utils/shallowEqual";
import { bindActionCreators } from "redux";
import FormComponent from './component';
import { initActionFormWidget, submitActionFormWidget } from './actions';
import { initModelAction } from '../../actions/ModelActions';

export default function FormWidgetHoc(
    actionName,
    url,
    className,
    initModel,
    children,
    formName,
    entityName,
) {

    const mapStateToProps = state => ({
        actionName:actionName,
        url: url,
        className: className,
        initModel: initModel,
        children: children,
        formName: formName,
        entityName,
        model: state[entityName].model.values,
    });
    const mapDispatchToProps = dispatch => ({
        initModelAction: bindActionCreators(initModelAction, dispatch),
        initActionFormWidget: bindActionCreators(initActionFormWidget, dispatch),
        submitActionFormWidget: bindActionCreators(submitActionFormWidget, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class FormWidgetContainer extends Component {

        /**
         * Объект с данными формы
         * @type {{}}
         */
        modelForm = {};

        /**
         * Объект с ошибками формы
         * @type {{}}
         */
        modelFormError = {};

        static defaultProps = {
            initModelConstant: addTextToConstant(entityName, 'MODEL_INIT'),
        };

        /**
         * Перед рендерингом инициализируем форму в store
         */
        componentWillMount() {

            this.generationModelForm(this.props.initModel);

            // Init state model
            this.props.initModelAction(
                this.props.initModelConstant,
                this.modelForm,
                this.modelFormError,
            );

            // Init state form-widget
            this.props.initActionFormWidget(this.props.formName);
        }


        /**
         * Запрещаем рендер если данные в модельках разные.
         * @param nextProps
         * @returns {boolean}
         */
        shouldComponentUpdate(nextProps) {
            return shallowEqual(this.props.model, nextProps.model);
        }

        /**
         * Generation model form
         * @param initModel
         */
        generationModelForm(initModel) {
            if (initModel) {
                for (let attribute in initModel) {
                    if (initModel.hasOwnProperty(attribute)) {
                        this.modelForm[attribute] = initModel[attribute];
                        this.modelFormError[attribute] = null;
                    }
                }
            }
        }

        /**
         * Отправка данных
         * @param event
         */
        submitHandle(event) {
            // Собираем из store данные введенные в форму
            let values = this.props.model;

            // Запускаем submit формы
            this.props.submitActionFormWidget(
                this.props.actionName,
                {
                    formName: this.props.formName,
                    values: values,
                    url: this.props.url,
                },
                entityName,
            );

            event.preventDefault();
        }

        render() {
            return (
                <FormComponent
                    submitHandle={::this.submitHandle}
                    className={this.props.className}
                >
                    {this.props.children}
                </FormComponent>
            )
        }
    }
    return FormWidgetContainer;
}