import {
    FORM_WIDGET_INIT,
    FORM_WIDGET_RESET,
} from '../../constants';
import * as actions from '../../actions';

/**
 * Первоначальная инициализация формы в глобальном state
 * @param formName
 * @returns {function(*): *}
 */
export function initActionFormWidget(formName) {
    return dispatch => dispatch({
        type: FORM_WIDGET_INIT,
        payload: {
            formName: formName,
        },
    });
}

/**
 * Actions submit form
 * @param {string}  actionName - название action который обрабатывает submit
 * @param {object} data - объект с данными формы
 * data{
 *  formName {string} - название формы
 *  data {object} - объект с данными формы
 *  url {string} - url на который происходит отправка данных
 * }
 * @param {string} entityName
 * @returns {{types: *[], promise: (function())}}
 */
export function submitActionFormWidget(actionName, data, entityName) {
    return actions[actionName]( data, {formName: data.formName}, entityName);
}

export function reset(formName, modelForm, modelFormError) {

    return dispatch => dispatch({
        type: FORM_WIDGET_RESET,
        formName,
        modelForm,
        modelFormError,
    });
}