import compare from '../utils/compare';
import concat from 'lodash/concat';
import isArray from 'lodash/isArray';
import isEqual from 'lodash/isEqual';

/**
 *
 * @param state - глобальный объект с состоянием проекта
 * @param action - действие
 * @param initType
 * @param updateType
 * @returns {*}
 * @constructor
 */

let BaseModelReducer = {};

BaseModelReducer.initialState = {};

BaseModelReducer.initModel = (state, action) => {
    BaseModelReducer.initialState = {
        values: action.values,
        isChanged: false,
        errors: action.errors,
    };

    return {
        ...state,
        values: action.values,
        isChanged: false,
        errors: action.errors,
    }
};

BaseModelReducer.updateModel = (state, action) => {

    // Формируем объект с данными из input
    let values = {};
    
    for (let value in state['values']) {
        if (state['values'].hasOwnProperty(value)) {
            if (action.payload.hasOwnProperty(value)) {
                if (isArray(state['values'][value])) {
                    values[value] = state['values'][value] !== null ?
                        concat(state['values'][value], action.payload[value])
                        : [action.payload[value]];
                } else {
                    values[value] = action.payload[value];
                }
            } else {
                values[value] = state['values'][value];
            }
        }
    }

    // Чистим ошибки в state
    let errors = {};
    for (let errorFieldName in state['errors']) {
        if (state['errors'].hasOwnProperty(errorFieldName)) {
            errors[errorFieldName] = null;
        }
    }

    // Флаг изменились ли данные
    let isChanged = !isEqual(
        BaseModelReducer.initialState.values,
        values,
    );

    return {
        ...state,
        values: {
            ...state.values,
            ...values
        },
        errors: {...errors},
        isChanged,
    };
};

BaseModelReducer.errorModel = (state, action) => {

    let errors = {};
    
    for (let error in action.errors) {
        if (action.errors.hasOwnProperty(error)) {
            errors[action.errors[error]['field']] = action.errors[error].message;
        }
    }
    return {
        ...state,
        errors: {...errors},
    };
};

BaseModelReducer.fileInputMultipleWidgetConvertToBase64 = (state, action) => {

    // Формируем объект с данными из input
    let values = {};

    for (let value in state['values']) {
        if (state['values'].hasOwnProperty(value)) {
            if (action.payload.hasOwnProperty(value)) {
                values[value] = action.payload[value];
            } else {
                values[value] = state['values'][value];
            }
        } else {
            values[value] = state['values'][value];
        }
    }

    // Модифицируем первоначальный объект с данными
    BaseModelReducer.initialState = {
        ...BaseModelReducer.initialState,
        values: values,
    };

    return {

        ...state,
        values: {
            ...state.values,
            ...values
        },
    };
};

BaseModelReducer.fileInputMultipleWidgetUpdateValue = (state, action) => {

    // Формируем объект с данными из input
    let values = {},
        isChanged = false,
        initialFileWidgetMultipleState = [];

    for (let value in state['values']) {
        if (state['values'].hasOwnProperty(value)) {
            if (action.payload.hasOwnProperty(value)) {

                if (BaseModelReducer.initialState['values'].hasOwnProperty(value)) {
                    initialFileWidgetMultipleState[value] = BaseModelReducer.initialState['values'][value]
                }

                if (state['values'][value] !== null) {
                    values[value] = isArray(state['values'][value]) ?
                        concat(state['values'][value], action.payload[value])
                        : [action.payload[value]];
                } else {
                    values[value] = action.payload[value];
                }

                isChanged = !compare(initialFileWidgetMultipleState[value], values[value]);

            } else {
                values[value] = state['values'][value];
            }
        }
    }

    // Чистим ошибки в state
    let errors = {};
    for(let errorFieldName in state['errors']) {
        if (state['errors'].hasOwnProperty(errorFieldName)) {
            errors[errorFieldName] = null;
        }
    }

    return {
        ...state,
        values: {
            ...state.values,
            ...values
        },
        errors: {...errors},
        isChanged,
    };
};

BaseModelReducer.fileInputMultipleWidgetDeleteValue = (state, action) => {

    // Формируем объект с данными из input
    let values = {},
        isChanged = false;

    for (let value in state['values']) {
        if (state['values'].hasOwnProperty(value)) {
            if (action.payload.hasOwnProperty(value)) {
                values[value] = action.payload[value];
                isChanged = !compare(BaseModelReducer.initialState['values'][value], values[value]);
            } else {
                values[value] = state['values'][value];
            }

        } else {
            values[value] = state['values'][value];
        }
    }

    // Чистим ошибки в state
    let errors = {};
    for(let errorFieldName in state['errors']) {
        if (state['errors'].hasOwnProperty(errorFieldName)) {
            errors[errorFieldName] = null;
        }
    }

    return {
        ...state,
        values: {
            ...state.values,
            ...values
        },
        errors: {...errors},
        isChanged,
    }
};

export default BaseModelReducer;