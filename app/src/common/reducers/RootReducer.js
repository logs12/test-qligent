import { combineReducers } from 'redux';
import FormWidgetReducer from '../widgets/form-widget/reducers';
import SystemErrorReducer from '../widgets/error-widget/reducers';
import ConfigDataReducer from './ConfigDataReducer';
import ProgressBarWidgetReducer from '../widgets/progress-bar-widget/reducer';
import SnackbarWidgetReducer from '../widgets/snackbar-widget/reducer';
import PaginationWidgetReducer from '../../common/widgets/pagination-widget/reducer';
import SearchWidgetReducer from '../../common/widgets/search-widget/reducer';
import Select2WidgetReducer from '../../common/widgets/select2-widget/reducer';

// Collections Reducer
import BaseCollectionReducer from './BaseCollectionReducer';

// Model Reducer
import BaseModelReducer from './BaseModelReducer';

import { reducer as breadcrumb } from '../../common/widgets/breadcrumbs-widget';
import { routerReducer } from 'react-router-redux';

const createReducer = () => {
    return combineReducers({
        User: combineReducers({
            collection: BaseCollectionReducer.bind(null,'User'),
            model: BaseModelReducer.bind(null, 'User'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Login: combineReducers({
            model: BaseModelReducer.bind(null, 'Login'),
        }),
        common: combineReducers({
            formWidget: FormWidgetReducer,
            select2Widget: Select2WidgetReducer,
            snackbarWidget: SnackbarWidgetReducer,
            progressBarWidget: ProgressBarWidgetReducer,
            configData: ConfigDataReducer,
            systemError: SystemErrorReducer,
        }),
        routing: routerReducer,
        ApartmentAdvertisement: combineReducers({
            collection: BaseCollectionReducer.bind(null,'ApartmentAdvertisement'),
            model: BaseModelReducer.bind(null, 'ApartmentAdvertisement'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        CommercialEstateAdvertisement: combineReducers({
            collection: BaseCollectionReducer.bind(null,'CommercialEstateAdvertisement'),
            model: BaseModelReducer.bind(null, 'CommercialEstateAdvertisement'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        CottageHousesLandAdvertisement: combineReducers({
            collection: BaseCollectionReducer.bind(null,'CottageHousesLandAdvertisement'),
            model: BaseModelReducer.bind(null, 'CottageHousesLandAdvertisement'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        CustomerRequest: combineReducers({
            collection: BaseCollectionReducer.bind(null, 'CustomerRequest'),
            model: BaseModelReducer.bind(null, 'CustomerRequest'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),

        NewsArticle: combineReducers({
            collection: BaseCollectionReducer.bind(null,'NewsArticle'),
            model: BaseModelReducer.bind(null, 'NewsArticle'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),

        ImportReport: combineReducers({
            collection: BaseCollectionReducer.bind(null,'ImportReport'),
            model: BaseModelReducer.bind(null, 'ImportReport'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        Page: combineReducers({
            collection: BaseCollectionReducer.bind(null,'Page'),
            model: BaseModelReducer.bind(null,'Page'),
            pagination: PaginationWidgetReducer,
            search: SearchWidgetReducer,
        }),
        breadcrumb
    })
};

export default createReducer();