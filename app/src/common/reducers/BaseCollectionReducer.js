import isArray from 'lodash/isArray';
import filter from 'lodash/filter';
import {
    BASE_MODEL_GET,
    BASE_MODEL_DELETE,
} from '../../common/constants/index';

export default function BaseCollectionReducer(entityName, state = [], action) {
    switch (action.type) {
        case BASE_MODEL_GET(entityName): {
            if (!isArray(action.payload)) {
                return [
                    {...action.payload}
                ];
            }
            return [
                ...action.payload
            ];
        }
        case BASE_MODEL_DELETE(entityName): {
            // Filter the collection of deleted item
            return [
                ...filter(state, (user) => user.id !== action.payload['modelId']),
            ];
        }
        default: {
            return state
        }
    }
}
