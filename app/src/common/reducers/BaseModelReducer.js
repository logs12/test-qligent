import BaseModel from '../../common/reducers/BaseModel';

import {
    BASE_MODEL_INIT,
    BASE_MODEL_UPDATE,
    BASE_FORM_MODEL_ERROR,
    BASE_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64,
    BASE_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE,
    BASE_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE
} from '../../common/constants/index';

let initialState = {};

export default function BaseModelReducer(entityName, state = initialState, action) {

    switch (action.type) {
        case BASE_MODEL_INIT(entityName): {
            return BaseModel.initModel(state, action);
        }

        case BASE_MODEL_UPDATE(entityName): {
            return BaseModel.updateModel(state, action);
        }
            
        case BASE_FORM_MODEL_ERROR(entityName): {
            return BaseModel.errorModel(state, action);
        }

        case BASE_FILE_INPUT_MULTIPLE_WIDGET_CONVERT_TO_BASE64(entityName): {
            return BaseModel.fileInputMultipleWidgetConvertToBase64(state, action);
        }

        case BASE_FILE_INPUT_MULTIPLE_WIDGET_UPDATE_VALUE(entityName): {
            return BaseModel.fileInputMultipleWidgetUpdateValue(state, action);
        }

        case BASE_FILE_INPUT_MULTIPLE_WIDGET_DELETE_VALUE(entityName): {
            return BaseModel.fileInputMultipleWidgetDeleteValue(state, action);
        }
    }
    
    return state;
}