import React, {Component} from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import * as actions from '../../common/actions';


/**
 * Base HOC for relation page view with server
 * @param ComposedComponent
 * @param entityName
 * @param urlGetRequest
 * @param condition
 * @returns {ComponentHOC}
 * @constructor
 */
export const BaseListItems = (ComposedComponent, entityName, urlGetRequest, condition, actionName = 'baseGetAction') => {
    const mapStateToProps = (state, ownProps) => {
        return ({
            entityName: entityName,
            collection: state[entityName].collection,
            currentRoute: state.routing.locationBeforeTransitions.pathname,
            urlGetRequest: urlGetRequest,
            actionName: actionName,
        });
    };

    const mapDispatchToProps = (dispatch) => ({
        getCollection: bindActionCreators(actions[actionName], dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class ComponentHOC extends Component {

        getActionName(options) {
            if (this.props.actionName === 'baseGetAction') {
                return this.props.getCollection(this.props.urlGetRequest, options, this.props.entityName);
            }
            return this.props.getCollection(options);
        }

        componentWillMount() {
            this.getActionName(
                {condition: this.props.condition}
            );
        }

        render() {
            return <ComposedComponent { ...this.props } { ...this.state } />;
        }
    }

    return ComponentHOC;
};