import React, {Component} from 'react';
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import * as actions from '../../common/actions';
import { getModel } from '../selectors/BaseSelector';

import { initModelAction } from '../../common/actions/ModelActions';

import { BASE_MODEL_INIT } from '../../common/constants/BaseConstants';

/**
 * Base HOC for relation page view with server
 * @param ComposedComponent
 * @param entityName
 * @param urlGetRequest
 * @param condition
 * @returns {ComponentHOC}
 * @constructor
 */
export const BasePageView = (ComposedComponent, entityName, urlGetRequest, condition) => {

    const mapStateToProps = (state, ownProps) => {
        return ({
            entityName: entityName,
            collection: state[entityName].collection,
            model: getModel(state[entityName], condition ? condition : [Number(ownProps.params.id)]),
            currentRoute: state.routing.locationBeforeTransitions.pathname,
            urlGetRequest: urlGetRequest,
        });
    };

    const mapDispatchToProps = (dispatch) => ({
        modelActions: bindActionCreators(actions, dispatch),
        initModelAction: bindActionCreators(initModelAction, dispatch),
        pushToRouter: bindActionCreators(push, dispatch),
    });

    @connect(mapStateToProps, mapDispatchToProps)
    class ComponentHOC extends Component {

        componentWillMount() {
            if (isEmpty(this.props.model)) {
                this.props.modelActions['baseGetAction'](this.props.urlGetRequest, {}, this.props.entityName);
            } else {
                this.props.initModelAction(
                    BASE_MODEL_INIT(this.props.entityName),
                    this.props.model,
                    null,
                );
            }
        }

        componentWillReceiveProps(nextProps) {

            // Инициализируем модель
            this.props.initModelAction(
                BASE_MODEL_INIT(this.props.entityName),
                nextProps.model,
                null,
            );
        }

        render() {
            return <ComposedComponent { ...this.props } { ...this.state } />;
        }
    }

    return ComponentHOC;
};