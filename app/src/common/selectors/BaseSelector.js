import { createSelector } from 'reselect';
import find from 'lodash/find';
import filter from 'lodash/filter';
import includes from 'lodash/includes';

/**
 * Selected Collection
 * @param state
 * @param collectionName
 * @return collection
 */
const collectionSelector = (state) => {
    return state.collection;
};

/**
 * id for filtering collection
 * @param state
 * @param selectedModelId
 */
const selectedModelIdSelector = (state, selectedModelId) => (Number(selectedModelId));


/**
 * Filtering collection
 * @param collection
 * @param selectedId
 */
const getModelSelector = (collection, selectedId) => {
    return find(collection, {id: selectedId});
};

/**
 *
 * @param collection
 * @param selectedIds
 * @returns {*}
 */
const getModelsSelector = (collection, selectedIds) => {

    return filter(
        collection,
        model => includes(selectedIds, model.id),
    );
};

export const getModel = createSelector(
    collectionSelector,
    selectedModelIdSelector,
    getModelSelector,
);


export const getModels =  createSelector(
    collectionSelector,
    getModelsSelector,
);
