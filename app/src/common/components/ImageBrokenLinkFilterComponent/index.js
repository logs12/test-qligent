import React, { Component } from 'react';

export default class ImageBrokenLinkFilterComponent extends Component {

    static defaultProps = {
        key: 0,
    };

    state = {
        urlImage: null,
    };

    componentWillMount() {
        this.filterImageUrl(this.props.urlImage);
    }

    componentWillReceiveProps(nextProps) {
        this.filterImageUrl(nextProps.urlImage);
    }

    /**
     * Фильтрация битых урлов изображений
     * @param url
     */
    filterImageUrl(url) {

        const { noImageUrl } = this.props;

        new Promise(function(resolve, reject){
            let image = new Image();
            image.onload = function(){
                resolve(url);
            };
            image.onerror = function(){
                reject();
            };
            image.src = url;
        })
        .then((url) => {
            this.setState({ urlImage: url});
            return url;
        })
        .catch(() => {
            this.setState({ urlImage: noImageUrl});
        });

        return noImageUrl;
    };

    render() {
        const { noImageUrl, key } = this.props;
        const { urlImage } = this.state;

        return <img key={key} src={urlImage ? urlImage : noImageUrl} />;
    }
}