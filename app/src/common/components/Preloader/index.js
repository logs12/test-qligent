import './style.scss';

import React from 'react';

const Preloader = () => {
    return (
        <div className="preloader mdl-color-text--white">
            <div className="layout mdl-layout mdl-js-layout">
                <main className="mdl-layout__content">
                    <div className="preload-layout__preloader-content">
                            <h1>Альфа-Регион Инвест</h1>
                            <img src="/images/preloader-gif.gif" />
                    </div>
                </main>
            </div>
        </div>
    );
};

export default Preloader;