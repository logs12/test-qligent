import './style.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CustomToggleListFieldComponent from './component';

export default class CustomToggleListField extends Component {

    state = {
        checked: false,
    };

    static propTypes = {
        className: PropTypes.string,
        labelCommon: PropTypes.string,
        handleChange: PropTypes.func.isRequired,
        defaultValue: PropTypes.array.isRequired,
    };

    values = [];

    onChangeCheckbox(valueField, statusField) {

        if (this.values.indexOf(valueField) !== -1 && statusField === 'off') {
            this.values.splice(this.values.indexOf(valueField), 1);
        }
        if (this.values.indexOf(valueField) === -1 && statusField === 'on') {
            this.values.push(valueField);
        }

        if (this.props.handleChange) this.props.handleChange(this.values);
    }

    render() {
        const { className, defaultValue, labelCommon } = this.props;

        const { checked } = this.state;

        const classes = classNames('custom-toggle-field', {
            'mdl-js-ripple-effect': checked
        }, className);

        return (
            <div className={classes}>
                {
                    defaultValue.map((value, index) => {
                        return <CustomToggleListFieldComponent
                            key={index}
                            value={value.value}
                            label={value.label}
                            index={index}
                            id={Math.floor(Math.random() * 100000 )}
                            handleChange={::this.onChangeCheckbox}
                        />
                    })
                }
                <span className="custom-toggle-field__label-common">{ labelCommon }</span>
            </div>

        );
    }
}