import './style.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default class CustomToggleListFieldComponent extends Component {

    state = {
        checked: false,
    };

    static propTypes = {
        className: PropTypes.string,
        handleChange: PropTypes.func.isRequired,
    };

    onChangeCheckbox(event, value, callback) {
        event.preventDefault();
        let targetValue = 'off';
        if (!this.state.checked) {
            this.setState({
                checked: true,
            });
            targetValue = 'on';
        } else {
            this.setState({
                checked: false,
            });
        }

        if (callback) callback(value, targetValue);
    }

    render() {
        const { className, value, label, handleChange, index, id } = this.props;

        const { checked } = this.state;

        const classesLabel = classNames('custom-toggle-field__label', {
            'custom-toggle-field__label--checked': checked
        }, className);

        return (
            <div key={index} className="custom-toggle-field__container-item">
                <input
                    id={id}
                    type="checkbox"
                    className="custom-toggle-field__input"
                    onChange={(event) => {::this.onChangeCheckbox(event, value, handleChange)}}
                    checked={checked}
                />
                <label htmlFor={id}
                       className={classesLabel}
                >
                    <span>{label}</span>
                </label>
            </div>

        );
    }
}