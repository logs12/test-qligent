import isEqual from 'lodash/isEqual';

/**
 * Comparison of two collections
 * @param collectionOne
 * @param collectionTwo
 * @returns {boolean}
 */
const compare = (collectionOne, collectionTwo) => {
    return collectionOne.length === collectionTwo.length && collectionOne.every((item, index)=>{
            return isEqual(item, collectionTwo[index]);
        })
};

export default compare;