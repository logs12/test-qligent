import snakeCase from 'lodash/snakeCase';

/**
 *
 * @param text
 * @param constant
 * @returns {string}
 */
export function addTextToConstant(text, constant) {
    let textSnakeCaseReplace = snakeCase(text, ' ', '_');
    return `${textSnakeCaseReplace.toUpperCase()}_${constant}`;
};
